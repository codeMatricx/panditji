 <?php 
 include "database.php";
?>
<?php require('include/head.php'); ?>
<body onload="initialize()" class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
<?php include "include/header.php";?>
<?php include "include/left_sidebar.php";?>
<div class="content-wrapper" style="min-height: 879.773px">
<section class="content-header">
<h1>
Aarti Sub Category List  
</h1>
<ol class="breadcrumb">
<li><a href="addAartiSubCategory.php"><button type="button" class="btn btn-block " style="margin-top: -5px;">Add Aarti Sub Category</button></a></li>
</ol>
</section>
<section class="content">
<div class="box">
<div class="box-header">
<h3 class="box-title">Aarti Sub Category Table With Full Features</h3>
</div>
            <!-- /.box-header -->
            <div class="box-body table-responsive table-scroll-y">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>S.NO</th>
                  <th>God Name</th>
                  <th>Aarti Name</th>
                  <th>Icon</th> 
                  <!-- <th>Status </th> -->
                  <th>Action</th>
                  <th><input name="select-all" id="checkall" onClick="check_uncheck_checkbox(this.checked);" value="check_all" type="checkbox"></th>
                </tr>
                </thead>
                    <tbody>
                      <?php 
                      $sql = "SELECT asl.id,asl.aartiSubName,asl.image,al.aartiName from aarti_sub_list AS asl LEFT JOIN aarti_list AS al ON asl.aarti_list_id=al.id ";
                      //print_r($sql);exit;
                      $result = $conn->query($sql);
                      if ($result->num_rows>0)
                      {
                      $serial=0;
                      while($sub_category = $result->fetch_assoc())
                      {
                      $serial++;
                      ?>
                    <tr id="<?php  echo $sub_category['id'];?>">
                    <td><?php echo $serial; ?></td>
                      <td><?php echo $sub_category['aartiName'];?></td>
                      <td><?php echo $sub_category['aartiSubName'];?></td>
                      <td><img src="assets/img/uploads/aarti/<?php echo $sub_category['image']; ?>" class="img-responsive" style="width:30px; height:30px" ></td>
                       
                    <td >
                    <a href="aartiSubCatEdit.php?sub_cat_id=<?php echo $sub_category['id']; ?>" style="cursor: pointer;">
                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i> </a>
                    </td>
                    <td><input   type="checkbox" value="<?php echo $sub_category['id'];?>" name="action" id="checkbox-1"></td>
                    </tr> 
                    <?php 
                    }
                    } 
                    ?>
                    </tbody>
              </table>
<button class="btn btn-danger" onclick="deleteAartiSubCat()">Delete</button>
</div>
</div>
</div>
<?php include "include/footer.php" ;?>
<?php include "include/right_sidebar.php" ;?>
</div>
<?php include "include/footer_script.php" ;?>
<script src='http://maps.googleapis.com/maps/api/js?v=3&sensor=false&amp;libraries=places&key=AIzaSyAL1eAba9HoD7qszOJ-ggOvZvbq-TvXDys'></script>
<script src="js/script.js"> </script>
</body>
</html>
