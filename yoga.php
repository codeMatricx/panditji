 <?php 
 include "database.php";
?>

<?php require('include/head.php'); ?>
<body onload="initialize()" class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
<?php include "include/header.php";?>
<?php include "include/left_sidebar.php";?>
<div class="content-wrapper">
<section class="content-header">
<h1>
Yoga List
</h1>
<ol class="breadcrumb">
<li><a href="addYoga.php"><button type="button" class="btn btn-block " style="margin-top: -5px;">Add Yoga</button></a></li>
</ol>
</section>
<section class="content">
<div class="box">
<div class="box-header">
<h3 class="box-title">Yoga Table With Full Features</h3>
</div>
            <!-- /.box-header -->
            <div class="box-body table-responsive table-scroll-y">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                <th>S.NO</th>
                <th>Yoga Name</th>
                <th>Step</th>
                <!-- <th>Price</th> -->
                <th>Yoga</th>
                <th>Image</th>
                <th>Video</th>
                <th>Audio</th>
                <th>Posted Date</th>
                <!-- <th>Status </th> -->
                <th>Action</th>
                  <th><input name="select-all" id="checkall" onClick="check_uncheck_checkbox(this.checked);" value="check_all" type="checkbox"></th>
                </tr>
                </thead>
                    <tbody>
                        <?php 
                            $sql = "SELECT i.yogaName,p.status,p.id,p.title,p.description,p.created_date_time,p.image AS yoga_image,p.video AS yoga_video,p.audio AS yoga_audio 
                            from yoga AS p  
                            LEFT JOIN yoga_list AS i ON p.yoga_list_id=i.id  
                            Order By p.id";
                        $result = $conn->query($sql);
                        if ($result->num_rows>0)
                        {
                        $serial=0;
                        while($yogaLst = $result->fetch_assoc())
                        {
                        	//print_r($productLst['product_video']);exit;
                        $desc=$yogaLst['description'];
                        $serial++;
                        ?>
                    <tr id="<?php  echo $yogaLst['id'];?>">
                        <td><?php echo $serial; ?></td>
                        <td><?php  echo $yogaLst['yogaName'];?></td>
                        <td><?php  echo $yogaLst['title'];?></td>
                        <td><?php  echo substr ($desc, 0, 20);?></td> 
                        <td><img src="assets/img/uploads/yoga/<?php echo $yogaLst['yoga_image']; ?>" class="img-responsive" style="width:30px; height:30px" ></td>
                        <td><video  width="30px" height="30px" controls><source src="assets/img/uploads/yoga/<?php echo $yogaLst['yoga_video']; ?>" class="img-responsive" style="width:30px; height:30px" type="video/mp4"></video></td>
                        <td><audio width="30px" height="30px" controls><source src="assets/img/uploads/yoga/<?php echo $yogaLst['yoga_audio']; ?>" class="img-responsive" style="width:30px; height:30px" type="audio/ogg"></audio></td>
                        <td><?php  echo $yogaLst['created_date_time'];?></td>
                       <!-- <?php 
                        if($yogaLst['status'] == 1)
                        {
                        ?>
                        <td><img src="assets/img/enable.gif" class="img-responsive" title="Activated"></td>
                        <?php
                        }
                        ?>
                        <?php
                        if($yogaLst['status'] == 0)
                        {
                        ?>
                        <td><img src="assets/img/disable.gif" class="img-responsive" title="Deactivated"></td>
                        <?php
                        }
                        ?> -->
                    <td >
                    <a href="yogaEdit.php?yogaId=<?php echo $yogaLst['id']; ?>" style="cursor: pointer;">
                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i> </a>/
                    <a href="yogaView.php?yogaId=<?php echo $yogaLst['id']; ?>" style="cursor: pointer;">
                    <i class="fa fa-eye" aria-hidden="true"></i> </a>
                    </td>
                    <td><input   type="checkbox" value="<?php echo $yogaLst['id'];?>" name="action" id="checkbox-1"></td>
                    </tr> 
                    <?php 
                    }
                    } 
                    ?>
                    </tbody>
              </table>
<!-- <button class="btn btn-success" onclick="yogaActivate()">Activate</button>
<button class="btn btn-danger" onclick="yogaDeactivate()">Deactivate</button> -->
<button class="btn btn-danger" onclick="deleteYoga()">Delete</button>
</div>
</div>
</div>
<?php include "include/footer.php" ;?>
<?php include "include/right_sidebar.php" ;?>
</div>
<?php include "include/footer_script.php" ;?>
<script src='http://maps.googleapis.com/maps/api/js?v=3&sensor=false&amp;libraries=places&key=AIzaSyAL1eAba9HoD7qszOJ-ggOvZvbq-TvXDys'></script>
<script src="js/script.js"> </script>
</body>
</html>
