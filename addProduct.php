<?php 
include "database.php";
?>
<?php
if(isset($_POST["sub"]))
{
  //image
$target_dir = "assets/img/uploads/products/";
$image = $_FILES['image']["name"];
$target_file = $target_dir . basename($_FILES['image']["name"]);
//video
$target_dir_video = "assets/img/uploads/products/";
$video = $_FILES['video']["name"];
$target_file_video = $target_dir_video . basename($_FILES['video']["name"]);
//audio
$target_dir_audio = "assets/img/uploads/products/";
$audio = $_FILES['audio']["name"];
$target_file_audio = $target_dir_audio . basename($_FILES['audio']["name"]);

$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
$videoFileType = pathinfo($target_file_video,PATHINFO_EXTENSION);
$audioFileType = pathinfo($target_file_audio,PATHINFO_EXTENSION);

// $userId=$_POST["userId"];
$title=$_POST["title"];
$categoryId=$_POST["categoryId"];
$subCategoryId=$_POST["subCategoryId"];
$price=$_POST["price"];
$description=$_POST["description"];
$status = 1;
              if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file) || move_uploaded_file($_FILES["video"]["tmp_name"], $target_file_video) || move_uploaded_file($_FILES["audio"]["tmp_name"], $target_file_audio))
              {
                $image =  $_FILES["image"]["name"];
                $video =  $_FILES["video"]["name"];
                $audio =  $_FILES["audio"]["name"];
              }
            
      $sql = "INSERT INTO product (title,categoryId,subCategoryId,price,description)
      VALUES ('$title','$categoryId','$subCategoryId','$price','$description')";
      if ($conn->query($sql) === TRUE) 
      {
        $productId = mysqli_insert_id($conn);
      //image
        if(!empty($productId) && !empty($image))
        {
          $sql_image = "INSERT INTO product_image_gallery (post_id,post_image)
          VALUES ('$productId','$image')";
          $product_image=$conn->query($sql_image);
        } 
         //Video
        if(!empty($productId) && !empty($video))
        {
          $sql_video = "INSERT INTO product_video_gallery (productId,product_video)
          VALUES ('$productId','$video')";
          $product_video=$conn->query($sql_video);
         }
            //Audio
           if(!empty($productId) && !empty($audio))
           {
              $sql_audio = "INSERT INTO product_audio_gallery (productId,product_audio)
              VALUES ('$productId','$audio')";
              $product_audio=$conn->query($sql_audio);
            } 

      header("location:yoga.php");
      } 
      else 
      {
      echo "Error: " . $sql . "<br>" . $conn->error;
      }
}
?>
<?php require('include/head.php'); ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
 <?php include "include/header.php";?> 
  <?php include "include/left_sidebar.php";?>
       <div class="content-wrapper">
            <section class="content-header">
              <h1>
              Add Product     
              </h1>
            </section>
            <section class="content">
        <div class="box">
        <div class="box-body table-responsive table-scroll-y">
        <form method="post" enctype="multipart/form-data">
                  
            <table id="example1" class="table table-bordered table-striped">
            <table  class="table table-bordered table-striped">           
            <tbody>
              <tr>
              <th>Title</th>
              <td><input type="text" name="title" placeholder="Add Title" required></td>
              </tr>
            <tr>
            <th>Category</th>
              <td>
              <select name="categoryId" style="margin-left:0px;" required>
              <?php 
              $sql = "SELECT * from category";
              $result = $conn->query($sql);
              if ($result->num_rows>0)
              {
              $serial=0;
              while($category = $result->fetch_assoc())
              {
              ?>
              <option value="<?php echo $category['id'];?>" selected disable><?php echo $category['categoryName'];?></option>   
              <?php 
              } }
              ?>       
              </select>
            </td> 
            </tr>
            <tr>
              <th>Sub Category</th>
              <td>
              <select name="subCategoryId" style="margin-left:0px;" required>
              <?php 
              $sqlSub = "SELECT * from sub_category";
              $resultSub = $conn->query($sqlSub);
              if ($resultSub->num_rows>0)
              {
              while($subCategory = $resultSub->fetch_assoc())
              {
              ?>
              <option value="<?php echo $subCategory['id'];?>" selected disable><?php echo $subCategory['subCategoryName'];?></option>   
              <?php 
              } }
              ?>       
              </select>
            </td> 
            </tr>
            <tr>
              <th>Add Image</th>
                <td><!-- <label for="newimage" class="btn text-muted text-center " style="width:82%;margin-top: 2%;">Choose Image</label> -->
                    <input id="newimage" type="file" name="image"> 
              </td>
            </tr>
            <tr>
              <th>Add Video</th>
                <td><!-- <label for="newvideo" class="btn text-muted text-center " style="width:82%;margin-top: 2%;">Choose Video</label> -->
                    <input id="newvideo" type="file" name="video"> 
              </td>
            </tr>
            <tr>
              <th>Add Audio</th>
                <td><!-- <label for="newaudio" class="btn text-muted text-center " style="width:82%;margin-top: 2%;">Choose Audio</label> -->
                    <input id="newaudio" type="file" name="audio"> 
              </td>
            </tr>
            <tr>
              <th>Price</th>
              <td><input type="text" name="price" placeholder="Add Price"></td>
            </tr>
            <tr>
              <th>Description</th>
              <td><input type="text" name="description" placeholder="Add Description" required></td>
            </tr>
            
            </tbody>             
            </table>
            </table>
            <a href="product.php" style="color: #fff;"><button type="button" class="btn" style="margin-top: 10px" >Back</button></a>
            <button type="submit" class="btn   pull-right" name="sub" style="margin-top: 10px" >Add</button>
            
        </form>
        </div>
        </div>
        </div>
  <?php include "include/footer.php" ;?>
  <?php include "include/right_sidebar.php" ;?>  
</div>
<?php include "include/footer_script.php" ;?>
<script src="jscolor.js"></script>
</body>
</html>
