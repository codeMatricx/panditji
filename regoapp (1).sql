-- phpMyAdmin SQL Dump
-- version 4.0.10.18
-- https://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Jan 05, 2018 at 11:52 PM
-- Server version: 5.6.36-cll-lve
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `regoapp`
--

-- --------------------------------------------------------

--
-- Table structure for table `about_help`
--

CREATE TABLE IF NOT EXISTS `about_help` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `desription` varchar(255) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `about_help`
--

INSERT INTO `about_help` (`id`, `desription`, `created_date_time`) VALUES
(1, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has su', '2017-12-23 13:55:01');

-- --------------------------------------------------------

--
-- Table structure for table `admin_commission`
--

CREATE TABLE IF NOT EXISTS `admin_commission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `commission_type_id` int(11) NOT NULL,
  `commission_value` varchar(50) NOT NULL,
  `commission_status` tinyint(4) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `admin_commission`
--

INSERT INTO `admin_commission` (`id`, `user_id`, `commission_type_id`, `commission_value`, `commission_status`, `created_date_time`) VALUES
(1, 70, 1, '10', 1, '2017-12-29 06:57:14'),
(2, 70, 2, '70', 0, '2017-12-29 06:57:28');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `color_code` varchar(255) NOT NULL,
  `category_status` tinyint(4) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `category_name`, `image`, `user_id`, `color_code`, `category_status`, `created_date_time`) VALUES
(5, 'Outdoor Fun', 'outdoorfun.jpg', 33, '8132A8', 1, '2017-12-19 12:56:01'),
(6, 'Party Events', 'partyevents.jpg', 34, 'FF732B', 1, '2017-12-19 12:57:45'),
(7, 'Automotive', 'automotive.png', 35, '3129FF', 1, '2017-12-19 13:00:08'),
(8, 'Boating/PWC share', 'boating.png', 34, '99574D', 0, '2017-12-20 13:21:21'),
(9, 'Tools', 'tool.png', 70, '66DCFA', 0, '2017-12-30 16:30:04'),
(10, 'Babay Zone', 'babyzone.png', 70, 'FF61F4', 0, '2017-12-30 16:30:04'),
(11, 'Kid Zone', 'kidzone.png', 70, '3B9139', 0, '2017-12-30 16:30:55'),
(12, 'Electronics', 'electronics.png', 70, 'FFF670', 0, '2017-12-30 16:30:55'),
(15, 'Personal Items', 'personal.jpg', 47, 'FFFFFF', 0, '2018-01-04 04:51:18');

-- --------------------------------------------------------

--
-- Table structure for table `discount_type`
--

CREATE TABLE IF NOT EXISTS `discount_type` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `discount_name` varchar(100) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `discount_type`
--

INSERT INTO `discount_type` (`id`, `discount_name`, `created_date_time`) VALUES
(1, 'Percent', '2017-12-20 07:34:47'),
(2, 'Direct', '2017-12-20 07:35:00');

-- --------------------------------------------------------

--
-- Table structure for table `filter`
--

CREATE TABLE IF NOT EXISTS `filter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `categoryId` int(11) NOT NULL,
  `subCategoryId` int(11) NOT NULL,
  `priceFrom` int(11) NOT NULL,
  `priceTo` int(11) NOT NULL,
  `adsInKm` int(11) NOT NULL DEFAULT '30',
  `lati` float DEFAULT NULL,
  `lang` float DEFAULT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `filter`
--

INSERT INTO `filter` (`id`, `userId`, `categoryId`, `subCategoryId`, `priceFrom`, `priceTo`, `adsInKm`, `lati`, `lang`, `created_date_time`) VALUES
(5, 47, 9, 36, 100, 300, 81, NULL, NULL, '2018-01-04 16:47:06'),
(13, 70, 5, 4, 210, 250, 10, NULL, NULL, '2018-01-05 05:21:30'),
(7, 102, 5, 5, 1500, 5000, 11, NULL, NULL, '2018-01-04 17:05:01'),
(8, 70, 5, 4, 200, 300, 10, NULL, NULL, '2018-01-04 17:23:02'),
(12, 70, 5, 4, 210, 250, 30, NULL, NULL, '2018-01-05 05:20:42'),
(11, 70, 5, 4, 210, 250, 10, NULL, NULL, '2018-01-05 05:18:26'),
(17, 70, 5, 4, 210, 250, 10, NULL, NULL, '2018-01-05 05:52:00'),
(18, 70, 5, 0, 255, 0, 0, NULL, NULL, '2018-01-05 06:41:59');

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE IF NOT EXISTS `gender` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `gender_name` varchar(255) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`id`, `gender_name`, `created_date_time`) VALUES
(1, 'Male', '2017-12-19 10:43:35'),
(2, 'Female', '2017-12-19 10:43:50'),
(3, 'Others', '2017-12-19 10:44:08');

-- --------------------------------------------------------

--
-- Table structure for table `length_of_rental`
--

CREATE TABLE IF NOT EXISTS `length_of_rental` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `length_of_rental` varchar(255) NOT NULL,
  `number_of_days` int(11) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `length_of_rental`
--

INSERT INTO `length_of_rental` (`id`, `user_id`, `length_of_rental`, `number_of_days`, `created_date_time`) VALUES
(1, 34, '1 Year', 365, '2017-12-22 13:34:34'),
(2, 34, '1 Month', 30, '2017-12-22 13:34:50'),
(3, 34, '1 Week', 7, '2017-12-22 13:35:08'),
(4, 34, '1 Day', 1, '2017-12-22 13:35:27');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE IF NOT EXISTS `login` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `user_id` int(255) NOT NULL,
  `session_id` varchar(255) NOT NULL,
  `device_token` varchar(255) NOT NULL,
  `device_type` varchar(255) NOT NULL,
  `user_login_type` int(11) DEFAULT '0',
  `login_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=280 ;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`id`, `user_id`, `session_id`, `device_token`, `device_type`, `user_login_type`, `login_date_time`) VALUES
(80, 56, '657', '', '', 0, '2017-12-26 09:37:15'),
(81, 56, '791', '', '', 0, '2017-12-26 09:38:24'),
(83, 62, '537', '', '', 0, '2017-12-26 09:39:59'),
(85, 63, '491', '', '', 0, '2017-12-26 09:43:01'),
(103, 63, '833', '', '', 0, '2017-12-26 13:26:41'),
(104, 37, '998', '', '', 0, '2017-12-26 14:36:20'),
(105, 63, '577', '', '', 0, '2017-12-26 14:39:22'),
(128, 63, '495', '', '', 0, '2017-12-27 05:40:12'),
(129, 63, '813', '', '', 0, '2017-12-27 05:43:42'),
(131, 63, '929', '', '', 0, '2017-12-27 05:47:17'),
(136, 74, '790', '', '', 0, '2017-12-27 07:26:23'),
(179, 63, '837', '', '', 0, '2017-12-28 09:38:27'),
(180, 63, '956', '', '', 0, '2017-12-28 09:43:59'),
(181, 78, '845', '', '', 0, '2017-12-28 09:57:46'),
(208, 80, '996', '', '', 0, '2017-12-30 07:38:15'),
(209, 70, '641', '', '', 0, '2017-12-30 07:45:25'),
(211, 74, '581', '', '', 0, '2017-12-30 14:03:19'),
(212, 74, '895', '', '', 0, '2017-12-30 14:06:45'),
(213, 74, '445', '', '', 0, '2017-12-30 14:09:53'),
(214, 88, '658', '', '', 0, '2017-12-30 15:50:22'),
(215, 88, '820', '', '', 0, '2017-12-30 16:23:28'),
(221, 67, '694', '', '', 0, '2018-01-03 14:57:15'),
(222, 67, '643', '', '', 0, '2018-01-03 14:59:37'),
(223, 67, '631', '', '', 0, '2018-01-03 15:00:12'),
(224, 67, '927', '', '', 0, '2018-01-03 15:01:19'),
(225, 67, '868', '', '', 0, '2018-01-03 15:01:21'),
(226, 67, '913', '', '', 0, '2018-01-03 15:01:21'),
(227, 67, '745', '', '', 0, '2018-01-03 15:03:18'),
(228, 94, '902', '', '', 0, '2018-01-03 15:04:11'),
(229, 95, '678', '', '', 0, '2018-01-03 16:03:08'),
(230, 97, '710', '', '', 0, '2018-01-03 16:14:35'),
(231, 97, '806', '', '', 0, '2018-01-03 16:26:38'),
(232, 97, '869', '', '', 0, '2018-01-03 16:27:47'),
(239, 102, '993', '', '', 0, '2018-01-04 11:58:59'),
(241, 102, '783', '', '', 0, '2018-01-04 12:00:04'),
(248, 104, '627', '', '', 0, '2018-01-04 12:44:27'),
(251, 104, '934', '', '', 0, '2018-01-04 12:53:39'),
(269, 102, '595', '', '', 0, '2018-01-05 11:20:21');

-- --------------------------------------------------------

--
-- Table structure for table `manage_coupon`
--

CREATE TABLE IF NOT EXISTS `manage_coupon` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `valid_for` int(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `coupon_code` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `discount_type_id` int(11) NOT NULL,
  `discount` int(255) NOT NULL,
  `valid_till` date NOT NULL,
  `image` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `manage_coupon`
--

INSERT INTO `manage_coupon` (`id`, `valid_for`, `email`, `coupon_code`, `description`, `discount_type_id`, `discount`, `valid_till`, `image`, `user_id`, `status`, `created_date_time`) VALUES
(10, 2, 'rego@gmail.com', 'DF56', 'In addition to finding coupon codes offered directly by an online merchant, there are a number of websites that track new coupon offers from merchants worldwide', 1, 6, '2017-12-31', 'offerimage.jpeg', 47, 1, '2017-12-29 05:47:57');

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE IF NOT EXISTS `notifications` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `sender_user_id` int(11) NOT NULL,
  `reciever_user_id` int(11) NOT NULL,
  `message` varchar(255) NOT NULL,
  `userImage` varchar(255) NOT NULL,
  `device_type` varchar(100) NOT NULL,
  `device_token` varchar(255) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `notification_type_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `sender_user_id`, `reciever_user_id`, `message`, `userImage`, `device_type`, `device_token`, `created_date_time`, `notification_type_id`) VALUES
(1, 70, 47, 'Hi', 'user.png', 'ios', 'sasdsadasdsad', '2017-12-23 10:17:10', 1),
(2, 47, 70, 'Hello', 'user.png', 'Android', 'sadsadsdad', '2017-12-23 10:17:10', 2),
(3, 70, 102, 'hello team', 'logo.png', '', '', '2018-01-05 11:25:18', 2);

-- --------------------------------------------------------

--
-- Table structure for table `notification_type`
--

CREATE TABLE IF NOT EXISTS `notification_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(100) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `notification_type`
--

INSERT INTO `notification_type` (`id`, `type_name`, `created_date_time`) VALUES
(1, 'New Post', '2017-12-29 11:03:02'),
(2, 'Place order', '2017-12-29 11:03:02'),
(3, 'By Admin', '2017-12-29 11:03:33');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `order_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL,
  `total_amount` varchar(255) NOT NULL,
  `payment_mode` varchar(255) NOT NULL,
  `payment_status` int(255) NOT NULL,
  `admin_commission_id` int(11) NOT NULL,
  `manage_coupon_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `sub_category_id` int(11) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `order_date`, `user_id`, `total_amount`, `payment_mode`, `payment_status`, `admin_commission_id`, `manage_coupon_id`, `post_id`, `sub_category_id`, `created_date_time`) VALUES
(1, '2017-12-20 15:06:06', 70, '500', 'online', 1, 1, 10, 0, 2, '2017-12-20 09:36:06');

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE IF NOT EXISTS `post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `price` varchar(255) NOT NULL,
  `length_of_rental` int(255) DEFAULT NULL,
  `user_id` int(255) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `sub_category_id` int(11) NOT NULL,
  `address` varchar(255) NOT NULL,
  `lat` float(15,11) NOT NULL,
  `lng` float(15,11) NOT NULL,
  `status` int(4) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=41 ;

--
-- Dumping data for table `post`
--

INSERT INTO `post` (`id`, `category`, `description`, `location`, `price`, `length_of_rental`, `user_id`, `user_name`, `title`, `sub_category_id`, `address`, `lat`, `lng`, `status`, `created_date_time`) VALUES
(1, '5', 'simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only', 'Noida', '200', 1, 34, 'ravindra', 'demo', 2, 'c-75', 28.62080001831, 77.36389923096, 1, '2017-12-20 06:52:33'),
(2, '5', 'simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only', '', '300', 2, 47, '', 'Bike', 2, 'c-75', 28.62080001831, 77.36389923096, 0, '2017-12-22 14:49:04'),
(3, '5', 'simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s', '', '310', 2, 100, '', 'Car', 2, 'c-75', 28.53549957275, 77.39099884033, 1, '2017-12-22 14:50:41'),
(9, '5', 'simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only', '', '200', 2, 100, '', '', 2, 'c-75', 28.53549957275, 77.39099884033, 0, '2017-12-23 08:03:15'),
(11, '6', 'simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only', 'Noida Sec-10', '200', 2, 102, '', 'Truck', 1, 'c-75', 28.53549957275, 77.39099884033, 0, '2017-12-23 08:10:22'),
(12, '', '', '', '', 0, 0, '', '', 0, '', 0.00000000000, 0.00000000000, 0, '2017-12-26 14:43:22'),
(14, '5', 'simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only', '', '200', 2, 102, '', 'BeachGear', 2, 'c-75', 28.53549957275, 77.39099884033, 0, '2017-12-27 07:33:44'),
(15, '5', 'simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only', '', '200', 2, 100, '', '', 2, 'c-75', 28.53549957275, 77.39099884033, 0, '2017-12-27 07:34:55'),
(16, '5', '', '', '200', 2, 100, '', 'Bicycles', 2, 'c-75', 28.53549957275, 77.39099884033, 0, '2017-12-27 08:10:28'),
(17, '6', 'simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only', '', '600', 2, 100, '', 'Games', 2, 'c-75', 28.53549957275, 77.39099884033, 0, '2017-12-27 08:11:21'),
(18, '5', 'Desc', '', '200', 2, 74, '', '', 2, 'c-75', 28.53549957275, 77.39099884033, 0, '2017-12-27 08:17:11'),
(20, '5', 'Desc', '', '600', 2, 74, '', '', 2, 'c-75', 28.53549957275, 77.39099884033, 0, '2017-12-27 09:25:11'),
(21, '5', 'Desc', '', '600', 2, 47, '', 'GolfCart', 2, 'c-75', 28.53549957275, 77.39099884033, 1, '2017-12-27 09:25:24'),
(22, '6', 'Desc', '', '200', 2, 74, '', '', 1, 'c-75', 28.53549957275, 77.39099884033, 0, '2017-12-27 09:54:03'),
(23, '6', 'Desc', '', '200', 2, 70, '', '', 1, 'c-75', 28.53549957275, 77.39099884033, 0, '2017-12-27 10:19:02'),
(24, '5', 'Desc', '', '200', 2, 70, '', '', 2, 'c-75', 28.53549957275, 77.39099884033, 0, '2017-12-27 11:41:29'),
(25, '5', 'Desc', '', '200', 2, 70, '', '', 2, 'c-75', 28.53549957275, 77.39099884033, 0, '2017-12-27 11:42:18'),
(26, '5', '', '', '', 0, 0, '', '', 0, '', 0.00000000000, 0.00000000000, 0, '2017-12-29 13:45:45'),
(27, '5', 'Dgdsgd dgdgd refreshed. Gdgd', '', '54', 1, 79, '', '', 3, 'c-75', 28.53549957275, 77.39099884033, 0, '2017-12-29 14:30:38'),
(28, '5', 'Sdsadsdsd sdf dsadasda sd FDA', '', '', 0, 79, '', '', 0, '', 0.00000000000, 0.00000000000, 0, '2017-12-29 14:59:09'),
(29, '5', 'Sdsadsdsd sdf dsadasda sd FDA', '', '123', 1, 79, '', '', 3, 'c-75', 28.53549957275, 77.39099884033, 0, '2017-12-29 14:59:27'),
(30, '5', 'Sdsadsdsd sdf dsadasda sd FDA', '', '123', 1, 79, '', '', 3, 'c-75', 28.53549957275, 77.39099884033, 0, '2017-12-29 15:01:23'),
(31, '', '', '', '', 0, 0, '', '', 0, '', 0.00000000000, 0.00000000000, 0, '2017-12-29 15:06:23'),
(32, '', 'simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only', '', '', 0, 0, '', '', 0, '', 0.00000000000, 0.00000000000, 0, '2017-12-29 15:08:43'),
(33, '', 'simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only', '', '', 0, 0, '', '', 0, '', 0.00000000000, 0.00000000000, 0, '2017-12-29 15:11:15'),
(34, '5', 'Desc', '', '200', 2, 70, '', '', 2, 'c-75', 28.53549957275, 77.39099884033, 1, '2017-12-29 15:16:17'),
(38, '5', 'New Bike for riding', '', '500', 2, 70, '', '', 2, 'c-75', 28.53549957275, 77.39099884033, 1, '2017-12-30 15:07:40'),
(39, '5', 'Learn from yesterday, live for today,Hop for tomorrow.. Happy New Year', '', '500', 1, 100, '', 'DirtBike', 2, 'c-75', 28.53549957275, 77.39099884033, 1, '2018-01-04 12:17:30'),
(40, '7', 'Hello', '', '400', 1, 47, '', '', 25, 'c-75', 28.53549957275, 77.39099884033, 1, '2018-01-05 11:18:28');

-- --------------------------------------------------------

--
-- Table structure for table `post_gallery`
--

CREATE TABLE IF NOT EXISTS `post_gallery` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `post_image` varchar(255) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=60 ;

--
-- Dumping data for table `post_gallery`
--

INSERT INTO `post_gallery` (`id`, `post_id`, `post_image`, `created_date_time`) VALUES
(5, 11, '5a42635caaf55.jpeg', '2017-12-23 08:10:22'),
(6, 38, '5a27e6c02576b.jpeg', '2017-12-23 08:10:22'),
(7, 12, '5a42600a0b300.jpeg', '2017-12-26 14:43:22'),
(8, 13, '5a434c258a3ee.jpeg', '2017-12-27 07:30:45'),
(9, 13, '5a434c258a693.jpeg', '2017-12-27 07:30:45'),
(10, 14, '5a434cd87ab77.jpeg', '2017-12-27 07:33:44'),
(11, 14, '5a434cd87ace9.jpeg', '2017-12-27 07:33:44'),
(12, 3, '5a434d1fa8c42.jpeg', '2017-12-27 07:34:55'),
(13, 3, '5a434d1fa8e7c.jpeg', '2017-12-27 07:34:55'),
(14, 16, '5a435574b4070.jpeg', '2017-12-27 08:10:28'),
(15, 16, '5a435574b5f3c.jpeg', '2017-12-27 08:10:28'),
(16, 17, '5a4355a97664f.jpeg', '2017-12-27 08:11:21'),
(17, 17, '5a4355a9767f3.jpeg', '2017-12-27 08:11:21'),
(18, 18, '5a435707dfb45.jpeg', '2017-12-27 08:17:11'),
(19, 18, '5a435707dfe76.jpeg', '2017-12-27 08:17:11'),
(20, 19, '5a4362370812b.jpeg', '2017-12-27 08:20:26'),
(21, 19, '5a4362370812b.jpeg', '2017-12-27 08:20:26'),
(22, 20, '5a4366f7bbefd.jpeg', '2017-12-27 09:25:11'),
(23, 20, '5a4366f7d7c11.jpeg', '2017-12-27 09:25:11'),
(24, 21, '5a43670438aa1.jpeg', '2017-12-27 09:25:24'),
(25, 21, '5a43670438c31.jpeg', '2017-12-27 09:25:24'),
(26, 22, '5a436dbb54b11.jpeg', '2017-12-27 09:54:03'),
(27, 22, '5a436dbb54cd8.jpeg', '2017-12-27 09:54:03'),
(28, 23, '5a4373966c34f.jpeg', '2017-12-27 10:19:02'),
(29, 24, '5a4386e9e7e14.jpeg', '2017-12-27 11:41:29'),
(30, 25, '5a43871a54712.jpeg', '2017-12-27 11:42:18'),
(31, 26, '5a4647098adc3.jpeg', '2017-12-29 13:45:45'),
(32, 27, '5a46518e111bd.jpeg', '2017-12-29 14:30:38'),
(33, 28, '5a46583d46837.jpeg', '2017-12-29 14:59:09'),
(34, 29, '5a46584f4163f.jpeg', '2017-12-29 14:59:27'),
(35, 30, '5a4658c35cdd1.jpeg', '2017-12-29 15:01:23'),
(36, 31, '5a4659ef4f846.jpeg', '2017-12-29 15:06:23'),
(37, 32, '5a465a7be5ab5.jpeg', '2017-12-29 15:08:43'),
(38, 33, '5a465b13ab7f4.jpeg', '2017-12-29 15:11:15'),
(39, 34, '5a465c414244e.jpeg', '2017-12-29 15:16:17'),
(40, 34, '5a465c4142615.jpeg', '2017-12-29 15:16:17'),
(41, 35, '5a4785bd41462.jpeg', '2017-12-30 12:25:33'),
(42, 36, '5a47aaea340c0.jpeg', '2017-12-30 15:04:10'),
(43, 36, '5a47aaea35d02.jpeg', '2017-12-30 15:04:10'),
(44, 37, '5a47ab861cc5d.jpeg', '2017-12-30 15:06:46'),
(45, 37, '5a47ab861ced6.jpeg', '2017-12-30 15:06:46'),
(46, 38, '5a47b7c7a820f.jpeg', '2017-12-30 15:07:40'),
(47, 38, '5a47b7c7a820f.jpeg', '2017-12-30 15:07:40'),
(48, 39, '5a4b6d0778246.jpeg', '2018-01-02 11:29:11'),
(49, 39, '5a4b6d078ea90.jpeg', '2018-01-02 11:29:11'),
(50, 39, '5a4b6d078ec87.jpeg', '2018-01-02 11:29:11'),
(51, 39, '5a4b6d078ee36.jpeg', '2018-01-02 11:29:11'),
(52, 39, '5a4e1b5a269b2.jpeg', '2018-01-04 12:17:30'),
(53, 39, '5a4e1b5a26d1e.jpeg', '2018-01-04 12:17:30'),
(54, 39, '5a4e1b5a26e5c.jpeg', '2018-01-04 12:17:30'),
(55, 39, '5a4e1b5a26f85.jpeg', '2018-01-04 12:17:30'),
(56, 40, '5a4f5f048e1ad.jpeg', '2018-01-05 11:18:28'),
(57, 40, '5a4f5f04905d5.jpeg', '2018-01-05 11:18:28'),
(58, 40, '5a4f5f049074e.jpeg', '2018-01-05 11:18:28'),
(59, 40, '5a4f5f04908c4.jpeg', '2018-01-05 11:18:28');

-- --------------------------------------------------------

--
-- Table structure for table `promotions`
--

CREATE TABLE IF NOT EXISTS `promotions` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `card_title` varchar(255) NOT NULL,
  `card_amount` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_date_time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `promotions`
--

INSERT INTO `promotions` (`id`, `card_title`, `card_amount`, `user_id`, `status`, `created_date_time`) VALUES
(1, 'test', '200', 4, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE IF NOT EXISTS `role` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(255) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `role_name`, `created_date_time`) VALUES
(1, 'Super Admin', '2017-12-19 10:44:36'),
(2, 'Admin', '2017-12-19 10:45:30'),
(3, 'User', '2017-12-19 10:46:05');

-- --------------------------------------------------------

--
-- Table structure for table `sub_category`
--

CREATE TABLE IF NOT EXISTS `sub_category` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `category_id` int(255) NOT NULL,
  `sub_category_name` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `color_code` varchar(255) NOT NULL,
  `sub_category_status` tinyint(4) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=57 ;

--
-- Dumping data for table `sub_category`
--

INSERT INTO `sub_category` (`id`, `category_id`, `sub_category_name`, `image`, `color_code`, `sub_category_status`, `created_date_time`) VALUES
(2, 5, 'Paddle', 'paddle.png', '8132A8', 1, '2017-12-19 14:23:53'),
(3, 5, 'Surf Boards', 'surfboards.png', '8132A8', 1, '2017-12-27 07:00:00'),
(5, 5, 'Wake Boards', 'wakeboard.jpg', '8132A8', 1, '2017-12-27 12:04:22'),
(6, 5, 'Skim Boards', 'skimboard.png', '8132A8', 0, '2018-01-04 05:02:00'),
(7, 5, 'Kayaks', 'kayaks.jpg', '8132A8', 0, '2018-01-04 05:02:49'),
(8, 5, 'Bicycles', 'bicycles.jpg', '8132A8', 0, '2018-01-04 05:03:14'),
(9, 5, 'Beach Gear', 'beachgear.jpg', '8132A8', 0, '2018-01-04 05:03:37'),
(10, 5, 'Body Board', 'bodyboard.png', '8132A8', 0, '2018-01-04 05:03:57'),
(11, 5, 'Water Skis', 'waterskis.png', '8132A8', 0, '2018-01-04 05:04:20'),
(12, 5, 'Snow Boards', 'snowboard.jpg', '8132A8', 0, '2018-01-04 05:04:37'),
(13, 5, 'Snow Skis', 'snowskis.png', '8132A8', 0, '2018-01-04 05:04:57'),
(14, 5, 'Sleds', 'sleds.png', '8132A8', 0, '2018-01-04 05:05:14'),
(15, 5, 'Fishing Poles', 'fishingpoles.png', '8132A8', 0, '2018-01-04 05:05:30'),
(16, 6, 'Paddle Boards', 'paddleboards.jpg', 'FF732B', 0, '2018-01-04 05:06:25'),
(17, 6, 'Bounce Houses', 'bouncehouses.png', 'FF732B', 0, '2018-01-04 05:07:10'),
(18, 6, 'Tables', 'tables.png', 'FF732B', 0, '2018-01-04 05:07:33'),
(19, 6, 'Chairs', 'chairs.png', 'FF732B', 0, '2018-01-04 05:07:55'),
(20, 6, 'Kayaks', 'partykayaks.png', 'FF732B', 0, '2018-01-04 05:08:12'),
(21, 6, 'Bicycles', 'partybicycles.png', 'FF732B', 0, '2018-01-04 05:08:30'),
(22, 6, 'Games', 'games.jpg', 'FF732B', 0, '2018-01-04 05:08:45'),
(23, 6, 'MISC Party', 'MISCparty.png', 'FF732B', 0, '2018-01-04 05:09:04'),
(24, 7, 'Cars', 'cars.png', '3129FF', 0, '2018-01-04 05:10:39'),
(25, 7, 'Trucks', 'truck.png', '3129FF', 0, '2018-01-04 05:11:08'),
(26, 7, 'Motorcycles', 'motorcycle.png', '3129FF', 0, '2018-01-04 05:11:26'),
(27, 7, 'RV Campers', 'RVcampers.png', '3129FF', 0, '2018-01-04 05:11:58'),
(28, 7, 'DirtBike/ATV', 'dirtbike.png', '3129FF', 0, '2018-01-04 05:12:17'),
(29, 7, 'GolfCart', 'golfcart.png', '3129FF', 0, '2018-01-04 05:12:36'),
(30, 7, 'Trailers', 'trailers.png', '3129FF', 0, '2018-01-04 05:13:12'),
(31, 7, 'Moped/Scooter', 'mopped.png', '3129FF', 0, '2018-01-04 05:13:32'),
(32, 8, 'Canoes', 'canoes.png', '99574D', 0, '2018-01-04 06:23:32'),
(33, 8, 'Kayaks', 'kayaks.jpg', '99574D', 0, '2018-01-04 06:23:55'),
(34, 8, 'Sail Board', 'shailboards.png', '99574D', 0, '2018-01-04 06:24:40'),
(35, 8, 'Cruising SailBoats', 'cruisingsail.png', '99574D', 0, '2018-01-04 06:25:19'),
(36, 9, 'Knife', 'knife.png', '66DCFA', 0, '2018-01-04 06:25:50'),
(37, 9, 'Siythe', 'siyth.png', '66DCFA', 0, '2018-01-04 06:26:15'),
(38, 9, 'BlowTorches', 'blowtorches.png', '66DCFA', 0, '2018-01-04 06:26:48'),
(39, 10, 'Car sheats', 'carsheats.png', 'FF61F4', 0, '2018-01-04 06:27:42'),
(40, 10, 'Stroller', 'babyzone.png', 'FF61F4', 0, '2018-01-04 06:28:47'),
(41, 10, 'Baby Swing', 'swings.png', 'FF61F4', 0, '2018-01-04 06:29:34'),
(42, 10, 'Changing Table', 'changingtable.jpg', 'FF61F4', 0, '2018-01-04 06:30:41'),
(43, 11, 'Games', 'games.jpg', '3B9139', 0, '2018-01-04 06:31:31'),
(44, 11, 'Clothes', 'kidclothes.png', '3B9139', 0, '2018-01-04 06:31:54'),
(45, 11, 'Food', 'kidfood.png', '3B9139', 0, '2018-01-04 06:32:16'),
(46, 12, 'Switch Board', 'switchboards.png', 'FFF670', 0, '2018-01-04 06:32:54'),
(47, 12, 'Wire', 'wirs.png', 'FFF670', 0, '2018-01-04 06:33:13'),
(48, 12, 'Mobile', 'mobile.png', 'FFF670', 0, '2018-01-04 06:33:30'),
(49, 12, 'Laptop', 'laptop.png', 'FFF670', 0, '2018-01-04 06:33:45'),
(50, 12, 'Power Cable', 'powercable.png', 'FFF670', 0, '2018-01-04 06:34:04'),
(51, 15, 'Sun Glasses', 'sunglass.png', 'FFFFFF', 0, '2018-01-04 06:35:30'),
(52, 15, 'Clothes', 'clothes.png', 'FFFFFF', 0, '2018-01-04 06:36:38'),
(53, 15, 'Makeup', 'makeup.png', 'FFFFFF', 0, '2018-01-04 06:37:43'),
(54, 15, 'Tooth Brush', 'toothbrush.png', 'FFFFFF', 0, '2018-01-04 06:38:20'),
(55, 15, 'Ski Gear', 'clothes.png', 'FFFFFF', 0, '2018-01-04 06:39:39');

-- --------------------------------------------------------

--
-- Table structure for table `temporary_user`
--

CREATE TABLE IF NOT EXISTS `temporary_user` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `type` tinyint(4) NOT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `date_of_birth` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `social_id` varchar(255) DEFAULT NULL,
  `social_picture` varchar(255) DEFAULT NULL,
  `user_status` tinyint(4) NOT NULL,
  `is_driving_license` tinyint(1) NOT NULL DEFAULT '0',
  `is_credit_card_approved` tinyint(1) NOT NULL DEFAULT '0',
  `device_type` varchar(255) DEFAULT NULL,
  `device_token` varchar(255) DEFAULT NULL,
  `user_register_type` int(11) NOT NULL DEFAULT '3',
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `rating` varchar(100) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `temporary_user`
--

INSERT INTO `temporary_user` (`id`, `name`, `email`, `password`, `image`, `type`, `gender`, `date_of_birth`, `phone`, `social_id`, `social_picture`, `user_status`, `is_driving_license`, `is_credit_card_approved`, `device_type`, `device_token`, `user_register_type`, `created_date_time`, `rating`) VALUES
(12, 'Admin', 'ravindra.aln@gmail.com', 'f7c3bc1d808e04732adf679965ccc34ca7ae3441', '5a4e0e0364b3b.jpeg', 3, NULL, NULL, '9898989898', NULL, NULL, 0, 0, 0, NULL, NULL, 3, '2018-01-04 11:20:35', '0'),
(13, 'Saumya', 'saumya@gmail.com', '7c222fb2927d828af22f592134e8932480637c0d', '5a4e0ffd4c648.jpeg', 3, NULL, NULL, '8077024283', NULL, NULL, 0, 0, 0, NULL, NULL, 3, '2018-01-04 11:29:01', '0'),
(14, 'Admin', 'ravindra.aln@gmail.com', 'f7c3bc1d808e04732adf679965ccc34ca7ae3441', '5a4e10e0aa9e1.jpeg', 3, NULL, NULL, '9898989898', NULL, NULL, 0, 0, 0, NULL, NULL, 3, '2018-01-04 11:32:48', '0'),
(16, 'Admin', 'ravindra.aln@gmail.com', 'f7c3bc1d808e04732adf679965ccc34ca7ae3441', '5a4e157ada1b1.jpeg', 3, NULL, NULL, '9898989898', NULL, NULL, 0, 0, 0, NULL, NULL, 3, '2018-01-04 11:52:27', '0');

-- --------------------------------------------------------

--
-- Table structure for table `temporary_user_document`
--

CREATE TABLE IF NOT EXISTS `temporary_user_document` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `creditcard` varchar(255) NOT NULL,
  `driving_licence` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Dumping data for table `temporary_user_document`
--

INSERT INTO `temporary_user_document` (`id`, `user_id`, `creditcard`, `driving_licence`, `address`, `description`, `created_date_time`) VALUES
(26, 14, '1234568763678', 'DL45HG241352', 'c-75,sector 10,Noida', 'Users Document Description', '2018-01-04 11:32:48'),
(25, 13, '123456789789465132', '123456789798465132', 'Noida', 'Hello', '2018-01-04 11:29:01'),
(24, 12, '1234568763678', 'DL45HG241352', 'c-75,sector 10,Noida', 'Users Document Description', '2018-01-04 11:20:35'),
(28, 16, '1234568763678', 'DL45HG241352', 'c-75,sector 10,Noida', 'Users Document Description', '2018-01-04 11:52:27');

-- --------------------------------------------------------

--
-- Table structure for table `transction_history`
--

CREATE TABLE IF NOT EXISTS `transction_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `order_by_id` int(11) NOT NULL,
  `amount` varchar(50) NOT NULL,
  `account` varchar(255) NOT NULL,
  `transction_status` tinyint(1) NOT NULL DEFAULT '0',
  `vendor_id` int(11) NOT NULL,
  `rent_days_count` varchar(255) NOT NULL,
  `transction_id` varchar(255) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `transction_history`
--

INSERT INTO `transction_history` (`id`, `order_id`, `order_by_id`, `amount`, `account`, `transction_status`, `vendor_id`, `rent_days_count`, `transction_id`, `created_date_time`) VALUES
(1, 182, 47, '500', 'sdsadsa123132gfdgfdgdfgdf', 1, 70, '7', 'sadsadsad', '2017-12-23 13:19:10'),
(2, 185, 70, '800', 'dsas213123213esdfdsfd', 1, 47, '7', 'q34r23442adasd', '2017-12-23 13:19:10');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `type` tinyint(4) NOT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `date_of_birth` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `social_id` varchar(255) DEFAULT NULL,
  `social_picture` varchar(255) DEFAULT NULL,
  `user_status` tinyint(4) NOT NULL,
  `is_driving_license` tinyint(1) NOT NULL DEFAULT '0',
  `is_credit_card_approved` tinyint(1) NOT NULL DEFAULT '0',
  `device_type` varchar(255) DEFAULT NULL,
  `device_token` varchar(255) DEFAULT NULL,
  `user_register_type` int(11) NOT NULL DEFAULT '3',
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `rating` varchar(100) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=105 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `image`, `type`, `gender`, `date_of_birth`, `phone`, `social_id`, `social_picture`, `user_status`, `is_driving_license`, `is_credit_card_approved`, `device_type`, `device_token`, `user_register_type`, `created_date_time`, `rating`) VALUES
(2, 'Admin', 'dlregonow@gmail.com', 'f7c3bc1d808e04732adf679965ccc34ca7ae3441', 'logo.png', 2, '1', '1979-09-19', '1234567899', NULL, NULL, 1, 1, 1, '', '', 3, '2017-12-19 11:59:16', '1'),
(47, 'Ankita', 'ankita.dhaka09@gmail.com', 'f7c3bc1d808e04732adf679965ccc34ca7ae3441', '5a4fc6bdd7d4c.jpeg', 3, '', '', '123456789', NULL, NULL, 1, 0, 0, '', '', 3, '2017-12-23 16:41:10', '2'),
(100, 'sachin', 'sachin.siwal@gmail.com', '7c222fb2927d828af22f592134e8932480637c0d', '5a4e12e69e86b.jpeg', 3, NULL, NULL, '', NULL, NULL, 0, 0, 0, NULL, NULL, 3, '2018-01-04 11:43:08', '0'),
(101, 'Akash', 'ravindra.aln@gmail.com', 'f7c3bc1d808e04732adf679965ccc34ca7ae3441', '5a4e21361b6ec.jpeg', 3, NULL, NULL, '123456788899', NULL, NULL, 0, 0, 0, NULL, NULL, 3, '2018-01-04 11:53:08', '0'),
(102, 'Ravindra', 'ravindra.4736@gmail.com', 'f7c3bc1d808e04732adf679965ccc34ca7ae3441', '5a4e1a977658e.jpeg', 3, NULL, NULL, '00007453545', NULL, NULL, 0, 0, 0, NULL, NULL, 3, '2018-01-04 11:58:01', '0'),
(103, 'Ankita Dhaka', 'ankita.dhaka091@gmail.com', '', NULL, 3, NULL, NULL, NULL, '1670581839668416', 'https://scontent.xx.fbcdn.net/v/t1.0-1/p50x50/24991578_1641593455900588_772669502303455700_n.jpg?oh=deb1ca23ae741bdb74a55e53c8ccf548&oe=5AF175EF', 0, 0, 0, NULL, NULL, 1, '2018-01-04 12:26:32', '0');

-- --------------------------------------------------------

--
-- Table structure for table `user_document`
--

CREATE TABLE IF NOT EXISTS `user_document` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `creditcard` varchar(255) DEFAULT NULL,
  `driving_licence` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=91 ;

--
-- Dumping data for table `user_document`
--

INSERT INTO `user_document` (`id`, `user_id`, `creditcard`, `driving_licence`, `address`, `description`, `created_date_time`) VALUES
(44, 47, 'TR4563783TY756', 'UP16TY3452', 'C-75 Sector-10 Noida   ', 'I am A user', '2017-12-23 16:41:10'),
(89, 103, NULL, NULL, NULL, NULL, '2018-01-04 12:26:32'),
(90, 104, NULL, NULL, NULL, NULL, '2018-01-04 12:40:48'),
(88, 102, 'TR4563783TY756', 'UP16TY3452', 'C-75 Sector-10 Noida ', 'I am A user', '2018-01-04 11:58:01'),
(87, 101, 'TR4563783TY756', 'UP16TY3452', 'C-75 Sector-10 Noida ', 'I am A user', '2018-01-04 11:53:08'),
(86, 100, '123456789987654333', 'wetyyqtqreqeqeqeqeq334', 'noida sector 12', 'i am owner of our organization.', '2018-01-04 11:43:08');

-- --------------------------------------------------------

--
-- Table structure for table `user_register_type`
--

CREATE TABLE IF NOT EXISTS `user_register_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `register_type_name` varchar(255) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `user_register_type`
--

INSERT INTO `user_register_type` (`id`, `register_type_name`, `created_date_time`) VALUES
(1, 'Facebook', '2017-12-20 10:55:25'),
(2, 'Gmail', '2017-12-20 10:55:25'),
(3, 'App User', '2017-12-20 10:56:09');

-- --------------------------------------------------------

--
-- Table structure for table `user_verify`
--

CREATE TABLE IF NOT EXISTS `user_verify` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `otp` int(11) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=31 ;

--
-- Dumping data for table `user_verify`
--

INSERT INTO `user_verify` (`id`, `email`, `otp`, `created_date_time`) VALUES
(26, 'saumya@gmail.com', 9635, '2018-01-04 11:29:01');

-- --------------------------------------------------------

--
-- Table structure for table `valid_for`
--

CREATE TABLE IF NOT EXISTS `valid_for` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `valid_for_name` varchar(100) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `valid_for`
--

INSERT INTO `valid_for` (`id`, `valid_for_name`, `created_date_time`) VALUES
(1, 'All Users', '2017-12-20 07:36:53'),
(2, 'Particular User', '2017-12-20 07:37:21');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
