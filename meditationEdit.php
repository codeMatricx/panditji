 <?php 
$id = $_GET['meditationId'];
include "database.php";
?>
<?php
if(isset($_POST["upd"]))
{
  //image
        if(empty($_FILES['image']['name']))
        {
        $image = $_POST['image_first'];
        }
        else
        {
        $target_dir = "assets/img/uploads/meditation/";
        $target_file = $target_dir . basename($_FILES["image"]["name"]);
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
        move_uploaded_file($_FILES["image"]["tmp_name"], $target_file);
        $image =  $_FILES["image"]["name"];
        $uploadOk = 1;
        }
//video
        if(empty($_FILES['video']['name']))
        {
        $video = $_POST['video_first'];
        }
        else
        {
        $target_dir_video = "assets/img/uploads/meditation/";
        $target_file_video = $target_dir_video . basename($_FILES['video']["name"]);
        $videoFileType = pathinfo($target_file_video,PATHINFO_EXTENSION);
        move_uploaded_file($_FILES["video"]["tmp_name"], $target_file_video);
        $video = $_FILES['video']["name"];
        $uploadOk = 1;
        }
//audio
        if(empty($_FILES['audio']['name']))
        {
        $audio = $_POST['audio_first'];
        }
        else
        {
        $target_dir_audio = "assets/img/uploads/meditation/";
        $target_file_audio = $target_dir_audio . basename($_FILES['audio']["name"]);
        $audioFileType = pathinfo($target_file_audio,PATHINFO_EXTENSION);
        move_uploaded_file($_FILES["audio"]["tmp_name"], $target_file_audio);
        $audio = $_FILES['audio']["name"];
        $uploadOk = 1;
        }

// $userId=$_POST["userId"];
$title=$_POST["title"];
$meditation_list_id=$_POST["meditation_list_id"];
// $subCategoryId=$_POST["subCategoryId"];
// $price=$_POST["price"];
$description=$_POST["description"];
$status = 1;
            
      $sql = "UPDATE meditation SET meditation_list_id='$meditation_list_id',title='$title',description='$description',image='$image',audio='$audio',video='$video' WHERE id='$id'";
      if ($conn->query($sql) === TRUE) 
      {
        header("location:meditation.php");
      } 
      else 
      {
      echo "Error: " . $sql . "<br>" . $conn->error;
      }
}
?>
<?php require('include/head.php'); ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
 <?php include "include/header.php";?> 
  <?php include "include/left_sidebar.php";?>
       <div class="content-wrapper">
            <section class="content-header">
              <h1>
              Update Meditation     
              </h1>
            </section>
            <section class="content">
        <div class="box">
        <div class="box-body table-responsive table-scroll-y">
        <form method="post" enctype="multipart/form-data">
                  
            <table id="example1" class="table table-bordered table-striped">
            <table  class="table table-bordered table-striped">
				<?php 
				$sql_update = "SELECT * from meditation WHERE id='$id'";
				$result_update = $conn->query($sql_update);
				if ($result_update->num_rows>0)
				{
				$meditationUpdate = $result_update->fetch_assoc();
				?>        
            <tbody>
              <tr>
                <th>Meditation Name</th>
                <td>
                  <select name="meditation_list_id">
                    <?php 
                  $sql = "SELECT id,meditationName from meditation_list";
                  $result = $conn->query($sql);
                  if ($result->num_rows>0)
                  {
                  while($meditationList = $result->fetch_assoc())
                  {
                  ?>
                    <option value="<?php echo $meditationList['id'];?>"><?php echo $meditationList['meditationName'];?>
                    </option>
                     <?php } }?>
                  </select>
                 </td>
            </tr>
            <tr>
              <th>Step</th>
              <td><input type="text" name="title" value="<?php echo $meditationUpdate['title'];?>" placeholder="Add Step"></td>
            </tr>
                    
            <tr>
               <th>Image</th>
               <td><img src="assets/img/uploads/meditation/<?php echo $meditationUpdate['image']; ?>" class="img-responsive" style="width:30px; height:30px" ></td> 
              </tr>
              <tr>
              <th>Change Image</th>
                   <td>
                   <input id="newimage" type="file" name="image">
                   <input type = "hidden" name = "image_first" id = "image_first" value = "<?php  echo $meditationUpdate['image'];?>">
               </td>
               </tr>
               
              <tr>
               <th>Video</th>
               <td><video  width="30px" height="30px" controls><source src="assets/img/uploads/meditation/<?php echo $meditationUpdate['video']; ?>" class="img-responsive" style="width:30px; height:30px" type="video/mp4"></video></td> 
              </tr>
              <tr>
              <th>Change Video</th>
                   <td>
                   <input id="newvideo" type="file" name="video">
                   <input type = "hidden" name = "video_first" id = "video_first" value = "<?php  echo $meditationUpdate['video'];?>">
               </td>
               </tr>
               
            <tr>
               <th>Audio</th>
               <td><audio width="30px" height="30px" controls><source src="assets/img/uploads/meditation/<?php echo $meditationUpdate['audio']; ?>" class="img-responsive" style="width:30px; height:30px" type="audio/ogg"></audio></td> 
              </tr>
              <tr>
              <th>Change Audio</th>
                   <td>
                   <input id="newaudio" type="file" name="audio">
                   <input type = "hidden" name = "audio_first" id = "audio_first" value = "<?php  echo $meditationUpdate['audio'];?>">
               </td>
               </tr>
            <tr>
              <th>Meditation</th>
              <td>
                <textarea class="tinymce" id="mytextarea" name="description" placeholder="Add Katha" value="<?php echo $meditationUpdate['description'];?>"></textarea>
                <!-- <input type="text" name="description" value="<?php echo $meditationUpdate['description'];?>" placeholder="Add Meditation"> -->
              </td>
            </tr>
            
            </tbody>
            <?php }?>             
            </table>
            </table>
            <a href="meditation.php" style="color: #fff;"><button type="button" class="btn" style="margin-top: 10px" >Back</button></a>
            <button type="submit" class="btn   pull-right" name="upd" style="margin-top: 10px" >UPDATE</button>
            
        </form>
        </div>
        </div>
        </div>
  <?php include "include/footer.php" ;?>
  <?php include "include/right_sidebar.php" ;?>  
</div>
<?php include "include/footer_script.php" ;?>
<script src="jscolor.js"></script>
</body>
</html>
