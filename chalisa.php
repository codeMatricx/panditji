 <?php 
 include "database.php";
?>

<?php require('include/head.php'); ?>
<body onload="initialize()" class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
<?php include "include/header.php";?>
<?php include "include/left_sidebar.php";?>
<div class="content-wrapper">
<section class="content-header">
<h1>
Chalisa List
</h1>
<ol class="breadcrumb">
<li><a href="addChalisa.php"><button type="button" class="btn btn-block " style="margin-top: -5px;">Add Chalisa</button></a></li>
</ol>
</section>
<section class="content">
<div class="box">
<div class="box-header">
<h3 class="box-title">Chalisa Table With Full Features</h3>
</div>
            <!-- /.box-header -->
            <div class="box-body table-responsive table-scroll-y">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                <th>S.NO</th>
                <th>Chalisa Name</th>
                <th>Chalisa</th>
                <th>Image</th>
                <th>Video</th>
                <th>Audio</th>
                <th>Posted Date</th>
                <!-- <th>Status </th> -->
                <th>Action</th>
                  <th><input name="select-all" id="checkall" onClick="check_uncheck_checkbox(this.checked);" value="check_all" type="checkbox"></th>
                </tr>
                </thead>
                    <tbody>
                        <?php 
                            $sql = "SELECT c.id,c.title,c.description,c.image,c.audio,c.video,c.created_date_time,cl.chalisaName FROM chalisa AS c LEFT JOIN chalisa_list AS cl ON c.chalisa_list_id=cl.id";
                        $result = $conn->query($sql);
                        if ($result->num_rows>0)
                        {
                        $serial=0;
                        while($chalisaList = $result->fetch_assoc())
                        {
                        $desc=$chalisaList['description'];
                        $serial++;
                        ?>
                    <tr id="<?php  echo $chalisaList['id'];?>">
                        <td><?php echo $serial; ?></td>
                        <td><?php  echo $chalisaList['chalisaName'];?></td>
                        <!-- <td><?php  echo $kathaList['title'];?></td> -->
                        <td><?php  echo substr ($desc, 0, 20);?></td> 
                        <td><img src="assets/img/uploads/chalisa/<?php echo $chalisaList['image']; ?>" class="img-responsive" style="width:30px; height:30px" ></td>
                        <td><video  width="30px" height="30px" controls><source src="assets/img/uploads/chalisa/<?php echo $chalisaList['video']; ?>" class="img-responsive" style="width:30px; height:30px" type="video/mp4"></video></td>
                        <td><audio width="30px" height="30px" controls><source src="assets/img/uploads/chalisa/<?php echo $chalisaList['audio']; ?>" class="img-responsive" style="width:30px; height:30px" type="audio/ogg"></audio></td>
                        <td><?php  echo $chalisaList['created_date_time'];?></td>
                       
                    <td >
                    <a href="chalisaEdit.php?chalisaId=<?php echo $chalisaList['id']; ?>" style="cursor: pointer;">
                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i> </a>/
                    <a href="chalisaView.php?chalisaId=<?php echo $chalisaList['id']; ?>" style="cursor: pointer;">
                    <i class="fa fa-eye" aria-hidden="true"></i> </a>
                    </td>
                    <td><input   type="checkbox" value="<?php echo $chalisaList['id'];?>" name="action" id="checkbox-1"></td>
                    </tr> 
                    <?php 
                    }
                    } 
                    ?>
                    </tbody>
              </table>
<button class="btn btn-danger" onclick="deleteChalisa()">Delete</button>
</div>
</div>
</div>
<?php include "include/footer.php" ;?>
<?php include "include/right_sidebar.php" ;?>
</div>
<?php include "include/footer_script.php" ;?>
<script src='http://maps.googleapis.com/maps/api/js?v=3&sensor=false&amp;libraries=places&key=AIzaSyAL1eAba9HoD7qszOJ-ggOvZvbq-TvXDys'></script>
<script src="js/script.js"> </script>
</body>
</html>
