 <?php 
 $productId = $_GET['productId'];
include "database.php";
?>

<?php require('include/head.php'); ?>
<body onload="initialize()" class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
<?php include "include/header.php";?>
<?php include "include/left_sidebar.php";?>
<div class="content-wrapper">
<section class="content-header">
<h1>
Product Details   
</h1>
</section>
<section class="content">
<div class="box">
<div class="box-header">
</div>
            <!-- /.box-header -->
<div class="box-body table-responsive table-scroll-y">
  <table  class="table table-bordered table-striped">
      <?php 
          $sql = "SELECT p.status,p.id,p.title,c.categoryName,sc.subCategoryName,p.price,p.description,p.created_date_time,i.post_image AS product_image,v.product_video,a.product_audio 
          from product AS p 
          INNER JOIN category AS c ON p.categoryId=c.id 
          INNER JOIN sub_category AS sc ON p.subCategoryId=sc.id 
          LEFT JOIN product_image_gallery AS i ON p.id=i.post_id 
          LEFT JOIN product_video_gallery AS v ON p.id=v.productId 
          LEFT JOIN product_audio_gallery AS a ON p.id=a.productId  
          WHERE p.id='$productId'";
          $result = $conn->query($sql);
          if ($result->num_rows>0)
          {
          $productview = $result->fetch_assoc();
        ?>
    <tbody>
      <tr>
        <th>Title</th>
        <td><?php echo $productview['title'];?></td>
      </tr>
      <tr>
        <th>Category</th>
        <td><?php echo $productview['categoryName'];?></td>
      </tr>
      <tr>
        <th>SubCategory</th>
        <td><?php echo $productview['subCategoryName'];?></td>
      </tr>
      <tr>
        <th>Price</th>
        <td><?php echo $productview['price'];?></td>
      </tr>
      <tr>
        <th>Description</th>
        <td><?php echo $productview['description'];?></td>
      </tr>
      <tr>
        <th>Image</th>
        <td><img src="assets/img/uploads/products/<?php echo $productview['product_image']; ?>" class="img-responsive" style="width:30px; height:30px" ></td>
      </tr>
      <tr>
        <th>Video</th>
        <td><video  width="100px" height="100px" controls><source src="assets/img/uploads/products/<?php echo $productview['product_video']; ?>" class="img-responsive" style="width:80px; height:80px" type="video/mp4"></video></td>
      </tr>
      <tr>
        <th>Audio</th>
        <td><audio width="30px" height="30px" controls><source src="assets/img/uploads/products/<?php echo $productview['product_audio']; ?>" class="img-responsive" style="width:30px; height:30px" type="audio/ogg"></audio></td>
      </tr>
      <tr>
        <th>Posted Date</th>
        <td><?php echo $productview['created_date_time'];?></td>
      </tr>

  </tbody>
  <?php } ?>
  </table>
  <a href="product.php" style="color: #fff;"><button type="button" class="btn" style="margin-top: 10px" >Back</button></a>           
</div>
</div>
</div>
<?php include "include/footer.php" ;?>
<?php include "include/right_sidebar.php" ;?>
</div>
<?php include "include/footer_script.php" ;?>
<script src='http://maps.googleapis.com/maps/api/js?v=3&sensor=false&amp;libraries=places&key=AIzaSyAL1eAba9HoD7qszOJ-ggOvZvbq-TvXDys'></script>
<script src="js/script.js"> </script>
</body>
</html>
