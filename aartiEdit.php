 <?php 
$aartiId = $_GET['aartiId'];
include "database.php";
?>
<?php
if(isset($_POST["upd"]))
{
  //image
        if(empty($_FILES['image']['name']))
        {
        $image = $_POST['image_first'];
        }
        else
        {
        $target_dir = "assets/img/uploads/aarti/";
        $target_file = $target_dir . basename($_FILES["image"]["name"]);
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
        move_uploaded_file($_FILES["image"]["tmp_name"], $target_file);
        $image =  $_FILES["image"]["name"];
        //print_r($image);exit;
        $uploadOk = 1;
        }
//video
        if(empty($_FILES['video']['name']))
        {
        $video = $_POST['video_first'];
        }
        else
        {
        $target_dir_video = "assets/img/uploads/aarti/";
        $target_file_video = $target_dir_video . basename($_FILES['video']["name"]);
        $videoFileType = pathinfo($target_file_video,PATHINFO_EXTENSION);
        move_uploaded_file($_FILES["video"]["tmp_name"], $target_file_video);
        $video = $_FILES['video']["name"];
        $uploadOk = 1;
        }
//audio
        if(empty($_FILES['audio']['name']))
        {
        $audio = $_POST['audio_first'];
        }
        else
        {
        $target_dir_audio = "assets/img/uploads/aarti/";
        $target_file_audio = $target_dir_audio . basename($_FILES['audio']["name"]);
        $audioFileType = pathinfo($target_file_audio,PATHINFO_EXTENSION);
        move_uploaded_file($_FILES["audio"]["tmp_name"], $target_file_audio);
        $audio = $_FILES['audio']["name"];
        $uploadOk = 1;
        }

 $godName=$_POST["godName"];
 $aartiName=$_POST["aartiName"];
$description=$_POST["description"];
$status = 1;
            
      $sql = "UPDATE aarti SET aarti_list_id='$godName',aarti_sub_list_id='$aartiName',description='$description',image='$image',audio='$audio',video='$video' WHERE id='$aartiId'";
      if ($conn->query($sql) === TRUE) 
      {
        header("location:aarti.php");
      } 
      else 
      {
      echo "Error: " . $sql . "<br>" . $conn->error;
      }
}
?>
<?php require('include/head.php'); ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
 <?php include "include/header.php";?> 
  <?php include "include/left_sidebar.php";?>
       <div class="content-wrapper">
            <section class="content-header">
              <h1>
              Update Aarti     
              </h1>
            </section>
            <section class="content">
        <div class="box">
        <div class="box-body table-responsive table-scroll-y">
        <form method="post" enctype="multipart/form-data">
                  
            <table id="example1" class="table table-bordered table-striped">
            <table  class="table table-bordered table-striped">
				<?php 
				// $sql_update = "SELECT p.title,p.description,i.yoga_image,v.yoga_video,a.yoga_audio from yoga AS p LEFT JOIN yoga_image_gallery AS i ON p.id=i.yogaId LEFT JOIN yoga_video_gallery AS v ON p.id=v.yogaId LEFT JOIN yoga_audio_gallery AS a ON p.id=a.yogaId WHERE p.id='$aartiId'";
        $sql_update="SELECT a.id,asl.id AS aartiNameId,al.id AS godNameId,a.status,al.aartiName AS godName,asl.aartiSubName AS aartiName,a.title,a.description AS Aarti,a.image,a.audio,a.video,a.created_date_time from aarti AS a  
        INNER JOIN aarti_sub_list AS asl ON a.aarti_sub_list_id=asl.id 
        INNER JOIN aarti_list AS al ON a.aarti_list_id=al.id WHERE a.id='$aartiId'";
				$result_update = $conn->query($sql_update);
				if ($result_update->num_rows>0)
				{
				$aartiUpdate = $result_update->fetch_assoc();
				?>        
            <tbody>
              <tr>
                
               <th>God Name</th>
               <td>
                 <select name="godName">
                  <?php
                  $sql_god="SELECT id,aartiName from aarti_list ";
                  $result_god = $conn->query($sql_god);
                  if ($result_god->num_rows>0)
                  {
                  while($aartiGod = $result_god->fetch_assoc())
                  {
                  ?>
                   <option value="<?php echo $aartiGod['id'];?>"><?php echo $aartiGod['aartiName'];?></option>
                   <?php }}?>
                 </select>
               </td>
               
              </tr>
              <tr>
                <th>Aarti Name</th>
                <td>
                 <select name="aartiName">
                  <?php
                  $sql_name="SELECT id,aartiSubName from aarti_sub_list ";
                  $result_name = $conn->query($sql_name);
                  if ($result_name->num_rows>0)
                  {
                   while($aartiName = $result_name->fetch_assoc())
                   {
                  ?>
                   <option value="<?php echo $aartiName['id'];?>"><?php echo $aartiName['aartiSubName'];?></option>
                   <?php } }?>
                 </select>
                </td>
              </tr>
                    
            <tr>
               <th>Aarti Image</th>
               <td><img src="assets/img/uploads/aarti/<?php echo $aartiUpdate['image']; ?>" class="img-responsive" style="width:30px; height:30px" ></td> 
              </tr>
              <tr>
              <th>Change Image</th>
              <td>
                   <input id="newimage" type="file" name="image">
                   <input type = "hidden" name = "image_first" id = "image_first" value = "<?php  echo $aartiUpdate['image'];?>">
               </td>
               </tr>
               
              <tr>
               <th>Aarti Video</th>
               <td><video  width="30px" height="30px" controls><source src="assets/img/uploads/aarti/<?php echo $aartiUpdate['video']; ?>" class="img-responsive" style="width:30px; height:30px" type="video/mp4"></video></td> 
              </tr>
              <tr>
              <th>Change Video</th>
                   <td>
                   <input id="newvideo" type="file" name="video">
                   <input type = "hidden" name = "video_first" id = "video_first" value = "<?php  echo $aartiUpdate['video'];?>">
               </td>
               </tr>
               
            <tr>
               <th>Aarti Audio</th>
               <td><audio width="30px" height="30px" controls><source src="assets/img/uploads/aarti/<?php echo $aartiUpdate['audio']; ?>" class="img-responsive" style="width:30px; height:30px" type="audio/ogg"></audio></td> 
              </tr>
              <tr>
              <th>Change Audio</th>
                   <td><!-- <label for="newaudio" class="btn text-muted text-center btn-success" style="width:20%;margin-top: -4px;padding: 12px;">Audio</label> -->
                   <input id="newaudio" type="file" name="audio">
                   <input type = "hidden" name = "audio_first" id = "audio_first" value = "<?php  echo $aartiUpdate['audio'];?>">
               </td>
               </tr>
            <tr>
              <th>Aarti</th>
              <td>
                <textarea class="tinymce" id="mytextarea" name="description" placeholder="Add Description" value="<?php echo $aartiUpdate['Aarti'];?>"></textarea>
                <!-- <input type="text" name="description" value="<?php echo $aartiUpdate['Aarti'];?>" placeholder="Add Description"> --></td>
            </tr>
            
            </tbody>
            <?php }?>             
            </table>
            </table>
            <a href="aarti.php" style="color: #fff;"><button type="button" class="btn" style="margin-top: 10px" >Back</button></a>
            <button type="submit" class="btn   pull-right" name="upd" style="margin-top: 10px" >UPDATE</button>
            
        </form>
        </div>
        </div>
        </div>
  <?php include "include/footer.php" ;?>
  <?php include "include/right_sidebar.php" ;?>  
</div>
<?php include "include/footer_script.php" ;?>
<script src="jscolor.js"></script>
</body>
</html>
