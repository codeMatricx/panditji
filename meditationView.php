 <?php 
 $id = $_GET['meditationId'];
include "database.php";
?>

<?php require('include/head.php'); ?>
<body onload="initialize()" class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
<?php include "include/header.php";?>
<?php include "include/left_sidebar.php";?>
<div class="content-wrapper">
<section class="content-header">
<h1>
Meditation Details   
</h1>
</section>
<section class="content">
<div class="box">
<div class="box-header">
</div>
            <!-- /.box-header -->
<div class="box-body table-responsive table-scroll-y">
  <table  class="table table-bordered table-striped">
      <?php 
          $sql = "SELECT m.id,m.title,m.description,m.image,m.audio,m.video,m.created_date_time,ml.meditationName FROM meditation AS m LEFT JOIN meditation_list AS ml ON m.meditation_list_id=ml.id WHERE m.id='$id'";
          $result = $conn->query($sql);
          if ($result->num_rows>0)
          {
          $meditationView = $result->fetch_assoc();
        ?>
    <tbody>
      <tr>
        <th>Meditation Name</th>
        <td><?php echo $meditationView['meditationName'];?></td>
      </tr>
      <tr>
        <th>Step</th>
        <td><?php echo $meditationView['title'];?></td>
      </tr>
      
      <tr>
        <th>Meditation</th>
        <td><?php echo $meditationView['description'];?></td>
      </tr>
      <tr>
        <th>Image</th>
        <td><img src="assets/img/uploads/meditation/<?php echo $meditationView['image']; ?>" class="img-responsive" style="width:30px; height:30px" ></td>
      </tr>
      <tr>
        <th>Video</th>
        <td><video  width="100px" height="100px" controls><source src="assets/img/uploads/meditation/<?php echo $meditationView['video']; ?>" class="img-responsive" style="width:80px; height:80px" type="video/mp4"></video></td>
      </tr>
      <tr>
        <th>Audio</th>
        <td><audio width="30px" height="30px" controls><source src="assets/img/uploads/meditation/<?php echo $meditationView['audio']; ?>" class="img-responsive" style="width:30px; height:30px" type="audio/ogg"></audio></td>
      </tr>
      <tr>
        <th>Posted Date</th>
        <td><?php echo $meditationView['created_date_time'];?></td>
      </tr>

  </tbody>
  <?php } ?>
  </table> 
  <a href="meditation.php" style="color: #fff;"><button type="button" class="btn" style="margin-top: 10px" >Back</button></a>          
</div>
</div>
</div>
<?php include "include/footer.php" ;?>
<?php include "include/right_sidebar.php" ;?>
</div>
<?php include "include/footer_script.php" ;?>
<script src='http://maps.googleapis.com/maps/api/js?v=3&sensor=false&amp;libraries=places&key=AIzaSyAL1eAba9HoD7qszOJ-ggOvZvbq-TvXDys'></script>
<script src="js/script.js"> </script>
</body>
</html>
