 <?php 
$productId = $_GET['productId'];
include "database.php";
?>
<?php
if(isset($_POST["upd"]))
{
  //image
        if(empty($_FILES['image']['name']))
        {
        $image = $_POST['image_first'];
        }
        else
        {
        $target_dir = "assets/img/uploads/products/";
        $target_file = $target_dir . basename($_FILES["image"]["name"]);
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
        move_uploaded_file($_FILES["image"]["tmp_name"], $target_file);
        $image =  $_FILES["image"]["name"];
        //print_r($image);exit;
        $uploadOk = 1;
        }
//video
        if(empty($_FILES['video']['name']))
        {
        $video = $_POST['video_first'];
        }
        else
        {
        $target_dir_video = "assets/img/uploads/products/";
        $target_file_video = $target_dir_video . basename($_FILES['video']["name"]);
        $videoFileType = pathinfo($target_file_video,PATHINFO_EXTENSION);
        move_uploaded_file($_FILES["video"]["tmp_name"], $target_file_video);
        $video = $_FILES['video']["name"];
        $uploadOk = 1;
        }
//audio
        if(empty($_FILES['audio']['name']))
        {
        $audio = $_POST['audio_first'];
        }
        else
        {
        $target_dir_audio = "assets/img/uploads/products/";
        $target_file_audio = $target_dir_audio . basename($_FILES['audio']["name"]);
        $audioFileType = pathinfo($target_file_audio,PATHINFO_EXTENSION);
        move_uploaded_file($_FILES["audio"]["tmp_name"], $target_file_audio);
        $audio = $_FILES['audio']["name"];
        $uploadOk = 1;
        }

// $userId=$_POST["userId"];
$title=$_POST["title"];
$categoryId=$_POST["categoryId"];
$subCategoryId=$_POST["subCategoryId"];
$price=$_POST["price"];
$description=$_POST["description"];
$status = 1;
            
      $sql = "UPDATE product SET title='$title',categoryId='$categoryId',subCategoryId='$subCategoryId',price='$price',description='$description' WHERE id='$productId'";
      if ($conn->query($sql) === TRUE) 
      {
        //image
				
        if(!empty($productId) && !empty($image))
        {  
          $sql_image = "SELECT * FROM product_image_gallery WHERE post_id='$productId'";
          //print_r($sql_image);exit;
          $result_image = $conn->query($sql_image);
          if ($result_image->num_rows>0)
          {
            $product_image = $result_image->fetch_assoc();

            //$post=$product_image['post_id'];
            $sql_image_update = "UPDATE product_image_gallery SET post_image='$image' WHERE post_id='$productId'";
            $product_image_update=$conn->query($sql_image_update);
          }
          else
          {
            $sql_image = "INSERT INTO product_image_gallery (post_id,post_image)
            VALUES ('$productId','$image')";
            $product_image=$conn->query($sql_image);
          }
        }
	        
          //video
            if(!empty($productId) && !empty($video))
            { 
            $sql_video = "SELECT * FROM product_video_gallery WHERE productId='$productId'";
            $result_video = $conn->query($sql_video);
            if ($result_video->num_rows>0)
              {
                $product_video = $result_video->fetch_assoc();
                $sql_video_update = "UPDATE product_video_gallery SET product_video='$video' WHERE productId='$productId'";
                $product_video_update=$conn->query($sql_video_update);
	            }
	            else
	            {
      					$sql_video = "INSERT INTO product_video_gallery (productId,product_video)
      					VALUES ('$productId','$video')";
      					$product_video=$conn->query($sql_video);
	            }
             }
                //audio
       if(!empty($productId) && !empty($audio))
       {
					$sql_audio = "SELECT * FROM product_audio_gallery WHERE productId='$productId'";
					$result_audio = $conn->query($sql_audio);
					if ($result_audio->num_rows>0)
					{
            $product_audio = $result_audio->fetch_assoc();
            $sql_audio_update = "UPDATE product_audio_gallery SET product_audio='$audio' WHERE productId='$productId'";
            $product_audio_update=$conn->query($sql_audio_update);
            header("location:product.php");
          }
            else
            {
              $sql_audio = "INSERT INTO product_audio_gallery (productId,product_audio)
              VALUES ('$productId','$audio')";
              $product_audio=$conn->query($sql_audio);
            }
        }
        header("location:product.php");
      } 
      else 
      {
      echo "Error: " . $sql . "<br>" . $conn->error;
      }
}
?>
<?php require('include/head.php'); ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
 <?php include "include/header.php";?> 
  <?php include "include/left_sidebar.php";?>
       <div class="content-wrapper">
            <section class="content-header">
              <h1>
              Update Product     
              </h1>
            </section>
            <section class="content">
        <div class="box">
        <div class="box-body table-responsive table-scroll-y">
        <form method="post" enctype="multipart/form-data">
                  
            <table id="example1" class="table table-bordered table-striped">
            <table  class="table table-bordered table-striped">
				<?php 
				$sql_update = "SELECT p.title,p.price,p.description,i.post_image,v.product_video,a.product_audio from product AS p LEFT JOIN product_image_gallery AS i ON p.id=i.post_id LEFT JOIN product_video_gallery AS v ON p.id=v.productId LEFT JOIN product_audio_gallery AS a ON p.id=a.productId WHERE p.id='$productId'";
				$result_update = $conn->query($sql_update);
				if ($result_update->num_rows>0)
				{
				$product_update = $result_update->fetch_assoc();
				?>        
            <tbody>
              <tr>
              <th>Title</th>
              <td><input type="text" name="title" value="<?php echo $product_update['title'];?>" placeholder="Add Title"></td>
              </tr>
            <tr>
            <th>Category</th>
              <td>
              <select name="categoryId" style="margin-left:0px;">
              <?php 
              $sql = "SELECT * from category";
              $result = $conn->query($sql);
              if ($result->num_rows>0)
              {
               while($category = $result->fetch_assoc())
              {
              ?>
              <option value="<?php echo $category['id'];?>" selected disable><?php echo $category['categoryName'];?></option>   
              <?php 
              } }
              ?>       
              </select>
            </td> 
            </tr>
            <tr>
              <th>Sub Category</th>
              <td>
              <select name="subCategoryId" style="margin-left:0px;">
              <?php 
              $sqlSub = "SELECT * from sub_category";
              $resultSub = $conn->query($sqlSub);
              if ($resultSub->num_rows>0)
              {
              while($subCategory = $resultSub->fetch_assoc())
              {
              ?>
              <option value="<?php echo $subCategory['id'];?>" selected disable><?php echo $subCategory['subCategoryName'];?></option>   
              <?php 
              } }
              ?>       
              </select>
            </td> 
            </tr>
                  
            <tr>
               <th>Product Image</th>
               <td><img src="assets/img/uploads/products/<?php echo $product_update['post_image']; ?>" class="img-responsive" style="width:30px; height:30px" ></td> 
              </tr>
              <tr>
              <th>Change Image</th>
                   <td><!-- <label for="newimage" class="btn text-muted text-center btn-success" style="width:20%;margin-top: -4px;padding: 12px;">image</label> -->
                   <input id="newimage" type="file" name="image">
                   <input type = "hidden" name = "image_first" id = "image_first" value = "<?php  echo $product_update['post_image'];?>">
               </td>
               </tr>
               
              <tr>
               <th>Product Video</th>
               <td><video  width="30px" height="30px" controls><source src="assets/img/uploads/products/<?php echo $product_update['product_video']; ?>" class="img-responsive" style="width:30px; height:30px" type="video/mp4"></video></td> 
              </tr>
              <tr>
              <th>Change Video</th>
                   <td><!-- <label for="newvideo" class="btn text-muted text-center btn-success" style="width:20%;margin-top: -4px;padding: 12px;">Video</label> -->
                   <input id="newvideo" type="file" name="video">
                   <input type = "hidden" name = "video_first" id = "video_first" value = "<?php  echo $product_update['product_video'];?>">
               </td>
               </tr>
               
            <tr>
               <th>Product Audio</th>
               <td><audio width="30px" height="30px" controls><source src="assets/img/uploads/products/<?php echo $product_update['product_audio']; ?>" class="img-responsive" style="width:30px; height:30px" type="audio/ogg"></audio></td> 
              </tr>
              <tr>
              <th>Change Audio</th>
                   <td><!-- <label for="newaudio" class="btn text-muted text-center btn-success" style="width:20%;margin-top: -4px;padding: 12px;">Audio</label> -->
                   <input id="newaudio" type="file" name="audio">
                   <input type = "hidden" name = "audio_first" id = "audio_first" value = "<?php  echo $product_update['product_audio'];?>">
               </td>
               </tr>
              
            <tr>
              <th>Price</th>
              <td><input type="text" name="price" value="<?php echo $product_update['price'];?>" placeholder="Add Price"></td>
            </tr>
            <tr>
              <th>Description</th>
              <td><input type="text" name="description" value="<?php echo $product_update['description'];?>" placeholder="Add Description"></td>
            </tr>
            
            </tbody>
            <?php }?>             
            </table>
            </table>
            <a href="product.php" style="color: #fff;"><button type="button" class="btn" style="margin-top: 10px" >Back</button></a>
            <button type="submit" class="btn   pull-right" name="upd" style="margin-top: 10px" >UPDATE</button>
            
        </form>
        </div>
        </div>
        </div>
  <?php include "include/footer.php" ;?>
  <?php include "include/right_sidebar.php" ;?>  
</div>
<?php include "include/footer_script.php" ;?>
<script src="jscolor.js"></script>
</body>
</html>
