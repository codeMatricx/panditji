-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 09, 2018 at 05:52 AM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 7.0.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `panditji`
--

-- --------------------------------------------------------

--
-- Table structure for table `about_help`
--

CREATE TABLE `about_help` (
  `id` int(11) NOT NULL,
  `desription` varchar(255) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `about_help`
--

INSERT INTO `about_help` (`id`, `desription`, `created_date_time`) VALUES
(1, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has su', '2017-12-23 13:55:01');

-- --------------------------------------------------------

--
-- Table structure for table `admin_commission`
--

CREATE TABLE `admin_commission` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `commission_type_id` int(11) NOT NULL,
  `commission_value` varchar(50) NOT NULL,
  `commission_status` tinyint(4) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_commission`
--

INSERT INTO `admin_commission` (`id`, `user_id`, `commission_type_id`, `commission_value`, `commission_status`, `created_date_time`) VALUES
(1, 70, 1, '10', 1, '2017-12-29 06:57:14'),
(2, 70, 2, '70', 0, '2017-12-29 06:57:28');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(255) NOT NULL,
  `categoryName` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `category_status` tinyint(4) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `categoryName`, `image`, `category_status`, `created_date_time`) VALUES
(16, 'Yoga', 'yoga.png', 1, '2018-01-08 07:25:57'),
(17, 'Arti', 'Aarti.jpg', 1, '2018-01-08 07:26:10'),
(18, 'Puja Vidhi', 'pujavidhi.png', 1, '2018-01-08 07:26:20'),
(19, 'Mantras', 'mantra.png', 1, '2018-01-08 07:26:27'),
(20, 'Katha', 'kathaa.jpg', 1, '2018-01-08 07:26:35'),
(21, 'Meditation', 'meditation.png', 1, '2018-01-08 07:26:43'),
(22, 'Home Remedies', 'home remedies.jpg', 1, '2018-01-08 07:27:01'),
(23, 'Ayurveda', 'aayurveda.jpg', 1, '2018-01-08 07:27:18'),
(27, 'Chalisa', 'chalisa.png', 1, '2018-01-08 07:29:38');

-- --------------------------------------------------------

--
-- Table structure for table `discount_type`
--

CREATE TABLE `discount_type` (
  `id` int(255) NOT NULL,
  `discount_name` varchar(100) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `discount_type`
--

INSERT INTO `discount_type` (`id`, `discount_name`, `created_date_time`) VALUES
(1, 'Percent', '2017-12-20 07:34:47'),
(2, 'Direct', '2017-12-20 07:35:00');

-- --------------------------------------------------------

--
-- Table structure for table `filter`
--

CREATE TABLE `filter` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `categoryId` int(11) NOT NULL,
  `subCategoryId` int(11) NOT NULL,
  `priceFrom` int(11) NOT NULL,
  `priceTo` int(11) NOT NULL,
  `adsInKm` int(11) NOT NULL DEFAULT '30',
  `lati` float DEFAULT NULL,
  `lang` float DEFAULT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `filter`
--

INSERT INTO `filter` (`id`, `userId`, `categoryId`, `subCategoryId`, `priceFrom`, `priceTo`, `adsInKm`, `lati`, `lang`, `created_date_time`) VALUES
(5, 47, 9, 36, 100, 300, 81, NULL, NULL, '2018-01-04 16:47:06'),
(13, 70, 5, 4, 210, 250, 10, NULL, NULL, '2018-01-05 05:21:30'),
(7, 102, 5, 5, 1500, 5000, 11, NULL, NULL, '2018-01-04 17:05:01'),
(8, 70, 5, 4, 200, 300, 10, NULL, NULL, '2018-01-04 17:23:02'),
(12, 70, 5, 4, 210, 250, 30, NULL, NULL, '2018-01-05 05:20:42'),
(11, 70, 5, 4, 210, 250, 10, NULL, NULL, '2018-01-05 05:18:26'),
(17, 70, 5, 4, 210, 250, 10, NULL, NULL, '2018-01-05 05:52:00'),
(18, 70, 5, 0, 255, 0, 0, NULL, NULL, '2018-01-05 06:41:59');

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE `gender` (
  `id` int(255) NOT NULL,
  `gender_name` varchar(255) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`id`, `gender_name`, `created_date_time`) VALUES
(1, 'Male', '2017-12-19 10:43:35'),
(2, 'Female', '2017-12-19 10:43:50'),
(3, 'Others', '2017-12-19 10:44:08');

-- --------------------------------------------------------

--
-- Table structure for table `length_of_rental`
--

CREATE TABLE `length_of_rental` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `length_of_rental` varchar(255) NOT NULL,
  `number_of_days` int(11) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `length_of_rental`
--

INSERT INTO `length_of_rental` (`id`, `user_id`, `length_of_rental`, `number_of_days`, `created_date_time`) VALUES
(1, 34, '1 Year', 365, '2017-12-22 13:34:34'),
(2, 34, '1 Month', 30, '2017-12-22 13:34:50'),
(3, 34, '1 Week', 7, '2017-12-22 13:35:08'),
(4, 34, '1 Day', 1, '2017-12-22 13:35:27');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `id` int(255) NOT NULL,
  `user_id` int(255) NOT NULL,
  `session_id` varchar(255) NOT NULL,
  `device_token` varchar(255) NOT NULL,
  `device_type` varchar(255) NOT NULL,
  `user_login_type` int(11) DEFAULT '0',
  `login_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`id`, `user_id`, `session_id`, `device_token`, `device_type`, `user_login_type`, `login_date_time`) VALUES
(80, 56, '657', '', '', 0, '2017-12-26 09:37:15'),
(81, 56, '791', '', '', 0, '2017-12-26 09:38:24'),
(83, 62, '537', '', '', 0, '2017-12-26 09:39:59'),
(85, 63, '491', '', '', 0, '2017-12-26 09:43:01'),
(103, 63, '833', '', '', 0, '2017-12-26 13:26:41'),
(104, 37, '998', '', '', 0, '2017-12-26 14:36:20'),
(105, 63, '577', '', '', 0, '2017-12-26 14:39:22'),
(128, 63, '495', '', '', 0, '2017-12-27 05:40:12'),
(129, 63, '813', '', '', 0, '2017-12-27 05:43:42'),
(131, 63, '929', '', '', 0, '2017-12-27 05:47:17'),
(136, 74, '790', '', '', 0, '2017-12-27 07:26:23'),
(179, 63, '837', '', '', 0, '2017-12-28 09:38:27'),
(180, 63, '956', '', '', 0, '2017-12-28 09:43:59'),
(181, 78, '845', '', '', 0, '2017-12-28 09:57:46'),
(208, 80, '996', '', '', 0, '2017-12-30 07:38:15'),
(209, 70, '641', '', '', 0, '2017-12-30 07:45:25'),
(211, 74, '581', '', '', 0, '2017-12-30 14:03:19'),
(212, 74, '895', '', '', 0, '2017-12-30 14:06:45'),
(213, 74, '445', '', '', 0, '2017-12-30 14:09:53'),
(214, 88, '658', '', '', 0, '2017-12-30 15:50:22'),
(215, 88, '820', '', '', 0, '2017-12-30 16:23:28'),
(221, 67, '694', '', '', 0, '2018-01-03 14:57:15'),
(222, 67, '643', '', '', 0, '2018-01-03 14:59:37'),
(223, 67, '631', '', '', 0, '2018-01-03 15:00:12'),
(224, 67, '927', '', '', 0, '2018-01-03 15:01:19'),
(225, 67, '868', '', '', 0, '2018-01-03 15:01:21'),
(226, 67, '913', '', '', 0, '2018-01-03 15:01:21'),
(227, 67, '745', '', '', 0, '2018-01-03 15:03:18'),
(228, 94, '902', '', '', 0, '2018-01-03 15:04:11'),
(229, 95, '678', '', '', 0, '2018-01-03 16:03:08'),
(230, 97, '710', '', '', 0, '2018-01-03 16:14:35'),
(231, 97, '806', '', '', 0, '2018-01-03 16:26:38'),
(232, 97, '869', '', '', 0, '2018-01-03 16:27:47'),
(239, 102, '993', '', '', 0, '2018-01-04 11:58:59'),
(241, 102, '783', '', '', 0, '2018-01-04 12:00:04'),
(248, 104, '627', '', '', 0, '2018-01-04 12:44:27'),
(251, 104, '934', '', '', 0, '2018-01-04 12:53:39'),
(269, 102, '595', '', '', 0, '2018-01-05 11:20:21');

-- --------------------------------------------------------

--
-- Table structure for table `manage_coupon`
--

CREATE TABLE `manage_coupon` (
  `id` int(255) NOT NULL,
  `valid_for` int(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `coupon_code` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `discount_type_id` int(11) NOT NULL,
  `discount` int(255) NOT NULL,
  `valid_till` date NOT NULL,
  `image` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `manage_coupon`
--

INSERT INTO `manage_coupon` (`id`, `valid_for`, `email`, `coupon_code`, `description`, `discount_type_id`, `discount`, `valid_till`, `image`, `user_id`, `status`, `created_date_time`) VALUES
(10, 2, 'rego@gmail.com', 'DF56', 'In addition to finding coupon codes offered directly by an online merchant, there are a number of websites that track new coupon offers from merchants worldwide', 1, 6, '2017-12-31', 'offerimage.jpeg', 47, 1, '2017-12-29 05:47:57');

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(255) NOT NULL,
  `sender_user_id` int(11) NOT NULL,
  `reciever_user_id` int(11) NOT NULL,
  `message` varchar(255) NOT NULL,
  `userImage` varchar(255) NOT NULL,
  `device_type` varchar(100) NOT NULL,
  `device_token` varchar(255) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `notification_type_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `sender_user_id`, `reciever_user_id`, `message`, `userImage`, `device_type`, `device_token`, `created_date_time`, `notification_type_id`) VALUES
(1, 70, 47, 'Hi', 'user.png', 'ios', 'sasdsadasdsad', '2017-12-23 10:17:10', 1),
(2, 47, 70, 'Hello', 'user.png', 'Android', 'sadsadsdad', '2017-12-23 10:17:10', 2),
(3, 70, 102, 'hello team', 'logo.png', '', '', '2018-01-05 11:25:18', 2);

-- --------------------------------------------------------

--
-- Table structure for table `notification_type`
--

CREATE TABLE `notification_type` (
  `id` int(11) NOT NULL,
  `type_name` varchar(100) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notification_type`
--

INSERT INTO `notification_type` (`id`, `type_name`, `created_date_time`) VALUES
(1, 'New Post', '2017-12-29 11:03:02'),
(2, 'Place order', '2017-12-29 11:03:02'),
(3, 'By Admin', '2017-12-29 11:03:33');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(255) NOT NULL,
  `order_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL,
  `total_amount` varchar(255) NOT NULL,
  `payment_mode` varchar(255) NOT NULL,
  `payment_status` int(255) NOT NULL,
  `admin_commission_id` int(11) NOT NULL,
  `manage_coupon_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `sub_category_id` int(11) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `order_date`, `user_id`, `total_amount`, `payment_mode`, `payment_status`, `admin_commission_id`, `manage_coupon_id`, `post_id`, `sub_category_id`, `created_date_time`) VALUES
(1, '2017-12-20 15:06:06', 70, '500', 'online', 1, 1, 10, 0, 2, '2017-12-20 09:36:06');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `categoryId` int(11) NOT NULL,
  `subCategoryId` int(11) NOT NULL,
  `price` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `video` varchar(255) NOT NULL,
  `audio` varchar(255) NOT NULL,
  `status` int(4) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `userId`, `title`, `categoryId`, `subCategoryId`, `price`, `description`, `image`, `video`, `audio`, `status`, `created_date_time`) VALUES
(44, 0, 'Puja vidhi', 27, 71, '1233312', 'Pujavidhi', '', '', '', 1, '2018-01-08 12:49:39');

-- --------------------------------------------------------

--
-- Table structure for table `product_audio_gallery`
--

CREATE TABLE `product_audio_gallery` (
  `id` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  `product_audio` varchar(255) NOT NULL,
  `created_date_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_image_gallery`
--

CREATE TABLE `product_image_gallery` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `post_image` varchar(255) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_image_gallery`
--

INSERT INTO `product_image_gallery` (`id`, `post_id`, `post_image`, `created_date_time`) VALUES
(62, 44, 'vishnuarti.jpg', '2018-01-08 12:49:39'),
(63, 45, 'hanuman.jpg', '2018-01-08 12:50:39');

-- --------------------------------------------------------

--
-- Table structure for table `product_video_gallery`
--

CREATE TABLE `product_video_gallery` (
  `id` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  `product_video` varchar(255) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `promotions`
--

CREATE TABLE `promotions` (
  `id` int(255) NOT NULL,
  `card_title` varchar(255) NOT NULL,
  `card_amount` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_date_time` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `promotions`
--

INSERT INTO `promotions` (`id`, `card_title`, `card_amount`, `user_id`, `status`, `created_date_time`) VALUES
(1, 'test', '200', 4, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `id` int(255) NOT NULL,
  `role_name` varchar(255) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `role_name`, `created_date_time`) VALUES
(1, 'Super Admin', '2017-12-19 10:44:36'),
(2, 'Admin', '2017-12-19 10:45:30'),
(3, 'User', '2017-12-19 10:46:05');

-- --------------------------------------------------------

--
-- Table structure for table `sub_category`
--

CREATE TABLE `sub_category` (
  `id` int(255) NOT NULL,
  `categoryId` int(11) NOT NULL,
  `subCategoryName` varchar(255) NOT NULL,
  `sub_category_status` tinyint(4) NOT NULL,
  `image` varchar(255) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_category`
--

INSERT INTO `sub_category` (`id`, `categoryId`, `subCategoryName`, `sub_category_status`, `image`, `created_date_time`) VALUES
(57, 27, 'Durga Chalisa', 1, 'durga.png', '2018-01-08 08:11:32'),
(58, 27, 'Hanuman Chalisa', 1, 'hanuman.jpg', '2018-01-08 09:12:03'),
(59, 16, 'Kundalini Yoga', 1, 'kundaliniyoga.jpg', '2018-01-08 09:19:03'),
(60, 16, 'Hatha Yoga', 1, 'hathayoga.jpg', '2018-01-08 09:19:53'),
(61, 16, 'Ashtanga Yoga', 1, 'astangayoga.png', '2018-01-08 09:20:31'),
(62, 17, 'Durga Arti', 1, 'durgaarti.jpg', '2018-01-08 09:21:05'),
(63, 17, 'Shiv Arti', 1, 'shivarti.jpg', '2018-01-08 09:24:06'),
(64, 17, 'Vshnu Arti', 1, 'vishnuarti.jpg', '2018-01-08 09:24:28'),
(65, 18, 'Lakshmi Puja Vidhi', 1, 'lakshmipuja.jpg', '2018-01-08 09:26:34'),
(66, 18, 'Vishnu  Puja Vidhi', 1, 'vishnupuja.jpg', '2018-01-08 09:27:23'),
(67, 18, 'Shiv Puja Vidhi', 1, 'shivpuja.jpg', '2018-01-08 09:27:52'),
(68, 19, 'Gaytri Mantra', 1, 'gaytrimntra.jpg', '2018-01-08 09:30:29'),
(69, 19, 'Maha Mirtunjay Mantra', 1, 'shivmntra.jpg', '2018-01-08 09:32:00'),
(70, 20, 'Bhagvat Katha', 1, 'bhagvatkatha.jpg', '2018-01-08 09:34:16'),
(71, 20, 'shiva Katha', 1, 'shivkatha.jpg', '2018-01-08 09:34:40'),
(72, 21, 'Transcendental Meditation', 1, 'transcendentalmeditation.jpg', '2018-01-08 14:53:32'),
(73, 21, 'Guided Visualization', 1, 'guidedvisualization.jpg', '2018-01-08 14:55:28'),
(74, 21, 'Movement Meditation', 1, 'movement.jpg', '2018-01-08 14:56:59'),
(75, 22, 'Cucumber', 1, 'cucumber.jpg', '2018-01-08 14:59:05'),
(76, 22, 'Ginger', 1, 'ginger.jpg', '2018-01-08 14:59:24'),
(77, 22, 'Calcium Rich Food', 0, 'calciumrichfood.jpg', '2018-01-08 15:00:14'),
(78, 23, 'Ayurveda Herbs', 0, 'ayurvedaherbs.jpg', '2018-01-08 15:01:25'),
(79, 23, 'Ayurvedic Medicines', 0, 'ayurvedicmedicines.jpg', '2018-01-08 15:02:16');

-- --------------------------------------------------------

--
-- Table structure for table `temporary_user`
--

CREATE TABLE `temporary_user` (
  `id` int(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `type` tinyint(4) NOT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `date_of_birth` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `social_id` varchar(255) DEFAULT NULL,
  `social_picture` varchar(255) DEFAULT NULL,
  `user_status` tinyint(4) NOT NULL,
  `is_driving_license` tinyint(1) NOT NULL DEFAULT '0',
  `is_credit_card_approved` tinyint(1) NOT NULL DEFAULT '0',
  `device_type` varchar(255) DEFAULT NULL,
  `device_token` varchar(255) DEFAULT NULL,
  `user_register_type` int(11) NOT NULL DEFAULT '3',
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `rating` varchar(100) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `temporary_user`
--

INSERT INTO `temporary_user` (`id`, `name`, `email`, `password`, `image`, `type`, `gender`, `date_of_birth`, `phone`, `social_id`, `social_picture`, `user_status`, `is_driving_license`, `is_credit_card_approved`, `device_type`, `device_token`, `user_register_type`, `created_date_time`, `rating`) VALUES
(12, 'Admin', 'ravindra.aln@gmail.com', 'f7c3bc1d808e04732adf679965ccc34ca7ae3441', '5a4e0e0364b3b.jpeg', 3, NULL, NULL, '9898989898', NULL, NULL, 0, 0, 0, NULL, NULL, 3, '2018-01-04 11:20:35', '0'),
(13, 'Saumya', 'saumya@gmail.com', '7c222fb2927d828af22f592134e8932480637c0d', '5a4e0ffd4c648.jpeg', 3, NULL, NULL, '8077024283', NULL, NULL, 0, 0, 0, NULL, NULL, 3, '2018-01-04 11:29:01', '0'),
(14, 'Admin', 'ravindra.aln@gmail.com', 'f7c3bc1d808e04732adf679965ccc34ca7ae3441', '5a4e10e0aa9e1.jpeg', 3, NULL, NULL, '9898989898', NULL, NULL, 0, 0, 0, NULL, NULL, 3, '2018-01-04 11:32:48', '0'),
(16, 'Admin', 'ravindra.aln@gmail.com', 'f7c3bc1d808e04732adf679965ccc34ca7ae3441', '5a4e157ada1b1.jpeg', 3, NULL, NULL, '9898989898', NULL, NULL, 0, 0, 0, NULL, NULL, 3, '2018-01-04 11:52:27', '0');

-- --------------------------------------------------------

--
-- Table structure for table `temporary_user_document`
--

CREATE TABLE `temporary_user_document` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `creditcard` varchar(255) NOT NULL,
  `driving_licence` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `temporary_user_document`
--

INSERT INTO `temporary_user_document` (`id`, `user_id`, `creditcard`, `driving_licence`, `address`, `description`, `created_date_time`) VALUES
(26, 14, '1234568763678', 'DL45HG241352', 'c-75,sector 10,Noida', 'Users Document Description', '2018-01-04 11:32:48'),
(25, 13, '123456789789465132', '123456789798465132', 'Noida', 'Hello', '2018-01-04 11:29:01'),
(24, 12, '1234568763678', 'DL45HG241352', 'c-75,sector 10,Noida', 'Users Document Description', '2018-01-04 11:20:35'),
(28, 16, '1234568763678', 'DL45HG241352', 'c-75,sector 10,Noida', 'Users Document Description', '2018-01-04 11:52:27');

-- --------------------------------------------------------

--
-- Table structure for table `transction_history`
--

CREATE TABLE `transction_history` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `order_by_id` int(11) NOT NULL,
  `amount` varchar(50) NOT NULL,
  `account` varchar(255) NOT NULL,
  `transction_status` tinyint(1) NOT NULL DEFAULT '0',
  `vendor_id` int(11) NOT NULL,
  `rent_days_count` varchar(255) NOT NULL,
  `transction_id` varchar(255) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transction_history`
--

INSERT INTO `transction_history` (`id`, `order_id`, `order_by_id`, `amount`, `account`, `transction_status`, `vendor_id`, `rent_days_count`, `transction_id`, `created_date_time`) VALUES
(1, 182, 47, '500', 'sdsadsa123132gfdgfdgdfgdf', 1, 70, '7', 'sadsadsad', '2017-12-23 13:19:10'),
(2, 185, 70, '800', 'dsas213123213esdfdsfd', 1, 47, '7', 'q34r23442adasd', '2017-12-23 13:19:10');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `type` tinyint(4) NOT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `date_of_birth` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `social_id` varchar(255) DEFAULT NULL,
  `social_picture` varchar(255) DEFAULT NULL,
  `user_status` tinyint(4) NOT NULL,
  `is_driving_license` tinyint(1) NOT NULL DEFAULT '0',
  `is_credit_card_approved` tinyint(1) NOT NULL DEFAULT '0',
  `device_type` varchar(255) DEFAULT NULL,
  `device_token` varchar(255) DEFAULT NULL,
  `user_register_type` int(11) NOT NULL DEFAULT '3',
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `rating` varchar(100) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `image`, `type`, `gender`, `date_of_birth`, `phone`, `social_id`, `social_picture`, `user_status`, `is_driving_license`, `is_credit_card_approved`, `device_type`, `device_token`, `user_register_type`, `created_date_time`, `rating`) VALUES
(2, 'PanditJi', 'panditji@gmail.com', 'f7c3bc1d808e04732adf679965ccc34ca7ae3441', 'panditji.jpg', 2, '1', '1979-09-19', '1234567899', NULL, NULL, 1, 1, 1, '', '', 3, '2017-12-19 11:59:16', '1'),
(104, 'Ravi', 'ravi@gmail.com', 'f7c3bc1d808e04732adf679965ccc34ca7ae3441', 'user.png', 3, NULL, NULL, '9834567898', NULL, NULL, 1, 0, 0, NULL, NULL, 3, '2018-01-08 09:57:07', '0');

-- --------------------------------------------------------

--
-- Table structure for table `user_document`
--

CREATE TABLE `user_document` (
  `id` int(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `creditcard` varchar(255) DEFAULT NULL,
  `driving_licence` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_document`
--

INSERT INTO `user_document` (`id`, `user_id`, `creditcard`, `driving_licence`, `address`, `description`, `created_date_time`) VALUES
(44, 47, 'TR4563783TY756', 'UP16TY3452', 'C-75 Sector-10 Noida   ', 'I am A user', '2017-12-23 16:41:10'),
(89, 103, NULL, NULL, NULL, NULL, '2018-01-04 12:26:32'),
(90, 104, NULL, NULL, NULL, NULL, '2018-01-04 12:40:48'),
(88, 102, 'TR4563783TY756', 'UP16TY3452', 'C-75 Sector-10 Noida ', 'I am A user', '2018-01-04 11:58:01'),
(87, 101, 'TR4563783TY756', 'UP16TY3452', 'C-75 Sector-10 Noida ', 'I am A user', '2018-01-04 11:53:08'),
(86, 100, '123456789987654333', 'wetyyqtqreqeqeqeqeq334', 'noida sector 12', 'i am owner of our organization.', '2018-01-04 11:43:08');

-- --------------------------------------------------------

--
-- Table structure for table `user_register_type`
--

CREATE TABLE `user_register_type` (
  `id` int(11) NOT NULL,
  `register_type_name` varchar(255) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_register_type`
--

INSERT INTO `user_register_type` (`id`, `register_type_name`, `created_date_time`) VALUES
(1, 'Facebook', '2017-12-20 10:55:25'),
(2, 'Gmail', '2017-12-20 10:55:25'),
(3, 'App User', '2017-12-20 10:56:09');

-- --------------------------------------------------------

--
-- Table structure for table `user_verify`
--

CREATE TABLE `user_verify` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `otp` int(11) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_verify`
--

INSERT INTO `user_verify` (`id`, `email`, `otp`, `created_date_time`) VALUES
(26, 'saumya@gmail.com', 9635, '2018-01-04 11:29:01');

-- --------------------------------------------------------

--
-- Table structure for table `valid_for`
--

CREATE TABLE `valid_for` (
  `id` int(255) NOT NULL,
  `valid_for_name` varchar(100) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `valid_for`
--

INSERT INTO `valid_for` (`id`, `valid_for_name`, `created_date_time`) VALUES
(1, 'All Users', '2017-12-20 07:36:53'),
(2, 'Particular User', '2017-12-20 07:37:21');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about_help`
--
ALTER TABLE `about_help`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_commission`
--
ALTER TABLE `admin_commission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `discount_type`
--
ALTER TABLE `discount_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `filter`
--
ALTER TABLE `filter`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gender`
--
ALTER TABLE `gender`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `length_of_rental`
--
ALTER TABLE `length_of_rental`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `manage_coupon`
--
ALTER TABLE `manage_coupon`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notification_type`
--
ALTER TABLE `notification_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_audio_gallery`
--
ALTER TABLE `product_audio_gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_image_gallery`
--
ALTER TABLE `product_image_gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_video_gallery`
--
ALTER TABLE `product_video_gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `promotions`
--
ALTER TABLE `promotions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_category`
--
ALTER TABLE `sub_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temporary_user`
--
ALTER TABLE `temporary_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temporary_user_document`
--
ALTER TABLE `temporary_user_document`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transction_history`
--
ALTER TABLE `transction_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_document`
--
ALTER TABLE `user_document`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_register_type`
--
ALTER TABLE `user_register_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_verify`
--
ALTER TABLE `user_verify`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `valid_for`
--
ALTER TABLE `valid_for`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about_help`
--
ALTER TABLE `about_help`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `admin_commission`
--
ALTER TABLE `admin_commission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `discount_type`
--
ALTER TABLE `discount_type`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `filter`
--
ALTER TABLE `filter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `gender`
--
ALTER TABLE `gender`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `length_of_rental`
--
ALTER TABLE `length_of_rental`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=270;
--
-- AUTO_INCREMENT for table `manage_coupon`
--
ALTER TABLE `manage_coupon`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `notification_type`
--
ALTER TABLE `notification_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT for table `product_audio_gallery`
--
ALTER TABLE `product_audio_gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `product_image_gallery`
--
ALTER TABLE `product_image_gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;
--
-- AUTO_INCREMENT for table `product_video_gallery`
--
ALTER TABLE `product_video_gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `promotions`
--
ALTER TABLE `promotions`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `sub_category`
--
ALTER TABLE `sub_category`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;
--
-- AUTO_INCREMENT for table `temporary_user`
--
ALTER TABLE `temporary_user`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `temporary_user_document`
--
ALTER TABLE `temporary_user_document`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `transction_history`
--
ALTER TABLE `transction_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=105;
--
-- AUTO_INCREMENT for table `user_document`
--
ALTER TABLE `user_document`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;
--
-- AUTO_INCREMENT for table `user_register_type`
--
ALTER TABLE `user_register_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `user_verify`
--
ALTER TABLE `user_verify`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `valid_for`
--
ALTER TABLE `valid_for`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
