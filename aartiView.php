 <?php 
 $aartiId = $_GET['aartiId'];
include "database.php";
?>

<?php require('include/head.php'); ?>
<body onload="initialize()" class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
<?php include "include/header.php";?>
<?php include "include/left_sidebar.php";?>
<div class="content-wrapper">
<section class="content-header">
<h1>
Aarti Details   
</h1>
</section>
<section class="content">
<div class="box">
<div class="box-header">
</div>
            <!-- /.box-header -->
<div class="box-body table-responsive table-scroll-y">
  <table  class="table table-bordered table-striped">
      <?php 
          $sql = "SELECT a.status,al.aartiName AS godName,asl.aartiSubName AS aartiName,a.title,a.description AS Aarti,a.image,a.audio,a.video,a.created_date_time from aarti AS a  
                            INNER JOIN aarti_sub_list AS asl ON a.aarti_sub_list_id=asl.id 
                            INNER JOIN aarti_list AS al ON a.aarti_list_id=al.id WHERE a.id='$aartiId'";
          $result = $conn->query($sql);
          if ($result->num_rows>0)
          {
          $aartiview = $result->fetch_assoc();
        ?>
    <tbody>
      <tr>
        <th>God Name</th>
        <td><?php echo $aartiview['godName'];?></td>
      </tr>
      <tr>
        <th>Aarti Name</th>
        <td><?php echo $aartiview['aartiName'];?></td>
      </tr>
      <tr>
        <th>Aarti</th>
        <td><?php echo $aartiview['Aarti'];?></td>
      </tr>
      <tr>
        <th>Image</th>
        <td><img src="assets/img/uploads/aarti/<?php echo $aartiview['image']; ?>" class="img-responsive" style="width:30px; height:30px" ></td>
      </tr>
      <tr>
        <th>Video</th>
        <td><video  width="100px" height="100px" controls><source src="assets/img/uploads/aarti/<?php echo $aartiview['video']; ?>" class="img-responsive" style="width:80px; height:80px" type="video/mp4"></video></td>
      </tr>
      <tr>
        <th>Audio</th>
        <td><audio width="30px" height="30px" controls><source src="assets/img/uploads/aarti/<?php echo $aartiview['audio']; ?>" class="img-responsive" style="width:30px; height:30px" type="audio/ogg"></audio></td>
      </tr>
      <tr>
        <th>Posted Date</th>
        <td><?php echo $aartiview['created_date_time'];?></td>
      </tr>

  </tbody>
  <?php } ?>
  </table> 
  <a href="aarti.php" style="color: #fff;"><button type="button" class="btn" style="margin-top: 10px" >Back</button></a>          
</div>
</div>
</div>
<?php include "include/footer.php" ;?>
<?php include "include/right_sidebar.php" ;?>
</div>
<?php include "include/footer_script.php" ;?>
<script src='http://maps.googleapis.com/maps/api/js?v=3&sensor=false&amp;libraries=places&key=AIzaSyAL1eAba9HoD7qszOJ-ggOvZvbq-TvXDys'></script>
<script src="js/script.js"> </script>
</body>
</html>
