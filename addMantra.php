<?php 
include "database.php";
?>
<?php
if(isset($_POST["sub"]))
{
  //image
$target_dir = "assets/img/uploads/mantra/";
$image = $_FILES['image']["name"];
$target_file = $target_dir . basename($_FILES['image']["name"]);
//video
$target_dir_video = "assets/img/uploads/mantra/";
$video = $_FILES['video']["name"];
$target_file_video = $target_dir_video . basename($_FILES['video']["name"]);
//audio
$target_dir_audio = "assets/img/uploads/mantra/";
$audio = $_FILES['audio']["name"];
$target_file_audio = $target_dir_audio . basename($_FILES['audio']["name"]);

$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
$videoFileType = pathinfo($target_file_video,PATHINFO_EXTENSION);
$audioFileType = pathinfo($target_file_audio,PATHINFO_EXTENSION);

$title=$_POST["title"];
$mantra_list_id=$_POST["mantra_list_id"];
$description=$_POST["description"];
$status = 1;
              if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file) || move_uploaded_file($_FILES["video"]["tmp_name"], $target_file_video) || move_uploaded_file($_FILES["audio"]["tmp_name"], $target_file_audio))
              {
                $image =  $_FILES["image"]["name"];
                $video =  $_FILES["video"]["name"];
                $audio =  $_FILES["audio"]["name"];
              }
            
      $sql = "INSERT INTO mantra (mantra_list_id,title,description,image,audio,video)
      VALUES ('$mantra_list_id','$title','$description','$image','$audio','$video')";
      if ($conn->query($sql) === TRUE) 
      {
      header("location:mantra.php");
      } 
      else 
      {
      echo "Error: " . $sql . "<br>" . $conn->error;
      }
}
?>
<?php require('include/head.php'); ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
 <?php include "include/header.php";?> 
  <?php include "include/left_sidebar.php";?>
       <div class="content-wrapper">
            <section class="content-header">
              <h1>
              Add Mantra     
              </h1>
            </section>
            <section class="content">
        <div class="box">
        <div class="box-body table-responsive table-scroll-y">
        <form method="post" enctype="multipart/form-data">
                  
            <table id="example1" class="table table-bordered table-striped">
            <table  class="table table-bordered table-striped">           
            <tbody>
            <tr>
                <th>God Name</th>
                <td>
                  <select name="mantra_list_id">
                    <?php 
                  $sql = "SELECT id,mantraName from mantra_list";
                  $result = $conn->query($sql);
                  if ($result->num_rows>0)
                  {
                  while($mantraList = $result->fetch_assoc())
                  {
                  ?>
                    <option value="<?php echo $mantraList['id'];?>"><?php echo $mantraList['mantraName'];?>
                    </option>
                     <?php } }?>
                  </select>
                 </td>
            </tr>
              <tr>
              <th>Title</th>
              <td><input type="text" name="title" placeholder="Add Title"></td>
              </tr>
            <tr>
              <th>Add Image</th>
                <td><!-- <label for="newimage" class="btn text-muted text-center " style="width:82%;margin-top: 2%;">Choose Image</label> -->
                    <input id="newimage" type="file" name="image"> 
              </td>
            </tr>
            <tr>
              <th>Add Video</th>
                <td><!-- <label for="newvideo" class="btn text-muted text-center " style="width:82%;margin-top: 2%;">Choose Video</label> -->
                    <input id="newvideo" type="file" name="video"> 
              </td>
            </tr>
            <tr>
              <th>Add Audio</th>
                <td><!-- <label for="newaudio" class="btn text-muted text-center " style="width:82%;margin-top: 2%;">Choose Audio</label> -->
                    <input id="newaudio" type="file" name="audio"> 
              </td>
            </tr>
            <tr>
              <th>Mantra</th>
              <td>
                <textarea class="tinymce" id="mytextarea" name="description" placeholder="Add Mantra" ></textarea>
                <!-- <input type="text" name="description" placeholder="Add Mantra" required> -->
              </td>
            </tr>
            
            </tbody>             
            </table>
            </table>
            <a href="mantra.php" style="color: #fff;"><button type="button" class="btn" style="margin-top: 10px" >Back</button></a>
            <button type="submit" class="btn   pull-right" name="sub" style="margin-top: 10px" >Add</button>
            
        </form>
        </div>
        </div>
        </div>
  <?php include "include/footer.php" ;?>
  <?php include "include/right_sidebar.php" ;?>  
</div>
<?php include "include/footer_script.php" ;?>
<script src="jscolor.js"></script>
</body>
</html>
