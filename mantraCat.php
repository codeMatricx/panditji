 <?php 
 include "database.php";
?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php require('include/head.php'); ?>
<body onload="initialize()" class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
<?php include "include/header.php";?> 
  <?php include "include/left_sidebar.php";?>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Mantra List 
      </h1>
      <ol class="breadcrumb">
        <li><a href="addMantraCat.php"><button type="button" class="btn btn-block " style="margin-top: -5px;" >Add Mantra Name</button></a></li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
     <div class="box">
            <div class="box-header">
              <h3 class="box-title">Mantra Table With Full Features</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive table-scroll-y">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
               <!--  <tr> -->
                  <tr>
                  <th>S.NO</th>
                  <th>Mantra Name </th>
                  <th>Icon</th>
                  <!-- <th>Status </th> -->
                  <th>Action</th>
                  <!-- </tr> -->
                  <th><input name="select-all" id="checkall" onClick="check_uncheck_checkbox(this.checked);" value="check_all" type="checkbox"></th>
                </tr> 
                </thead>
                <tbody>
                  <?php 
                      $sql = "SELECT * from mantra_list";
                      $result = $conn->query($sql);
                      if ($result->num_rows>0)
                      {
                      $serial=0;
                      while($mantra = $result->fetch_assoc())
                      {
                        $serial++;
                      ?>
                <!-- <tr> -->
                  <tr id="<?php  echo $mantra['id'];?>">
                      <td><?php echo $serial; ?></td>
                      <td><?php  echo $mantra['mantraName'];?></td>
                      <td><img src="assets/img/uploads/mantra/<?php echo $mantra['image']; ?>" class="img-responsive" style="width:30px; height:30px" ></td>
                  <td >
                    <a href="mantraCatEdit.php?cat_id=<?php echo $mantra['id'];?>"  style="cursor: pointer;">
                     <i class="fa fa-pencil-square-o" aria-hidden="true"></i> </a>
                  </td>
                  <td><input   type="checkbox" value="<?php echo $mantra['id'];?>" name="action" id="checkbox_category"></td>
                </tr>
                <?php
                      } 
                    } 
                      ?>    
                </tbody>
              </table>
                <button class="btn btn-danger" onclick="deleteMantraCat()">Delete</button>
            </div>
            <!-- /.box-body -->
          </div>
  </div>
  <!-- /.content-wrapper -->
  <?php include "include/footer.php" ;?> 
  <!-- Control Sidebar -->
  <?php include "include/right_sidebar.php" ;?>
</div>
<!-- ./wrapper -->
<?php include "include/footer_script.php" ;?>
<script src='http://maps.googleapis.com/maps/api/js?v=3&sensor=false&amp;libraries=places&key=AIzaSyAL1eAba9HoD7qszOJ-ggOvZvbq-TvXDys'></script>
<script src="js/script.js"> </script>
<script src="jscolor.js"></script>
</body>
</html>
