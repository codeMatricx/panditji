 <?php 
 include "database.php";
 $id = $_GET['user_id'];
?>
<?php require('include/head.php'); ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
<?php include "include/header.php";?>
<?php include "include/left_sidebar.php";?>
  <div class="content-wrapper">
      <section class="content-header">
      <h1>
      User Details   
      </h1>   
      </section>
     <section class="content">
      <div class="box">
        <div class="box-body">
          <table  class="table table-bordered table-striped">
            <tbody>
                  <?php 
                  $sql = "SELECT * from users WHERE id='$id'";
                  $result = $conn->query($sql);
                  if ($result->num_rows>0)
                  {
                  $serial=0;
                  $user_view = $result->fetch_assoc();
                  
                  ?>
                <tr>
                  <th>User Name</th>
                  <td><?php  echo $user_view['name'];?></td> 
                </tr>
                <tr>
                  <th>User Email</th>
                  <td><?php  echo $user_view['email'];?></td> 
                </tr>
                <tr>
                  <th>Register Date</th>
                  <td><?php  echo $user_view['created_date_time'];?></td> 
                </tr>
                <tr>
                  <th>User Image</th>
                  <td><img src="assets/img/uploads/users/<?php echo $user_view['image']; ?>" class="img-responsive" style="width:30px; height:30px" ></td> 
                </tr>
                <tr>
                  <th>User Phone</th>
                  <td><?php  echo $user_view['phone'];?></td> 
                </tr>
                <?php }?>
                </tbody>  
              </table>
              <a href="user.php" style="color: #fff;"><button type="button" class="btn  " style="margin-top: 10px">Back</button></a>
          </div>
      </div>
  </div>
  <?php include "include/footer.php" ;?>
  <?php include "include/right_sidebar.php" ;?>
</div>  
<?php include "include/footer_script.php" ;?>
</body>
</html>
