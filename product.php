 <?php 
 include "database.php";
?>

<?php require('include/head.php'); ?>
<body onload="initialize()" class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
<?php include "include/header.php";?>
<?php include "include/left_sidebar.php";?>
<div class="content-wrapper">
<section class="content-header">
<h1>
Product List
</h1>
<ol class="breadcrumb">
<li><a href="addProduct.php"><button type="button" class="btn btn-block " style="margin-top: -5px;">Add Product</button></a></li>
</ol>
</section>
<section class="content">
<div class="box">
<div class="box-header">
<h3 class="box-title">Product Table With Full Features</h3>
</div>
            <!-- /.box-header -->
            <div class="box-body table-responsive table-scroll-y">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                <th>S.NO</th>
                <!-- <th>User Name</th> -->
                <th>Title</th>
                <th>Category Name</th>
                <th>SubCategory Name</th>
                <th>Price</th>
                <th>Description</th>
                <th>Image</th>
                <th>Video</th>
                <th>Audio</th>
                <th>Posted Date</th>
                <th>Status </th>
                <th>Action</th>
                  <th><input name="select-all" id="checkall" onClick="check_uncheck_checkbox(this.checked);" value="check_all" type="checkbox"></th>
                </tr>
                </thead>
                    <tbody>
                        <?php 
                        $sql = "SELECT p.status,p.id,p.title,c.categoryName,sc.subCategoryName,p.price,p.description,p.created_date_time,i.post_image AS product_image,v.product_video,a.product_audio 
                        from product AS p 
                        INNER JOIN category AS c ON p.categoryId=c.id 
                        INNER JOIN sub_category AS sc ON p.subCategoryId=sc.id 
                        LEFT JOIN product_image_gallery AS i ON p.id=i.post_id 
                        LEFT JOIN product_video_gallery AS v ON p.id=v.productId 
                        LEFT JOIN product_audio_gallery AS a ON p.id=a.productId  
                        Order By p.id";
                        $result = $conn->query($sql);
                        if ($result->num_rows>0)
                        {
                        $serial=0;
                        while($productLst = $result->fetch_assoc())
                        {
                        	//print_r($productLst['product_video']);exit;
                        $desc=$productLst['description'];
                        $serial++;
                        ?>
                    <tr id="<?php  echo $productLst['id'];?>">
                        <td><?php echo $serial; ?></td>
                        <td><?php  echo $productLst['title'];?></td> 
                        <td><?php  echo $productLst['categoryName'];?></td>
                        <td><?php  echo $productLst['subCategoryName'];?></td>
                        <td><?php  echo $productLst['price'];?></td>
                        <td><?php  echo substr ($desc, 0, 20);?></td>
                        <td><img src="assets/img/uploads/products/<?php echo $productLst['product_image']; ?>" class="img-responsive" style="width:30px; height:30px" ></td>
                        <td><video  width="30px" height="30px" controls><source src="assets/img/uploads/products/<?php echo $productLst['product_video']; ?>" class="img-responsive" style="width:30px; height:30px" type="video/mp4"></video></td>
                        <td><audio width="30px" height="30px" controls><source src="assets/img/uploads/products/<?php echo $productLst['product_audio']; ?>" class="img-responsive" style="width:30px; height:30px" type="audio/ogg"></audio></td>
                        <td><?php  echo $productLst['created_date_time'];?></td>
                       <?php 
                        if($productLst['status'] == 1)
                        {
                        ?>
                        <td><img src="assets/img/enable.gif" class="img-responsive" title="Activated"></td>
                        <?php
                        }
                        ?>
                        <?php
                        if($productLst['status'] == 0)
                        {
                        ?>
                        <td><img src="assets/img/disable.gif" class="img-responsive" title="Deactivated"></td>
                        <?php
                        }
                        ?>
                    <td >
                    <a href="product_edit.php?productId=<?php echo $productLst['id']; ?>" style="cursor: pointer;">
                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i> </a>/
                    <a href="product_view.php?productId=<?php echo $productLst['id']; ?>" style="cursor: pointer;">
                    <i class="fa fa-eye" aria-hidden="true"></i> </a>
                    </td>
                    <td><input   type="checkbox" value="<?php echo $productLst['id'];?>" name="action" id="checkbox-1"></td>
                    </tr> 
                    <?php 
                    }
                    } 
                    ?>
                    </tbody>
              </table>
<button class="btn btn-success" onclick="productActivate()">Activate</button>
<button class="btn btn-danger" onclick="productDeactivate()">Deactivate</button>
<button class="btn btn-danger" onclick="deleteproduct()">Delete</button>
</div>
</div>
</div>
<?php include "include/footer.php" ;?>
<?php include "include/right_sidebar.php" ;?>
</div>
<?php include "include/footer_script.php" ;?>
<script src='http://maps.googleapis.com/maps/api/js?v=3&sensor=false&amp;libraries=places&key=AIzaSyAL1eAba9HoD7qszOJ-ggOvZvbq-TvXDys'></script>
<script src="js/script.js"> </script>
</body>
</html>
