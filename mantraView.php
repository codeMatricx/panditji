 <?php 
 $mantraId = $_GET['mantraId'];
include "database.php";
?>

<?php require('include/head.php'); ?>
<body onload="initialize()" class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
<?php include "include/header.php";?>
<?php include "include/left_sidebar.php";?>
<div class="content-wrapper">
<section class="content-header">
<h1>
Mantra Details   
</h1>
</section>
<section class="content">
<div class="box">
<div class="box-header">
</div>
            <!-- /.box-header -->
<div class="box-body table-responsive table-scroll-y">
  <table  class="table table-bordered table-striped">
      <?php 
          $sql = "SELECT m.id,m.title,m.description,m.image,m.audio,m.video,m.created_date_time,ml.mantraName FROM mantra AS m LEFT JOIN mantra_list AS ml ON m.mantra_list_id = ml.id WHERE m.id='$mantraId'";
          $result = $conn->query($sql);
          if ($result->num_rows>0)
          {
          $mantraView = $result->fetch_assoc();
        ?>
    <tbody>
      <tr>
        <th>God Name</th>
        <td><?php echo $mantraView['mantraName'];?></td>
      </tr>
      <tr>
        <th>Title</th>
        <td><?php echo $mantraView['title'];?></td>
      </tr>
      
      <tr>
        <th>Mantra</th>
        <td><?php echo $mantraView['description'];?></td>
      </tr>
      <tr>
        <th>Image</th>
        <td><img src="assets/img/uploads/mantra/<?php echo $mantraView['image']; ?>" class="img-responsive" style="width:30px; height:30px" ></td>
      </tr>
      <tr>
        <th>Video</th>
        <td><video  width="100px" height="100px" controls><source src="assets/img/uploads/mantra/<?php echo $mantraView['video']; ?>" class="img-responsive" style="width:80px; height:80px" type="video/mp4"></video></td>
      </tr>
      <tr>
        <th>Audio</th>
        <td><audio width="30px" height="30px" controls><source src="assets/img/uploads/mantra/<?php echo $mantraView['audio']; ?>" class="img-responsive" style="width:30px; height:30px" type="audio/ogg"></audio></td>
      </tr>
      <tr>
        <th>Posted Date</th>
        <td><?php echo $mantraView['created_date_time'];?></td>
      </tr>

  </tbody>
  <?php } ?>
  </table> 
  <a href="mantra.php" style="color: #fff;"><button type="button" class="btn" style="margin-top: 10px" >Back</button></a>          
</div>
</div>
</div>
<?php include "include/footer.php" ;?>
<?php include "include/right_sidebar.php" ;?>
</div>
<?php include "include/footer_script.php" ;?>
<script src='http://maps.googleapis.com/maps/api/js?v=3&sensor=false&amp;libraries=places&key=AIzaSyAL1eAba9HoD7qszOJ-ggOvZvbq-TvXDys'></script>
<script src="js/script.js"> </script>
</body>
</html>
