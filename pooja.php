 <?php 
 include "database.php";
?>

<?php require('include/head.php'); ?>
<body onload="initialize()" class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
<?php include "include/header.php";?>
<?php include "include/left_sidebar.php";?>
<div class="content-wrapper">
<section class="content-header">
<h1>
Pooja List
</h1>
<ol class="breadcrumb">
<li><a href="addPooja.php"><button type="button" class="btn btn-block " style="margin-top: -5px;">Add Pooja</button></a></li>
</ol>
</section>
<section class="content">
<div class="box">
<div class="box-header">
<h3 class="box-title">Pooja Table With Full Features</h3>
</div>
            <!-- /.box-header -->
            <div class="box-body table-responsive table-scroll-y">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                <th>S.NO</th>
                <th>Pooja Name</th>
                <th>Step</th>
                <!-- <th>Price</th> -->
                <th>Pooja Vidhi</th>
                <th>Image</th>
                <th>Video</th>
                <th>Audio</th>
                <th>Posted Date</th>
                <!-- <th>Status </th> -->
                <th>Action</th>
                  <th><input name="select-all" id="checkall" onClick="check_uncheck_checkbox(this.checked);" value="check_all" type="checkbox"></th>
                </tr>
                </thead>
                    <tbody>
                        <?php 
                            $sql = "SELECT p.id,p.title,p.description,p.image,p.audio,p.video,p.created_date_time,pl.poojaName FROM pooja AS p LEFT JOIN pooja_list AS pl ON p.poojaId=pl.id";
                        $result = $conn->query($sql);
                        if ($result->num_rows>0)
                        {
                        $serial=0;
                        while($poojaList = $result->fetch_assoc())
                        {
                        $desc=$poojaList['description'];
                        $serial++;
                        ?>
                    <tr id="<?php  echo $poojaList['id'];?>">
                        <td><?php echo $serial; ?></td>
                        <td><?php  echo $poojaList['poojaName'];?></td>
                        <td><?php  echo $poojaList['title'];?></td>
                        <td><?php  echo substr ($desc, 0, 20);?></td> 
                        <td><img src="assets/img/uploads/pooja/<?php echo $poojaList['image']; ?>" class="img-responsive" style="width:30px; height:30px" ></td>
                        <td><video  width="30px" height="30px" controls><source src="assets/img/uploads/pooja/<?php echo $poojaList['video']; ?>" class="img-responsive" style="width:30px; height:30px" type="video/mp4"></video></td>
                        <td><audio width="30px" height="30px" controls><source src="assets/img/uploads/pooja/<?php echo $poojaList['audio']; ?>" class="img-responsive" style="width:30px; height:30px" type="audio/ogg"></audio></td>
                        <td><?php  echo $poojaList['created_date_time'];?></td>
                       
                    <td >
                    <a href="poojaEdit.php?poojaId=<?php echo $poojaList['id']; ?>" style="cursor: pointer;">
                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i> </a>/
                    <a href="poojaView.php?poojaId=<?php echo $poojaList['id']; ?>" style="cursor: pointer;">
                    <i class="fa fa-eye" aria-hidden="true"></i> </a>
                    </td>
                    <td><input   type="checkbox" value="<?php echo $poojaList['id'];?>" name="action" id="checkbox-1"></td>
                    </tr> 
                    <?php 
                    }
                    } 
                    ?>
                    </tbody>
              </table>
<!-- <button class="btn btn-success" onclick="yogaActivate()">Activate</button>
<button class="btn btn-danger" onclick="yogaDeactivate()">Deactivate</button> -->
<button class="btn btn-danger" onclick="deletePooja()">Delete</button>
</div>
</div>
</div>
<?php include "include/footer.php" ;?>
<?php include "include/right_sidebar.php" ;?>
</div>
<?php include "include/footer_script.php" ;?>
<script src='http://maps.googleapis.com/maps/api/js?v=3&sensor=false&amp;libraries=places&key=AIzaSyAL1eAba9HoD7qszOJ-ggOvZvbq-TvXDys'></script>
<script src="js/script.js"> </script>
</body>
</html>
