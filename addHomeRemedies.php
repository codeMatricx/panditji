<?php 
include "database.php";
?>
<?php
if(isset($_POST["sub"]))
{
  //image
$target_dir = "assets/img/uploads/homeremedies/";
$image = $_FILES['image']["name"];
$target_file = $target_dir . basename($_FILES['image']["name"]);
//video
$target_dir_video = "assets/img/uploads/homeremedies/";
$video = $_FILES['video']["name"];
$target_file_video = $target_dir_video . basename($_FILES['video']["name"]);
//audio
$target_dir_audio = "assets/img/uploads/homeremedies/";
$audio = $_FILES['audio']["name"];
$target_file_audio = $target_dir_audio . basename($_FILES['audio']["name"]);

$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
$videoFileType = pathinfo($target_file_video,PATHINFO_EXTENSION);
$audioFileType = pathinfo($target_file_audio,PATHINFO_EXTENSION);

$title=$_POST["title"];
$remedies_list_id=$_POST["remedies_list_id"];
$description=$_POST["description"];
$status = 1;
              if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file) || move_uploaded_file($_FILES["video"]["tmp_name"], $target_file_video) || move_uploaded_file($_FILES["audio"]["tmp_name"], $target_file_audio))
              {
                $image =  $_FILES["image"]["name"];
                $video =  $_FILES["video"]["name"];
                $audio =  $_FILES["audio"]["name"];
              }
            
      $sql = "INSERT INTO home_remedies (remedies_list_id,title,description,image,audio,video)
      VALUES ('$remedies_list_id','$title','$description','$image','$audio','$video')";
      if ($conn->query($sql) === TRUE) 
      {
      header("location:homeRemedies.php");
      } 
      else 
      {
      echo "Error: " . $sql . "<br>" . $conn->error;
      }
}
?>
<?php require('include/head.php'); ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
 <?php include "include/header.php";?> 
  <?php include "include/left_sidebar.php";?>
       <div class="content-wrapper">
            <section class="content-header">
              <h1>
              Add Remedies     
              </h1>
            </section>
            <section class="content">
        <div class="box">
        <div class="box-body table-responsive table-scroll-y">
        <form method="post" enctype="multipart/form-data">
                  
            <table id="example1" class="table table-bordered table-striped">
            <table  class="table table-bordered table-striped">           
            <tbody>
            <tr>
                <th>Remedies Name</th>
                <td>
                  <select name="remedies_list_id">
                    <?php 
                  $sql = "SELECT id,remediesName from home_remedies_list";
                  $result = $conn->query($sql);
                  if ($result->num_rows>0)
                  {
                  while($remediesList = $result->fetch_assoc())
                  {
                  ?>
                    <option value="<?php echo $remediesList['id'];?>"><?php echo $remediesList['remediesName'];?>
                    </option>
                     <?php } }?>
                  </select>
                 </td>
            </tr>
              <tr>
              <th>Step</th>
              <td><input type="text" name="title" placeholder="Add Step"></td>
              </tr>
            <tr>
              <th>Add Image</th>
                <td>
                    <input id="newimage" type="file" name="image"> 
              </td>
            </tr>
            <tr>
              <th>Add Video</th>
                <td>
                    <input id="newvideo" type="file" name="video"> 
              </td>
            </tr>
            <tr>
              <th>Add Audio</th>
                <td>
                    <input id="newaudio" type="file" name="audio"> 
              </td>
            </tr>
            <tr>
              <th>Remedies</th>
              <td>
                <textarea class="tinymce" id="mytextarea" name="description" placeholder="Add Remedies" ></textarea>
                <!-- <input type="text" name="description" placeholder="Add Remedies" required> -->
              </td>
            </tr>
            
            </tbody>             
            </table>
            </table>
            <a href="homeRemedies.php" style="color: #fff;"><button type="button" class="btn" style="margin-top: 10px" >Back</button></a>
            <button type="submit" class="btn   pull-right" name="sub" style="margin-top: 10px" >Add</button>
            
        </form>
        </div>
        </div>
        </div>
  <?php include "include/footer.php" ;?>
  <?php include "include/right_sidebar.php" ;?>  
</div>
<?php include "include/footer_script.php" ;?>
<script src="jscolor.js"></script>
</body>
</html>
