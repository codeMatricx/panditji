<?php
include "database.php";
?>
<?php require('include/head.php'); ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper"> 
  <?php include "include/header.php";?>
  <?php include "include/left_sidebar.php";?>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Panditji Administration control Panel   
      </h1>
    <section class="content">
      <div class="row">
        <div class="col-md-4 col-sm-6 col-xs-12">
          <a href="user.php">
            <div class="info-box" style="border-radius: 10px;">
            <span class="info-box-icon bg-aqua" style="border-radius: 10px;"><i class="fa fa-user"></i></span>
            <div class="info-box-content">
                  <?php 
                  $sql = "SELECT count(id) AS cnt from users WHERE type != '1'";
                  $result = $conn->query($sql);
                  $numrow = $result->fetch_row();
                  $countUser = $numrow['0']; 
                  ?>
              <span class="info-box-text">Users</span>
              <span class="info-box-number"><?php echo $countUser; ?><small></small></span>
            </div>
          </div>
        </a>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12">
          <a href="yoga.php">
          <div class="info-box" style="border-radius: 10px;">
            <span class="info-box-icon bg-yellow" style="border-radius: 10px;"><i class="fa fa-adjust"></i></span>

            <div class="info-box-content">
              <?php 
                  $sql = "SELECT count(id) AS cnt from yoga ";
                  $result = $conn->query($sql);
                  $numrow = $result->fetch_row();
                  $countPost = $numrow['0']; 
                  ?>
              <span class="info-box-text">Yoga</span>
              <span class="info-box-number"><?php echo $countPost; ?></span>
            </div>
          </div>
        </a>
        </div>
        <div class="clearfix visible-sm-block"></div>
        <div class="col-md-4 col-sm-6 col-xs-12">
          <a href="aarti.php">
          <div class="info-box" style="border-radius: 10px;">
            <span class="info-box-icon bg-green" style="border-radius: 10px;"><i class="fa fa-sort"></i></span>
            <div class="info-box-content">
              <?php 
                  $sql = "SELECT count(id) AS cnt from aarti ";
                  $result = $conn->query($sql);
                  $numrow = $result->fetch_row();
                  $countOrder = $numrow['0']; 
                  ?>
              <span class="info-box-text">Aarti</span>
              <span class="info-box-number"><?php echo $countOrder; ?></span>
            </div>
          </div>
        </a>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12">
          <a href="pooja.php">
          <div class="info-box" style="border-radius: 10px;">
            <span class="info-box-icon bg-yellow" style="border-radius: 10px;"><i class="fa fa-list"></i></span>

            <div class="info-box-content">
              <?php 
                  $sql = "SELECT count(id) AS cnt from pooja ";
                  $result = $conn->query($sql);
                  $numrow = $result->fetch_row();
                  $countOCat = $numrow['0']; 
                  ?>
              <span class="info-box-text">Pooja</span>
              <span class="info-box-number"><?php echo $countOCat; ?></span>
            </div>
          </div>
        </a>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12">
          <a href="mantra.php">
          <div class="info-box" style="border-radius: 10px;">
            <span class="info-box-icon bg-red" style="border-radius: 10px;"><i class="fa fa-calendar"></i></span>

            <div class="info-box-content">
              <?php 
                  $sql = "SELECT count(id) AS cnt from mantra ";
                  $result = $conn->query($sql);
                  $numrow = $result->fetch_row();
                  $countPro = $numrow['0']; 
                  ?>
              <span class="info-box-text">Mantra</span>
              <span class="info-box-number"><?php echo $countPro; ?></span>
            </div>
          </div>
        </a>
        </div> 
        <div class="col-md-4 col-sm-6 col-xs-12">
          <a href="katha.php">
          <div class="info-box" style="border-radius: 10px;">
            <span class="info-box-icon bg-yellow" style="border-radius: 10px;"><i class="fa fa-th-list"></i></span>

            <div class="info-box-content">
               <?php 
                  $sql = "SELECT count(id) AS cnt from katha ";
                  $result = $conn->query($sql);
                  $numrow = $result->fetch_row();
                  $countSubCat = $numrow['0']; 
                  ?>
              <span class="info-box-text">Katha</span>
              <span class="info-box-number"><?php echo $countSubCat; ?></span>
            </div>
          </div>
        </a>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12">
          <a href="chalisa.php">
          <div class="info-box" style="border-radius: 10px;">
            <span class="info-box-icon bg-green" style="border-radius: 10px;"><i class="fa fa-file-picture-o"></i></span>

            <div class="info-box-content">
               <?php 
                  $sql = "SELECT count(id) AS cnt from chalisa ";
                  $result = $conn->query($sql);
                  $numrow = $result->fetch_row();
                  $countnote = $numrow['0']; 
                  ?>
              <span class="info-box-text">Chalisa</span>
              <span class="info-box-number"><?php echo $countnote; ?></span>
            </div>
           
          </div>
        </a>
        </div> 

         <div class="col-md-4 col-sm-6 col-xs-12">
          <a href="meditation.php">
          <div class="info-box" style="border-radius: 10px;">
            <span class="info-box-icon bg-aqua" style="border-radius: 10px;"><i class="fa fa-bullseye"></i></span>

            <div class="info-box-content">
               <?php 
                  $sql = "SELECT count(id) AS cnt from meditation ";
                  $result = $conn->query($sql);
                  $numrow = $result->fetch_row();
                  $countnote = $numrow['0']; 
                  ?>
              <span class="info-box-text">Meditation</span>
              <span class="info-box-number"><?php echo $countnote; ?></span>
            </div>
          </div>
        </a>
        </div>

        <div class="col-md-4 col-sm-6 col-xs-12">
          <a href="homeRemedies.php">
          <div class="info-box" style="border-radius: 10px;">
            <span class="info-box-icon bg-red" style="border-radius: 10px;"><i class="fa fa-crosshairs"></i></span>

            <div class="info-box-content">
               <?php 
                  $sql = "SELECT count(id) AS cnt from home_remedies ";
                  $result = $conn->query($sql);
                  $numrow = $result->fetch_row();
                  $countnote = $numrow['0']; 
                  ?>
              <span class="info-box-text">Home Remedies</span>
              <span class="info-box-number"><?php echo $countnote; ?></span>
            </div>
          </div>
        </a>
        </div>  

      </div>
  </section>
  </div>
  <?php include "include/footer.php" ;?>
  <?php include "include/right_sidebar.php" ;?>  
</div>
<?php include "include/footer_script.php" ;?>
</body>
</html>
