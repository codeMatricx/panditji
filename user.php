<?php
include "database.php";
?>
<?php require('include/head.php'); ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
 <?php include "include/header.php";?>
  <?php 
  include "include/left_sidebar.php"; ?>
  <div class="content-wrapper" style="min-height: 879.773px">
    <section class="content-header">
      <h1>
        Users List  
      </h1>    
    </section>
   <section class="content">
     <div class="box">
            <div class="box-header">
              <h3 class="box-title">Users Table With Full Features</h3>
            </div>
            <div class="box-body table-responsive table-scroll-y">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>S.No</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Mobile</th>
                  <th>Image</th>
                  <th>Register Date</th>
                  <th>Status</th>
                  <th>Action</th>
                  <th><input name="select-all" id="checkall" onClick="check_uncheck_checkbox(this.checked);"  value="check_all"  type="checkbox"></th>
                </tr>
                </thead>
                <tbody>
                        <?php 
                        $sql = "SELECT * FROM users WHERE (type='3' || type='2')";
                        $result = $conn->query($sql);
                        if ($result->num_rows>0)
                        {
                        $serial=0;
                        while($user = $result->fetch_assoc())
                        {
                        $serial++;
                        ?>
                
                <tr id="<?php  echo $user['id'];?>">
                  <td><?php echo $serial; ?></td>
                  <td><?php  echo $user['name'];?></td>
                  <td><?php  echo $user['email'];?></td>
                  <td><?php  echo $user['phone'];?></td>
                  <td><img src="assets/img/uploads/users/<?php echo $user['image']; ?>" class="img-responsive" style="width:30px; height:30px" ></td>
                  <td><?php  echo $user['created_date_time'];?></td>
                      <?php 
                      if($user['user_status'] == 1)
                      {
                      ?>
                      <td><img src="assets/img/enable.gif" class="img-responsive" title="Activated"></td>
                      <?php
                      }
                      ?>
                      <?php
                      if($user['user_status'] == 0)
                      {
                      ?>
                      <td><img src="assets/img/disable.gif" class="img-responsive" title="Deactivated"></td>
                      <?php
                      }
                      ?>
                  <td >
                       <a href="user_view.php?user_id=<?php  echo $user['id'];?>"  style="cursor: pointer;"><i class="fa fa-eye" aria-hidden="true"></i></a>
                  </td>
                  <td><input type="checkbox" value="<?php echo $user['id'];?>" name="action" id="checkboxs"></td>
                </tr>
                <?php } } ?>
                </tbody>
              </table>
                <button class="btn btn-success" onclick="activate()">Activate</button>
                <button class="btn btn-danger" onclick="deactivate()">Deactivate</button>
                <button class="btn btn-danger" onclick="deleteUser()">Delete</button>
            </div>
          </div>
    </section>
  </div>
  <?php include "include/footer.php" ;?>
  <?php include "include/right_sidebar.php" ;?>
</div>              
<?php include "include/footer_script.php" ;?>
</body>
</html>
