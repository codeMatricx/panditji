<?php 
session_start();
include "database.php";
include 'check_session.php';
if (isset($_SESSION["usr_id"]))
  { ?>

    <header class="main-header">
    <!-- Logo -->
    <span  class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>R</b>EGO</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>PanditJi</b></span>
    </span>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          
          <!-- Notifications: style can be found in dropdown.less -->
         
          <!-- Tasks: style can be found in dropdown.less -->
          
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <?php 
                                    $uid = $_SESSION['usr_id'];
                                    $sql = "SELECT u.id,u.name,u.image,u.email,u.phone,u.type,u.created_date_time FROM users AS u  WHERE u.id = '$uid'";
                                    //print_r($sql);exit;
                                    $result = $conn->query($sql);
                                    $user_data = $result->fetch_row(); 
                                       if(!empty($user_data['0']))
                                       {
                                          if(empty($user_data['2']))
                                          { ?>
                                                   <img src="assets/img/default.png" class="user-image" alt="User Image">
                                        <?php }                                                  else
                                                  { ?>
                                                         <img src="assets/img/uploads/users/<?php echo $user_data['2'];?>" class="user-image" alt="User Image">        
                                                 <?php }
                                       }
                                    ?>
              
              <span class="hidden-xs"><?php echo $user_data['1'];?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <?php 
                   if(!empty($user_data['0']))
                                       {
                                          if(empty($user_data['2']))
                                          { ?>
                                                   <img src="assets/img/default.png" class="user-image" alt="User Image">
                                        <?php }
                                                  else
                                                  { ?>
                                                         <img src="assets/img/uploads/users/<?php echo $user_data['2'];?>" class="user-image" alt="User Image">        
                                                 <?php }
                                       }
                 ?>
                <p>
                  <?php echo $user_data['1'];?> 
                  <small><?php echo $user_data['9'];?></small>
                </p>
              </li>
              
                <!-- /.row -->
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="profile.php?uid=<?php echo $_SESSION["usr_id"];?>" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="logout.php" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <!-- <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li> -->
        </ul>
      </div>
    </nav>
  </header>


    <?php } 
    else
    {
       header("location:index.php");
    } ?>