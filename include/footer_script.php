<!-- jQuery 3.1.1 -->
<script src="plugins/jQuery/jquery-3.1.1.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="js/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="js/raphael-min.js"></script>
<script src="plugins/morris/morris.min.js"></script>
<!-- Sparkline -->
<script src="plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="js/moment.min.js"></script>
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<script src="assets/js/main.js"></script>
<script>
$(function() {
    $('#category').change(function() {
          $('.subcategory').hide();
          var v =  $('#category option:selected').val();
          $('#subcategory_' + v).show();
    });
});
</script>
<!-- jQuery 3.1.1 -->

<script src="plugins/datatables/jquery.dataTables.min.js"></script>

<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="js/multiple_selection.js"></script>
<!-- page script -->
<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true
    });
  });
</script>
<script type="text/javascript">
  $(document).on('click', 'a[href^="#"]', function(e) {
    // target element id
    var id = $(this).attr('href');

    // target element
    var $id = $(id);
    if ($id.length === 0) {
        return;
    }

    // prevent standard hash navigation (avoid blinking in IE)
    e.preventDefault();

    // top position relative to the document
    var pos = $id.offset().top;

    // animated top scrolling
    $('body, html').animate({scrollTop: pos});
});
</script>
<script>
$(document).ready(function() 
  {
    $('#sus_block').on('change', function() 
    {
      var isChecked = $(this).is(':checked');
      if(isChecked) 
      {
        var user_id_data = '<?php echo $uid;?>';
        var user_status = 1;
        $.ajax(
        {
          type:'POST',
          datatype:'json',
          url : 'change_user_status.php',
          data : {user_id:user_id_data,status:user_status},
            success : function($data)
            {
              console.log($data);
            }
        });
      } 
      else 
      {
        var user_id_data = '<?php echo $uid;?>';
        var user_status = 0;
        $.ajax(
        {
          type:'POST',
          datatype:'json',
          url : 'change_user_status.php',
          data : {user_id:user_id_data,status:user_status},
            success : function($data)
            {
              console.log($data);
            }
        }); 
      }

      console.log('Selected data: ' + selectedData);

    });
});
</script>
<script type="text/javascript">
  function check_uncheck_checkbox(isChecked) {
  if(isChecked) 
  {
    $('input[name="action"]').each(function() 
    { 
      this.checked = true; 
    });
  } else {
    $('input[name="action"]').each(function() {
      this.checked = false;
    });
  }
}
</script>
<script type="text/javascript">
    $(function() {
        // this will get the full URL at the address bar
        var url = window.location.href;

        // passes on every "a" tag
        $(".sidebar a").each(function() {
            // checks if its the same on the address bar
            if (url == (this.href)) {
                $(this).closest("li").addClass("active");
                //for making parent of submenu active
               $(this).closest("li").parent().parent().addClass("active");
            }
        });
    });        
</script>

<!-- user status script start-->
<script type="text/javascript">
  function deactivate()
  {
    $("input:checkbox[name=action]:checked").each(function () 
    {
            //console.log($(this).val());
         var user_status = 0;
         var user_id_data = $(this).val();
         var methodName = "deactivateUsers";
          $.ajax(
            {
              type:'POST',
              datatype:'json',
              url : 'user_status.php',
              data : {user_id:user_id_data,status:user_status,mtdnme:methodName},
              success : function(data)
              {
              var obj = $.parseJSON(data); 
            }
          });
    });
    alert("User Deactivated Successfully");
    location.reload();
  }

  function activate()
  {
    //alert("call");
    $("input:checkbox[name=action]:checked").each(function () 
    {
          console.log($(this).val());
         var user_status = 1;
         var user_id_data = $(this).val();
         var methodName = "activateUsers";
          $.ajax(
            {
              type:'POST',
              datatype:'json',
              url : 'user_status.php',
              data : {user_id:user_id_data,status:user_status,mtdnme:methodName},
              success : function(data)
              {
                var obj = $.parseJSON(data);
              }
          });
    });
    alert("User Activated Successfully");
    location.reload();
  }

  //Delete user
   function deleteUser()
   {
     $("input:checkbox[name=action]:checked").each(function () 
     {
          // console.log($(this).val());
          //var user_status = 1;
          var user_id_data = $(this).val();
           $.ajax(
             {
               type:'POST',
               datatype:'json',
               url : 'user_status.php',
               data : {user_delete_id:user_id_data},
             success : function(data)
              {
                 var obj = $.parseJSON(data);
            }
          });
     });
     alert("Select Ok For Delete Data");
     location.reload();
   }


</script>
<!-- user status script End -->

<!-- post status -->
<script type="text/javascript">
  function productDeactivate()
  {
    $("input:checkbox[name=action]:checked").each(function () 
    {
         var post_status = 0;
         var post_id_data = $(this).val();
          $.ajax(
            {
              type:'POST',
              datatype:'json',
              url : 'product_status.php',
              data : {post_id:post_id_data,status:post_status},
              success : function(data)
              {
              var obj = $.parseJSON(data);
              
            }
          });
    });
    alert("Post Deactivated Successfully");
    location.reload();
  }

  function productActivate()
  {
    $("input:checkbox[name=action]:checked").each(function () 
    {
          console.log($(this).val());
         var post_status = 1;
         var post_id_data = $(this).val();
          $.ajax(
            {
              type:'POST',
              datatype:'json',
              url : 'product_status.php',
              data : {post_id:post_id_data,status:post_status},
              success : function(data)
              {
                var obj = $.parseJSON(data);
              }
          });
    });
    alert("Post Activated Successfully");
    location.reload();
  }

  //Delete Post
  function deleteproduct()
   {
     $("input:checkbox[name=action]:checked").each(function () 
     {
          var post_id_data = $(this).val();
           $.ajax(
             {
               type:'POST',
               datatype:'json',
               url : 'product_status.php',
               data : {delet_post_id:post_id_data},
             success : function(data)
              {
                 var obj = $.parseJSON(data);
            }
          });
     });
     alert("Select Ok For Delete Data");
     location.reload();
   }
</script>
<!-- post status end -->

<!-- Category status -->
<script type="text/javascript">
  function categoryDeactivate()
  {
    $("input:checkbox[name=action]:checked").each(function () 
    {
         var category_status = 0;
         var category_id_data = $(this).val();
          $.ajax(
            {
              type:'POST',
              datatype:'json',
              url : 'category_status.php',
              data : {category_id:category_id_data,status:category_status},
              success : function(data)
              {
              var obj = $.parseJSON(data);
              
            }
          });
    });
    alert("Category Deactivated Successfully");
    location.reload();
  }

  function categoryActivate()
  {
    $("input:checkbox[name=action]:checked").each(function () 
    {
          console.log($(this).val());
         var category_status = 1;
         var category_id_data = $(this).val();
          $.ajax(
            {
              type:'POST',
              datatype:'json',
              url : 'category_status.php',
              data : {category_id:category_id_data,status:category_status},
              success : function(data)
              {
                var obj = $.parseJSON(data);
              }
          });
    });
    alert("Category Activated Successfully");
    location.reload();
  }

 function deleteCategory()
   {
     $("input:checkbox[name=action]:checked").each(function () 
     {
          var user_id_data = $(this).val();
           $.ajax(
             {
               type:'POST',
               datatype:'json',
               url : 'category_status.php',
               data : {cat_delete_id:user_id_data},
             success : function(data)
              {
                 var obj = $.parseJSON(data);
            }
          });
     });
     alert("Select Ok For Delete Data");
     location.reload();
   }
</script>
<!-- Category status end -->

<!-- subCategory status -->
<script type="text/javascript">
  function subCategoryDeactivate()
  {
    $("input:checkbox[name=action]:checked").each(function () 
    {
         var sub_category_status = 0;
         var sub_category_id_data = $(this).val();
          $.ajax(
            {
              type:'POST',
              datatype:'json',
              url : 'sub_category_status.php',
              data : {sub_category_id:sub_category_id_data,status:sub_category_status},
              success : function(data)
              {
              var obj = $.parseJSON(data);
              
            }
          });
    });
    alert("subCategory Deactivated Successfully");
    location.reload();
  }

  function subCategoryActivate()
  {
    $("input:checkbox[name=action]:checked").each(function () 
    {
          console.log($(this).val());
         var sub_category_status = 1;
         var sub_category_id_data = $(this).val();
          $.ajax(
            {
              type:'POST',
              datatype:'json',
              url : 'sub_category_status.php',
              data : {sub_category_id:sub_category_id_data,status:sub_category_status},
              success : function(data)
              {
                var obj = $.parseJSON(data);
              }
          });
    });
    alert("subCategory Activated Successfully");
    location.reload();
  }

  //Delete Subcategory
  function deleteSubCategory()
   {
     $("input:checkbox[name=action]:checked").each(function () 
     {
          var user_id_data = $(this).val();
           $.ajax(
             {
               type:'POST',
               datatype:'json',
               url : 'sub_category_status.php',
               data : {delet_sub_cat_id:user_id_data},
             success : function(data)
              {
                 var obj = $.parseJSON(data);
            }
          });
     });
     alert("Select Ok For Delete Data");
     location.reload();
   }
</script>
<!-- subCategory status end -->

<!-- Yoga -->
<script type="text/javascript">
  function yogaDeactivate()
  {
    $("input:checkbox[name=action]:checked").each(function () 
    {
         var yogaStatus = 0;
         var yoga_id_data = $(this).val();
          $.ajax(
            {
              type:'POST',
              datatype:'json',
              url : 'yogaStatus.php',
              data : {yogaId:yoga_id_data,status:yogaStatus},
              success : function(data)
              {
              var obj = $.parseJSON(data);
              
            }
          });
    });
    alert("Yoga Deactivated Successfully");
    location.reload();
  }

  function yogaActivate()
  {
    $("input:checkbox[name=action]:checked").each(function () 
    {
          console.log($(this).val());
         var yogaStatus = 1;
         var yoga_id_data = $(this).val();
          $.ajax(
            {
              type:'POST',
              datatype:'json',
              url : 'yogaStatus.php',
              data : {yogaId:yoga_id_data,status:yogaStatus},
              success : function(data)
              {
                var obj = $.parseJSON(data);
              }
          });
    });
    alert("Yoga Activate Successfully");
    location.reload();
  }

  //Delete Post
  function deleteYoga()
   {
     $("input:checkbox[name=action]:checked").each(function () 
     {
          var yoga_id_data = $(this).val();
           $.ajax(
             {
               type:'POST',
               datatype:'json',
               url : 'yogaStatus.php',
               data : {delet_yogaId:yoga_id_data},
             success : function(data)
              {
                 var obj = $.parseJSON(data);
            }
          });
     });
     alert("Yoga Deleted");
     location.reload();
   }
</script>
<!-- YogaEnd -->
<!-- AartiDelete -->
<script>
function deleteAarti()
   {
     $("input:checkbox[name=action]:checked").each(function () 
     {
          var aarti_id_data = $(this).val();
           $.ajax(
             {
               type:'POST',
               datatype:'json',
               url : 'yogaStatus.php',
               data : {delet_aartiId:aarti_id_data},
             success : function(data)
              {
                 var obj = $.parseJSON(data);
            }
          });
     });
     alert("Aarti Deleted");
     location.reload();
   }
 </script>
<!-- AartiEnd -->
<!-- AartiCatDelete -->
<script>
function deleteAartiCat()
   {
     $("input:checkbox[name=action]:checked").each(function () 
     {
          var aartiCat_id_data = $(this).val();
           $.ajax(
             {
               type:'POST',
               datatype:'json',
               url : 'yogaStatus.php',
               data : {delet_aartCatiId:aartiCat_id_data},
             success : function(data)
              {
                 var obj = $.parseJSON(data);
            }
          });
     });
     alert("God Name Deleted");
     location.reload();
   }
 </script>
<!-- AartiCatEnd -->
<!-- AartiSubCatDelete -->
<script>
function deleteAartiSubCat()
   {
     $("input:checkbox[name=action]:checked").each(function () 
     {
          var aartiSubCat_id_data = $(this).val();
           $.ajax(
             {
               type:'POST',
               datatype:'json',
               url : 'yogaStatus.php',
               data : {delet_aartSubCatiId:aartiSubCat_id_data},
             success : function(data)
              {
                 var obj = $.parseJSON(data);
            }
          });
     });
     alert("Arti Name Deleted");
     location.reload();
   }
 </script>
<!-- AartiSubCatEnd -->
<!-- PoojaDelete -->
<script>
function deletePooja()
   {
     $("input:checkbox[name=action]:checked").each(function () 
     {
          var pooja_id_data = $(this).val();
           $.ajax(
             {
               type:'POST',
               datatype:'json',
               url : 'yogaStatus.php',
               data : {delet_poojaiId:pooja_id_data},
             success : function(data)
              {
                 var obj = $.parseJSON(data);
            }
          });
     });
     alert("Pooja Vidhi Deleted");
     location.reload();
   }
 </script>
<!-- PoojaEnd -->
<!-- poojaCatDelete -->
<script>
function deletePoojaCat()
   {
     $("input:checkbox[name=action]:checked").each(function () 
     {
          var poojaCat_id_data = $(this).val();
           $.ajax(
             {
               type:'POST',
               datatype:'json',
               url : 'yogaStatus.php',
               data : {delet_poojaCatiId:poojaCat_id_data},
             success : function(data)
              {
                 var obj = $.parseJSON(data);
            }
          });
     });
     alert("Pooja Name Deleted");
     location.reload();
   }
 </script>
<!-- poojaCatEnd -->
<!-- MantraDelete -->
<script>
function deleteMantra()
   {
     $("input:checkbox[name=action]:checked").each(function () 
     {
          var mantra_id_data = $(this).val();
           $.ajax(
             {
               type:'POST',
               datatype:'json',
               url : 'yogaStatus.php',
               data : {delet_mantraiId:mantra_id_data},
             success : function(data)
              {
                 var obj = $.parseJSON(data);
            }
          });
     });
     alert("Mantra Deleted");
     location.reload();
   }
 </script>
<!-- MantraEnd -->
<!-- mantraCatDelete -->
<script>
function deleteMantraCat()
   {
     $("input:checkbox[name=action]:checked").each(function () 
     {
          var mantraCat_id_data = $(this).val();
           $.ajax(
             {
               type:'POST',
               datatype:'json',
               url : 'yogaStatus.php',
               data : {delet_mantraCatiId:mantraCat_id_data},
             success : function(data)
              {
                 var obj = $.parseJSON(data);
            }
          });
     });
     alert("Mantra Name Deleted");
     location.reload();
   }
 </script>
<!-- mantraCatEnd -->
<!-- KathaDelete -->
<script>
function deleteKatha()
   {
     $("input:checkbox[name=action]:checked").each(function () 
     {
          var katha_id_data = $(this).val();
           $.ajax(
             {
               type:'POST',
               datatype:'json',
               url : 'yogaStatus.php',
               data : {delet_KathaId:katha_id_data},
             success : function(data)
              {
                 var obj = $.parseJSON(data);
            }
          });
     });
     alert("Katha Deleted");
     location.reload();
   }
 </script>
<!-- KathaEnd -->
<!-- kathaCatDelete -->
<script>
function deleteKathaCat()
   {
     $("input:checkbox[name=action]:checked").each(function () 
     {
          var kathaCat_id_data = $(this).val();
           $.ajax(
             {
               type:'POST',
               datatype:'json',
               url : 'yogaStatus.php',
               data : {delet_kathaCatiId:kathaCat_id_data},
             success : function(data)
              {
                 var obj = $.parseJSON(data);
            }
          });
     });
     alert("Katha Name Deleted");
     location.reload();
   }
 </script>
<!-- kathaCatEnd -->
<!-- ChalisaCatDelete -->
<script>
function deleteChalisa()
   {
     $("input:checkbox[name=action]:checked").each(function () 
     {
          var chalisa_id_data = $(this).val();
           $.ajax(
             {
               type:'POST',
               datatype:'json',
               url : 'yogaStatus.php',
               data : {delet_chalisaId:chalisa_id_data},
             success : function(data)
              {
                 var obj = $.parseJSON(data);
            }
          });
     });
     alert("Chalisa Deleted");
     location.reload();
   }
 </script>
<!-- chalisaCatEnd -->
<!-- ChalisaCatDelete -->
<script>
function deleteChalisaCat()
   {
     $("input:checkbox[name=action]:checked").each(function () 
     {
          var chalisaCat_id_data = $(this).val();
           $.ajax(
             {
               type:'POST',
               datatype:'json',
               url : 'yogaStatus.php',
               data : {delet_chalisaCatiId:chalisaCat_id_data},
             success : function(data)
              {
                 var obj = $.parseJSON(data);
            }
          });
     });
     alert("Chalisa Name Deleted");
     location.reload();
   }
 </script>
<!-- chalisaCatEnd -->
<!-- MeditationDelete -->
<script>
function deleteMeditation()
   {
     $("input:checkbox[name=action]:checked").each(function () 
     {
          var meditation_id_data = $(this).val();
           $.ajax(
             {
               type:'POST',
               datatype:'json',
               url : 'yogaStatus.php',
               data : {delet_meditationId:meditation_id_data},
             success : function(data)
              {
                 var obj = $.parseJSON(data);
            }
          });
     });
     alert("Meditation Deleted");
     location.reload();
   }
 </script>
<!-- MeditationEnd -->
<!-- MeditationCatDelete -->
<script>
function deleteMeditationCat()
   {
     $("input:checkbox[name=action]:checked").each(function () 
     {
          var meditationCat_id_data = $(this).val();
           $.ajax(
             {
               type:'POST',
               datatype:'json',
               url : 'yogaStatus.php',
               data : {delet_meditationCatiId:meditationCat_id_data},
             success : function(data)
              {
                 var obj = $.parseJSON(data);
            }
          });
     });
     alert("Meditation Name Deleted");
     location.reload();
   }
 </script>
<!-- MeditationCatEnd -->
<!-- HomeRemedies Delete -->
<script>
function deleteHomeRemedies()
   {
     $("input:checkbox[name=action]:checked").each(function () 
     {
          var remedies_id_data = $(this).val();
           $.ajax(
             {
               type:'POST',
               datatype:'json',
               url : 'yogaStatus.php',
               data : {delet_remediesId:remedies_id_data},
             success : function(data)
              {
                 var obj = $.parseJSON(data);
            }
          });
     });
     alert("Home Remedies  Deleted");
     location.reload();
   }
 </script>
<!-- RemediesEnd -->
<!-- homeRemediesCatDelete -->
<script>
function deleteHomeRemediesCat()
   {
     $("input:checkbox[name=action]:checked").each(function () 
     {
          var remediesCat_id_data = $(this).val();
           $.ajax(
             {
               type:'POST',
               datatype:'json',
               url : 'yogaStatus.php',
               data : {delet_remediesCatiId:remediesCat_id_data},
             success : function(data)
              {
                 var obj = $.parseJSON(data);
            }
          });
     });
     alert("Remedies Name Deleted");
     location.reload();
   }
 </script>
<!-- RemediesCatEnd -->
<!-- yogaCatDelete -->
<script>
function deleteYogaCat()
   {
     $("input:checkbox[name=action]:checked").each(function () 
     {
          var yogaCat_id_data = $(this).val();
           $.ajax(
             {
               type:'POST',
               datatype:'json',
               url : 'yogaStatus.php',
               data : {delet_yogaCatiId:yogaCat_id_data},
             success : function(data)
              {
                 var obj = $.parseJSON(data);
            }
          });
     });
     alert("Yoga Name Deleted");
     location.reload();
   }
 </script>
<!-- YogaCatEnd -->
