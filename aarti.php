 <?php 
 include "database.php";
?>

<?php require('include/head.php'); ?>
<body onload="initialize()" class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
<?php include "include/header.php";?>
<?php include "include/left_sidebar.php";?>
<div class="content-wrapper">
<section class="content-header">
<h1>
Aarti List
</h1>
<ol class="breadcrumb">
<li><a href="addAarti.php"><button type="button" class="btn btn-block " style="margin-top: -5px;">Add Aarti</button></a></li>
</ol>
</section>
<section class="content">
<div class="box">
<div class="box-header">
<h3 class="box-title">Aarti Table With Full Features</h3>
</div>
            <!-- /.box-header -->
            <div class="box-body table-responsive table-scroll-y">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                <th>S.NO</th>
                <th>God Name</th>
                <th>Aarti Name</th>
                <!-- <th>Title</th> -->
                <th>Aarti</th>
                <th>Image</th>
                <th>Video</th>
                <th>Audio</th>
                <th>Posted Date</th>
                <!-- <th>Status </th> -->
                <th>Action</th>
                  <th><input name="select-all" id="checkall" onClick="check_uncheck_checkbox(this.checked);" value="check_all" type="checkbox"></th>
                </tr>
                </thead>
                    <tbody>
                        <?php 
                            $sql = "SELECT a.id,a.status,al.aartiName AS godName,asl.aartiSubName AS aartiName,a.title,a.description AS Aarti,a.image,a.audio,a.video,a.created_date_time from aarti AS a  
                            LEFT JOIN aarti_sub_list AS asl ON a.aarti_sub_list_id=asl.id 
                            LEFT JOIN aarti_list AS al ON a.aarti_list_id=al.id  
                            Order By a.id";
                            //print_r($sql);exit;
                        $result = $conn->query($sql);
                        if ($result->num_rows>0)
                        {
                        $serial=0;
                        while($aartilist = $result->fetch_assoc())
                        {
                        	//print_r($productLst['product_video']);exit;
                        $desc=$aartilist['Aarti'];
                        $serial++;
                        ?>
                    <tr id="<?php  echo $aartilist['id'];?>">
                        <td><?php echo $serial; ?></td>
                        <td><?php  echo $aartilist['godName'];?></td>
                        <td><?php  echo $aartilist['aartiName'];?></td>
                        <!-- <td><?php  echo $aartilist['title'];?></td>
                        <td><?php  echo $aartilist['title'];?></td> -->
                        <td><?php  echo substr ($desc, 0, 20);?></td> 
                        <td><img src="assets/img/uploads/aarti/<?php echo $aartilist['image']; ?>" class="img-responsive" style="width:30px; height:30px" ></td>
                        <td><video  width="30px" height="30px" controls><source src="assets/img/uploads/aarti/<?php echo $aartilist['video']; ?>" class="img-responsive" style="width:30px; height:30px" type="video/mp4"></video></td>
                        <td><audio width="30px" height="30px" controls><source src="assets/img/uploads/aarti/<?php echo $aartilist['audio']; ?>" class="img-responsive" style="width:30px; height:30px" type="audio/ogg"></audio></td>
                         <td><?php  echo $aartilist['created_date_time'];?></td>
                        <!-- <?php 
                        if($aartilist['status'] == 1)
                        {
                        ?>
                        <td><img src="assets/img/enable.gif" class="img-responsive" title="Activated"></td>
                        <?php
                        }
                        ?>
                        <?php
                        if($aartilist['status'] == 0)
                        {
                        ?>
                        <td><img src="assets/img/disable.gif" class="img-responsive" title="Deactivated"></td>
                        <?php
                        }
                        ?> -->
                    <td>
                    <a href="aartiEdit.php?aartiId=<?php echo $aartilist['id']; ?>" style="cursor: pointer;">
                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i> </a>/
                    <a href="aartiView.php?aartiId=<?php echo $aartilist['id']; ?>" style="cursor: pointer;">
                    <i class="fa fa-eye" aria-hidden="true"></i> </a>
                    </td>
                    <td><input   type="checkbox" value="<?php echo $aartilist['id'];?>" name="action" id="checkbox-1"></td>
                    </tr> 
                    <?php 
                    }
                    } 
                    ?>
                    </tbody>
              </table>
<!-- <button class="btn btn-success" onclick="yogaActivate()">Activate</button>
<button class="btn btn-danger" onclick="yogaDeactivate()">Deactivate</button> -->
<button class="btn btn-danger" onclick="deleteAarti()">Delete</button>
</div>
</div>
</div>
<?php include "include/footer.php" ;?>
<?php include "include/right_sidebar.php" ;?>
</div>
<?php include "include/footer_script.php" ;?>
<script src='http://maps.googleapis.com/maps/api/js?v=3&sensor=false&amp;libraries=places&key=AIzaSyAL1eAba9HoD7qszOJ-ggOvZvbq-TvXDys'></script>
<script src="js/script.js"> </script>
</body>
</html>
