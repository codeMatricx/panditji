 <?php 
$id = $_GET['mantraId'];
include "database.php";
?>
<?php
if(isset($_POST["upd"]))
{
  //image
        if(empty($_FILES['image']['name']))
        {
        $image = $_POST['image_first'];
        }
        else
        {
        $target_dir = "assets/img/uploads/mantra/";
        $target_file = $target_dir . basename($_FILES["image"]["name"]);
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
        move_uploaded_file($_FILES["image"]["tmp_name"], $target_file);
        $image =  $_FILES["image"]["name"];
        //print_r($image);exit;
        $uploadOk = 1;
        }
//video
        if(empty($_FILES['video']['name']))
        {
        $video = $_POST['video_first'];
        }
        else
        {
        $target_dir_video = "assets/img/uploads/mantra/";
        $target_file_video = $target_dir_video . basename($_FILES['video']["name"]);
        $videoFileType = pathinfo($target_file_video,PATHINFO_EXTENSION);
        move_uploaded_file($_FILES["video"]["tmp_name"], $target_file_video);
        $video = $_FILES['video']["name"];
        $uploadOk = 1;
        }
//audio
        if(empty($_FILES['audio']['name']))
        {
        $audio = $_POST['audio_first'];
        }
        else
        {
        $target_dir_audio = "assets/img/uploads/mantra/";
        $target_file_audio = $target_dir_audio . basename($_FILES['audio']["name"]);
        $audioFileType = pathinfo($target_file_audio,PATHINFO_EXTENSION);
        move_uploaded_file($_FILES["audio"]["tmp_name"], $target_file_audio);
        $audio = $_FILES['audio']["name"];
        $uploadOk = 1;
        }

// $userId=$_POST["userId"];
$title=$_POST["title"];
$mantra_list_id=$_POST["mantra_list_id"];
// $subCategoryId=$_POST["subCategoryId"];
// $price=$_POST["price"];
$description=$_POST["description"];
$status = 1;
            
      $sql = "UPDATE mantra SET mantra_list_id='$mantra_list_id',title='$title',description='$description',image='$image',audio='$audio',video='$video' WHERE id='$id'";
      if ($conn->query($sql) === TRUE) 
      {
        header("location:mantra.php");
      } 
      else 
      {
      echo "Error: " . $sql . "<br>" . $conn->error;
      }
}
?>
<?php require('include/head.php'); ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
 <?php include "include/header.php";?> 
  <?php include "include/left_sidebar.php";?>
       <div class="content-wrapper">
            <section class="content-header">
              <h1>
              Update Mantra     
              </h1>
            </section>
            <section class="content">
        <div class="box">
        <div class="box-body table-responsive table-scroll-y">
        <form method="post" enctype="multipart/form-data">
                  
            <table id="example1" class="table table-bordered table-striped">
            <table  class="table table-bordered table-striped">
				<?php 
				$sql_update = "SELECT * from mantra WHERE id='$id'";
				$result_update = $conn->query($sql_update);
				if ($result_update->num_rows>0)
				{
				$mantraUpdate = $result_update->fetch_assoc();
				?>        
            <tbody>
              <tr>
                <th>God Name</th>
                <td>
                  <select name="mantra_list_id">
                    <?php 
                  $sql = "SELECT id,mantraName from mantra_list";
                  $result = $conn->query($sql);
                  if ($result->num_rows>0)
                  {
                  while($mantraList = $result->fetch_assoc())
                  {
                  ?>
                    <option value="<?php echo $mantraList['id'];?>"><?php echo $mantraList['mantraName'];?>
                    </option>
                     <?php } }?>
                  </select>
                 </td>
            </tr>
              <tr>
              <th>Title</th>
              <td><input type="text" name="title" value="<?php echo $mantraUpdate['title'];?>" placeholder="Add Step"></td>
              </tr>      
            <tr>
               <th>Image</th>
               <td><img src="assets/img/uploads/mantra/<?php echo $mantraUpdate['image']; ?>" class="img-responsive" style="width:30px; height:30px" ></td> 
              </tr>
              <tr>
              <th>Change Image</th>
                   <td><!-- <label for="newimage" class="btn text-muted text-center btn-success" style="width:20%;margin-top: -4px;padding: 12px;">image</label> -->
                   <input id="newimage" type="file" name="image">
                   <input type = "hidden" name = "image_first" id = "image_first" value = "<?php  echo $mantraUpdate['image'];?>">
               </td>
               </tr>
               
              <tr>
               <th>Video</th>
               <td><video  width="30px" height="30px" controls><source src="assets/img/uploads/mantra/<?php echo $mantraUpdate['video']; ?>" class="img-responsive" style="width:30px; height:30px" type="video/mp4"></video></td> 
              </tr>
              <tr>
              <th>Change Video</th>
                   <td><!-- <label for="newvideo" class="btn text-muted text-center btn-success" style="width:20%;margin-top: -4px;padding: 12px;">Video</label> -->
                   <input id="newvideo" type="file" name="video">
                   <input type = "hidden" name = "video_first" id = "video_first" value = "<?php  echo $mantraUpdate['video'];?>">
               </td>
               </tr>
               
            <tr>
               <th>Audio</th>
               <td><audio width="30px" height="30px" controls><source src="assets/img/uploads/mantra/<?php echo $mantraUpdate['audio']; ?>" class="img-responsive" style="width:30px; height:30px" type="audio/ogg"></audio></td> 
              </tr>
              <tr>
              <th>Change Audio</th>
                   <td><!-- <label for="newaudio" class="btn text-muted text-center btn-success" style="width:20%;margin-top: -4px;padding: 12px;">Audio</label> -->
                   <input id="newaudio" type="file" name="audio">
                   <input type = "hidden" name = "audio_first" id = "audio_first" value = "<?php  echo $mantraUpdate['audio'];?>">
               </td>
               </tr>
            <tr>
              <th>Mantra</th>
              <td>
                <textarea class="tinymce" id="mytextarea" name="description" placeholder="Add Mantra" value="<?php echo $mantraUpdate['description'];?>"></textarea>
                <!-- <input type="text" name="description" value="<?php echo $mantraUpdate['description'];?>" placeholder="Add Pooja Vidhi"> -->
              </td>
            </tr>
            
            </tbody>
            <?php }?>             
            </table>
            </table>
            <a href="mantra.php" style="color: #fff;"><button type="button" class="btn" style="margin-top: 10px" >Back</button></a>
            <button type="submit" class="btn   pull-right" name="upd" style="margin-top: 10px" >UPDATE</button>
            
        </form>
        </div>
        </div>
        </div>
  <?php include "include/footer.php" ;?>
  <?php include "include/right_sidebar.php" ;?>  
</div>
<?php include "include/footer_script.php" ;?>
<script src="jscolor.js"></script>
</body>
</html>
