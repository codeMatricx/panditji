<?php 
include "database.php";
?>
<?php
if(isset($_POST["sub"]))
{
$target_dir = "assets/img/uploads/category/";
$image = $_FILES['image']["name"];
$target_file = $target_dir . basename($_FILES['image']["name"]);
$uploadOk = 1;
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

$category_id=$_POST["category_id"];
$sub_category_name  =$_POST["sub_category_name"];
$status = 1;
$image="";

            if(empty($_FILES["image"]["tmp_name"]))
            {
            echo "Please choose File!";
            $uploadOk = 0;
            }
            else
            {
              if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
              && $imageFileType != "gif" ) 
              {
              echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
              $uploadOk = 0;
              }
              $check = getimagesize($_FILES["image"]["tmp_name"]);
              if($check != true) 
              {
              echo "File is not an image.";
              $uploadOk = 0;
              }
              else
              { 
                if ($_FILES["image"]["size"] > 500000)
                {
                echo "Sorry, your file is too large.";
                $uploadOk = 0;
                }
                else
                  {
                    $uploadOk = 1;
                  }
              }
            }
            if($uploadOk == 1)
            {
              if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file))
              {
              $image =  $_FILES["image"]["name"];
              }
            }

      $sql = "INSERT INTO sub_category (categoryId,subCategoryName,image)
      VALUES ('$category_id','$sub_category_name','$image')";
      if ($conn->query($sql) === TRUE) 
      {
      header("location:sub_category.php");
      } 
      else 
      {
      echo "Error: " . $sql . "<br>" . $conn->error;
      }
}
?>
<?php require('include/head.php'); ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
 <?php include "include/header.php";?> 
  <?php include "include/left_sidebar.php";?>
       <div class="content-wrapper">
            <section class="content-header">
              <h1>
              Add Sub Category     
              </h1>
            </section>
            <section class="content">
        <div class="box">
        <div class="box-body table-responsive table-scroll-y">
        <form method="post" enctype="multipart/form-data">
                  
            <table id="example1" class="table table-bordered table-striped">
            <table  class="table table-bordered table-striped">           
            <tbody>
            <tr>
            <th>Category Name</th>
              <td><select name="category_id" style="margin-left:0px;" required>
              <?php 
              $sql = "SELECT * from category";
              $result = $conn->query($sql);
              if ($result->num_rows>0)
              {
              $serial=0;
              while($category = $result->fetch_assoc())
              {
              ?>
              <option value="<?php echo $category['id'];?>" selected disable><?php echo $category['categoryName'];?></option>   
              <?php 
              } }
              ?>       
              </select></td> 
            </tr>
            <tr>
              <th>Sub Category</th>
              <td><input id="name" name="sub_category_name" placeholder="Sub Category Name" type="text" required></td>
            </tr>
            <tr>
              <th>Add Icon</th>
                <td><!-- <label for="newimage" class="btn text-muted text-center " style="width:82%;margin-top: 2%;">Choose Image</label> -->
                    <input id="newimage" type="file" name="image" required> 
              </td>
            </tr>
            
            </tbody>             
            </table>
            </table>
            <a href="sub_category.php" style="color: #fff;"><button type="button" class="btn" style="margin-top: 10px" >Back</button></a>
            <button type="submit" class="btn   pull-right" name="sub" style="margin-top: 10px" >Add</button>
            
        </form>
        </div>
        </div>
        </div>
  <?php include "include/footer.php" ;?>
  <?php include "include/right_sidebar.php" ;?>  
</div>
<?php include "include/footer_script.php" ;?>
<script src="jscolor.js"></script>
</body>
</html>
