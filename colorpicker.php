<!DOCTYPE html>
<html>
<head>
    <title>Basic usage</title>
</head>
<body>


<script src="jscolor.js"></script>

<button class="jscolor
    {valueElement:'valueInput', styleElement:'styleInput'}">
    Click here to pick a color
</button>

<p>
Value: <input id="valueInput" value="ff6699"><br>
Style: <input id="styleInput">

</body>
</html>