 <?php 
$id = $_GET['remediesId'];
include "database.php";
?>
<?php
if(isset($_POST["upd"]))
{
  //image
        if(empty($_FILES['image']['name']))
        {
        $image = $_POST['image_first'];
        }
        else
        {
        $target_dir = "assets/img/uploads/homeremedies/";
        $target_file = $target_dir . basename($_FILES["image"]["name"]);
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
        move_uploaded_file($_FILES["image"]["tmp_name"], $target_file);
        $image =  $_FILES["image"]["name"];
        $uploadOk = 1;
        }
//video
        if(empty($_FILES['video']['name']))
        {
        $video = $_POST['video_first'];
        }
        else
        {
        $target_dir_video = "assets/img/uploads/homeremedies/";
        $target_file_video = $target_dir_video . basename($_FILES['video']["name"]);
        $videoFileType = pathinfo($target_file_video,PATHINFO_EXTENSION);
        move_uploaded_file($_FILES["video"]["tmp_name"], $target_file_video);
        $video = $_FILES['video']["name"];
        $uploadOk = 1;
        }
//audio
        if(empty($_FILES['audio']['name']))
        {
        $audio = $_POST['audio_first'];
        }
        else
        {
        $target_dir_audio = "assets/img/uploads/homeremedies/";
        $target_file_audio = $target_dir_audio . basename($_FILES['audio']["name"]);
        $audioFileType = pathinfo($target_file_audio,PATHINFO_EXTENSION);
        move_uploaded_file($_FILES["audio"]["tmp_name"], $target_file_audio);
        $audio = $_FILES['audio']["name"];
        $uploadOk = 1;
        }

$title=$_POST["title"];
$remedies_list_id=$_POST["remedies_list_id"];
$description=$_POST["description"];
$status = 1;
            
      $sql = "UPDATE home_remedies SET remedies_list_id='$remedies_list_id',title='$title',description='$description',image='$image',audio='$audio',video='$video' WHERE id='$id'";
      if ($conn->query($sql) === TRUE) 
      {
        header("location:homeRemedies.php");
      } 
      else 
      {
      echo "Error: " . $sql . "<br>" . $conn->error;
      }
}
?>
<?php require('include/head.php'); ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
 <?php include "include/header.php";?> 
  <?php include "include/left_sidebar.php";?>
       <div class="content-wrapper">
            <section class="content-header">
              <h1>
              Update Remedies     
              </h1>
            </section>
            <section class="content">
        <div class="box">
        <div class="box-body table-responsive table-scroll-y">
        <form method="post" enctype="multipart/form-data">
                  
            <table id="example1" class="table table-bordered table-striped">
            <table  class="table table-bordered table-striped">
				<?php 
				$sql_update = "SELECT * from home_remedies WHERE id='$id'";
				$result_update = $conn->query($sql_update);
				if ($result_update->num_rows>0)
				{
				$remediesUpdate = $result_update->fetch_assoc();
				?>        
            <tbody>
              <tr>
                <th>Home Rmemedies Name</th>
                <td>
                  <select name="remedies_list_id">
                    <?php 
                  $sql = "SELECT id,remediesName from home_remedies_list";
                  $result = $conn->query($sql);
                  if ($result->num_rows>0)
                  {
                  while($remediesList = $result->fetch_assoc())
                  {
                  ?>
                    <option value="<?php echo $remediesList['id'];?>"><?php echo $remediesList['remediesName'];?>
                    </option>
                     <?php } }?>
                  </select>
                 </td>
            </tr>
            <tr>
              <th>Step</th>
              <td><input type="text" name="title" value="<?php echo $remediesUpdate['title'];?>" placeholder="Add Step"></td>
            </tr>
                    
            <tr>
               <th>Image</th>
               <td><img src="assets/img/uploads/homeremedies/<?php echo $remediesUpdate['image']; ?>" class="img-responsive" style="width:30px; height:30px" ></td> 
              </tr>
              <tr>
              <th>Change Image</th>
                   <td>
                   <input id="newimage" type="file" name="image">
                   <input type = "hidden" name = "image_first" id = "image_first" value = "<?php  echo $remediesUpdate['image'];?>">
               </td>
               </tr>
               
              <tr>
               <th>Video</th>
               <td><video  width="30px" height="30px" controls><source src="assets/img/uploads/homeremedies/<?php echo $remediesUpdate['video']; ?>" class="img-responsive" style="width:30px; height:30px" type="video/mp4"></video></td> 
              </tr>
              <tr>
              <th>Change Video</th>
                   <td>
                   <input id="newvideo" type="file" name="video">
                   <input type = "hidden" name = "video_first" id = "video_first" value = "<?php  echo $remediesUpdate['video'];?>">
               </td>
               </tr>
               
            <tr>
               <th>Audio</th>
               <td><audio width="30px" height="30px" controls><source src="assets/img/uploads/homeremedies/<?php echo $remediesUpdate['audio']; ?>" class="img-responsive" style="width:30px; height:30px" type="audio/ogg"></audio></td> 
              </tr>
              <tr>
              <th>Change Audio</th>
                   <td>
                   <input id="newaudio" type="file" name="audio">
                   <input type = "hidden" name = "audio_first" id = "audio_first" value = "<?php  echo $remediesUpdate['audio'];?>">
               </td>
               </tr>
            <tr>
              <th>Remedies Description</th>
              <td>
                <textarea class="tinymce" id="mytextarea" name="description" placeholder="Add Remedies" value="<?php echo $remediesUpdate['description'];?>"></textarea>
                <!-- <input type="text" name="description" value="<?php echo $remediesUpdate['description'];?>" placeholder="Add Home Remedies"> -->
              </td>
            </tr>
            
            </tbody>
            <?php }?>             
            </table>
            </table>
            <a href="homeRemedies.php" style="color: #fff;"><button type="button" class="btn" style="margin-top: 10px" >Back</button></a>
            <button type="submit" class="btn   pull-right" name="upd" style="margin-top: 10px" >UPDATE</button>
            
        </form>
        </div>
        </div>
        </div>
  <?php include "include/footer.php" ;?>
  <?php include "include/right_sidebar.php" ;?>  
</div>
<?php include "include/footer_script.php" ;?>
<script src="jscolor.js"></script>
</body>
</html>
