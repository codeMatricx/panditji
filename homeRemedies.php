 <?php 
 include "database.php";
?>
<?php require('include/head.php'); ?>
<body onload="initialize()" class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
<?php include "include/header.php";?>
<?php include "include/left_sidebar.php";?>
<div class="content-wrapper">
<section class="content-header">
<h1>
Home Remedies List
</h1>
<ol class="breadcrumb">
<li><a href="addHomeRemedies.php"><button type="button" class="btn btn-block " style="margin-top: -5px;">Add Home Remedies</button></a></li>
</ol>
</section>
<section class="content">
<div class="box">
<div class="box-header">
<h3 class="box-title">Remedies Table With Full Features</h3>
</div>
            <!-- /.box-header -->
            <div class="box-body table-responsive table-scroll-y">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                <th>S.NO</th>
                <th>Home Remedies Name</th>
                <th>Step</th>
                <th>Remedies Description</th>
                <th>Image</th>
                <th>Video</th>
                <th>Audio</th>
                <th>Posted Date</th>
                <th>Action</th>
                  <th><input name="select-all" id="checkall" onClick="check_uncheck_checkbox(this.checked);" value="check_all" type="checkbox"></th>
                </tr>
                </thead>
                    <tbody>
                        <?php 
                            $sql = "SELECT hr.id,hr.title,hr.description,hr.image,hr.audio,hr.video,hr.created_date_time,hrl.remediesName FROM home_remedies AS hr LEFT JOIN home_remedies_list AS hrl ON hr.remedies_list_id=hrl.id";
                        $result = $conn->query($sql);
                        if ($result->num_rows>0)
                        {
                        $serial=0;
                        while($remedies = $result->fetch_assoc())
                        {
                        $desc=$remedies['description'];
                        $serial++;
                        ?>
                    <tr id="<?php  echo $remedies['id'];?>">
                        <td><?php echo $serial; ?></td>
                        <td><?php  echo $remedies['remediesName'];?></td>
                        <td><?php  echo $remedies['title'];?></td>
                        <td><?php  echo substr ($desc, 0, 20);?></td> 
                        <td><img src="assets/img/uploads/homeremedies/<?php echo $remedies['image']; ?>" class="img-responsive" style="width:30px; height:30px" ></td>
                        <td><video  width="30px" height="30px" controls><source src="assets/img/uploads/homeremedies/<?php echo $remedies['video']; ?>" class="img-responsive" style="width:30px; height:30px" type="video/mp4"></video></td>
                        <td><audio width="30px" height="30px" controls><source src="assets/img/uploads/homeremedies/<?php echo $remedies['audio']; ?>" class="img-responsive" style="width:30px; height:30px" type="audio/ogg"></audio></td>
                        <td><?php  echo $remedies['created_date_time'];?></td>
                       
                    <td >
                    <a href="homeRemediesEdit.php?remediesId=<?php echo $remedies['id']; ?>" style="cursor: pointer;">
                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i> </a>/
                    <a href="homeremediesView.php?remediesId=<?php echo $remedies['id']; ?>" style="cursor: pointer;">
                    <i class="fa fa-eye" aria-hidden="true"></i> </a>
                    </td>
                    <td><input   type="checkbox" value="<?php echo $remedies['id'];?>" name="action" id="checkbox-1"></td>
                    </tr> 
                    <?php 
                    }
                    } 
                    ?>
                    </tbody>
              </table>
<button class="btn btn-danger" onclick="deleteHomeRemedies()">Delete</button>
</div>
</div>
</div>
<?php include "include/footer.php" ;?>
<?php include "include/right_sidebar.php" ;?>
</div>
<?php include "include/footer_script.php" ;?>
<script src='http://maps.googleapis.com/maps/api/js?v=3&sensor=false&amp;libraries=places&key=AIzaSyAL1eAba9HoD7qszOJ-ggOvZvbq-TvXDys'></script>
<script src="js/script.js"> </script>
</body>
</html>
