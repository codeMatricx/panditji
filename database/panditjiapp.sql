-- phpMyAdmin SQL Dump
-- version 4.0.10.18
-- https://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Jan 25, 2018 at 07:13 AM
-- Server version: 5.6.36-cll-lve
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `panditjiapp`
--

-- --------------------------------------------------------

--
-- Table structure for table `aarti`
--

CREATE TABLE IF NOT EXISTS `aarti` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aarti_list_id` int(11) NOT NULL,
  `aarti_sub_list_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `audio` varchar(255) DEFAULT NULL,
  `video` varchar(255) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `aarti`
--

INSERT INTO `aarti` (`id`, `aarti_list_id`, `aarti_sub_list_id`, `title`, `description`, `image`, `audio`, `video`, `status`, `created_date_time`) VALUES
(4, 3, 4, '', '<p>Ganesh,<br />Jai&nbsp;<strong>Ganesh Deva</strong>.<br />Mata jaki&nbsp;<strong>Parvati</strong>,<br />pita&nbsp;<strong>Mahadeva</strong>.</p>\r\n<p><strong>Ek dant</strong>&nbsp;dayaavant,<br /><strong>chaar bhuja</strong>&nbsp;dhaari.<br /><strong>Maathe par tilak</strong>&nbsp;sohe,<br />moose ki savaari.</p>\r\n<p>Paan chadhe phool chadhe,<br />aur chadhe meva.<br />Laduvan ka bhog lage,<br /><strong>sant kare seva</strong>.</p>\r\n<p>Jai Ganesh, Jai&nbsp;<strong><a href="http://www.bhajansandhya.com/aarti/ganpati-ki-seva-mangal-meva-lyrics.html">Ganesh,</a></strong><br />Jai Ganesh Deva.<br />Mata jaki Parvati,<br />pita&nbsp;<a href="http://www.bhajansandhya.com/aarti/om-jai-shiv-omkara-lyrics.html"><strong>Mahadeva.</strong></a></p>\r\n<p><strong>Andhan ko aankh</strong>&nbsp;det,<br /><strong>kodhin ko kaaya</strong>.<br /><strong>Baanjhan ko putra</strong>&nbsp;det,<br /><strong>nirdhan ko maaya</strong>.</p>\r\n<p><strong>Sur shyaam</strong>&nbsp;sharan aaye,<br />saphal kije seva.<br />Mata jaki&nbsp;<strong>Parvati</strong>,<br />pita&nbsp;<strong>Mahadeva</strong>.</p>\r\n<p>Jai Ganesh, Jai Ganesh,<br />Jai Ganesh Deva.<br />Mata jaki Parvati,<br />pita Mahadeva.</p>\r\n<p><strong>Jai Ganesh</strong>, Jai Ganesh,<br />Jai Ganesh Deva.<br />Mata jaki Parvati,<br />pita Mahadeva.</p>\r\n<h3>Shlok</h3>\r\n<p>Vrakatund Mahaakaay,<br />Suryakoti Samaprabhaah.<br />Nirvaghnam Kuru me Dev,<br />Sarvakaaryeshu Sarvada.</p>\r\n<p>"&gt;</p>', 'ganesh.jpg', '', '', 0, '2018-01-25 06:08:24'),
(5, 3, 5, '', '<p>Sukhkarta Dukhharta Varta Vighnachi.<br />Nurvi Purvi Prem Kripa Jayachi.</p>\r\n<p>Sarvaangi Sundar Uti Shenduraachi.<br />Kanthi Jhalake Maal Muktaa Phadanchi.</p>\r\n<p>Jai Dev, Jai Dev, Jai Mangal Murti.<br />Darshan-matre Mann Kamana-purti.<br />Jai Dev, Jai Dev</p>\r\n<p>Jai Dev, Jai Dev,<br />Jai Mangal Murti, O Shri Mangal Murti.<br />Darshan Matre Mann Kamana Purti.<br />Jai Dev, Jai Dev</p>', 'ganesh.jpg', '', '', 0, '2018-01-25 11:09:11'),
(6, 3, 6, '', '<p>Ganpati ki seva mangal meva,<br />seva se sab vighna tare.</p>\r\n<p>Tin lok taitis devta,<br />dwar khade sab arj kare.</p>\r\n<p>Riddhi-Siddhi dakshin vaam viraaje,<br />aru aanand so chavar kare.<br />Dhoop deep aur liye aarti,<br />bhakt khade jaykaar kare.</p>\r\n<p>Gud ke modak bhog lagat hai,<br />mushak vaahan chadha kare.<br />Saumya-roop seva Ganpati ki,<br />vighna bhaag-ja door pare.</p>', 'ganesh.jpg', '', '', 0, '2018-01-25 11:10:40'),
(7, 3, 7, '', '<p>Aarti Gajvadan Vinayak ki.<br />Sur muni-poojit Gananaayak ki.</p>\r\n<p>Aarti Gajvadan Vinayak ki.<br />Sur muni-poojit Gananaayak ki.</p>\r\n<p>Ekadant, shashibhaal, gajaanan,<br />Vighnavinaashak, shubhagun kaanan,<br />Shivasut, Vandyamaan-chaturaanan,<br />Dukhavinaashak, sukhadaayak ki.</p>\r\n<p>Aarti Gajvadan Vinayak ki.<br />Sur muni-poojit Gananaayak ki.</p>\r\n<p>Rddhi-siddhi svaami samarth ati,<br />Vimal buddhi daata suvimal-mati,<br />Agh-van-dahan, amal avigat gati,<br />Vidya, vinay-vibhav daayak ki.</p>\r\n<p>Aarti Gajvadan Vinayak ki.<br />Sur muni-poojit Gananaayak ki.</p>', 'ganesh.jpg', '', '', 0, '2018-01-25 11:12:16');

-- --------------------------------------------------------

--
-- Table structure for table `aarti_list`
--

CREATE TABLE IF NOT EXISTS `aarti_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aartiName` varchar(255) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `aarti_list`
--

INSERT INTO `aarti_list` (`id`, `aartiName`, `image`, `created_date_time`) VALUES
(4, 'Lakshmi Aarti', 'lakshmipuja.jpg', '2018-01-25 05:45:49'),
(2, 'Hanuman Aarti', 'hanuman.jpg', '2018-01-22 08:16:41'),
(3, 'Ganesh  Aarti', 'ganesh.jpg', '2018-01-23 12:45:05'),
(5, 'Vishnu Aarti', 'vishnuarti.jpg', '2018-01-25 05:48:24'),
(6, 'Shiv Aarti', 'shivarti.jpg', '2018-01-25 05:48:51'),
(7, 'Durga Aarti', 'durgaarti.jpg', '2018-01-25 05:49:47'),
(8, 'Shani Dev Aarti', 'shanidev.jpg', '2018-01-25 05:55:55'),
(9, 'Sai Baba Aarti', 'saibaba.jpg', '2018-01-25 05:58:49');

-- --------------------------------------------------------

--
-- Table structure for table `aarti_sub_list`
--

CREATE TABLE IF NOT EXISTS `aarti_sub_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aarti_list_id` int(11) NOT NULL,
  `aartiSubName` varchar(255) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `aarti_sub_list`
--

INSERT INTO `aarti_sub_list` (`id`, `aarti_list_id`, `aartiSubName`, `image`, `created_date_time`) VALUES
(5, 3, 'Sukhkarta Dukhharta â€“ Jai Dev Jai Dev', 'ganesh.jpg', '2018-01-25 06:03:44'),
(4, 3, 'Jai Ganesh, Jai Ganesh Deva', 'ganesh.jpg', '2018-01-25 06:02:58'),
(6, 3, 'Ganpati Ki Seva Mangal Meva', 'ganesh.jpg', '2018-01-25 06:04:17'),
(7, 3, 'Aarti Gajvadan Vinayak Ki', 'ganesh.jpg', '2018-01-25 06:05:12'),
(8, 3, 'Shri Ganpati Bhaj Pragat Parvati', 'ganesh.jpg', '2018-01-25 06:06:04');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `categoryName` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `category_status` tinyint(4) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=28 ;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `categoryName`, `image`, `category_status`, `created_date_time`) VALUES
(16, 'Yoga', 'yoga.png', 1, '2018-01-08 07:25:57'),
(17, 'Arti', 'Aarti.jpg', 1, '2018-01-08 07:26:10'),
(18, 'Puja Vidhi', 'pujavidhi.png', 1, '2018-01-08 07:26:20'),
(19, 'Mantras', 'mantra.png', 1, '2018-01-08 07:26:27'),
(20, 'Katha', 'kathaa.jpg', 1, '2018-01-08 07:26:35'),
(21, 'Meditation', 'meditation.png', 1, '2018-01-08 07:26:43'),
(22, 'Home Remedies', 'home remedies.jpg', 1, '2018-01-08 07:27:01'),
(23, 'Ayurveda', 'aayurveda.jpg', 1, '2018-01-08 07:27:18'),
(27, 'Chalisa', 'chalisa.png', 1, '2018-01-08 07:29:38');

-- --------------------------------------------------------

--
-- Table structure for table `chalisa`
--

CREATE TABLE IF NOT EXISTS `chalisa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `chalisa_list_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `image` varchar(255) NOT NULL,
  `audio` varchar(255) DEFAULT NULL,
  `video` varchar(255) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `chalisa`
--

INSERT INTO `chalisa` (`id`, `chalisa_list_id`, `title`, `description`, `image`, `audio`, `video`, `status`, `created_date_time`) VALUES
(1, 2, 'Shre Hanuman Chalisa', 'Shri Guru charan saroj raj,Nij man mukut sudhar', 'hanuman.jpg', 'Om-Mantra-Preview.mp3', 'vid-20140210-wa0001.mp4', 0, '2018-01-22 11:50:31'),
(3, 1, '', '<p>Pehle Sai ke charno mein, apna sheesh nivauo maiy,<br />Kaise Shirdi Sai aaye, saara haal sunau maiy. ||1||<br /><br />Kaun hai mata, pita kaun hai, yeh na kisi ne bhi jaana,<br />Kaha janam Sai ne dhara, prashan paheli raha bana. ||2||<br /><br />Koee kahe Ayodhya ke, yeh Ramchandra bhagvan hain,<br />Koee kehta Saibaba, pavan putra Hanuman hain. ||3||<br /><br />Koee kehta mangal murti, Shri Gajanan hain Sai,<br />Koee kehta Gokul-Mohan Devki Nandan hain Sai. ||4||<br /><br />Shanker samajh bhakt kaee to, baba ko bhajhte rahte,<br />Koee kahe avtar datt ka, pooja Sai ki karte. ||5||<br /><br />Kuch bhi mano unko tum, pur sai hain sachche bhagvan,<br />Bade dayalu deen-bandhu, kitno ko diya jivan-daan. ||6||</p>', 'saibaba.jpg', '', '', 0, '2018-01-25 10:43:02');

-- --------------------------------------------------------

--
-- Table structure for table `chalisa_list`
--

CREATE TABLE IF NOT EXISTS `chalisa_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `chalisaName` varchar(255) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `chalisa_list`
--

INSERT INTO `chalisa_list` (`id`, `chalisaName`, `image`, `created_date_time`) VALUES
(1, 'Sai Baba Chalisa', 'saibaba.jpg', '2018-01-22 11:39:27'),
(2, 'Shri Hanuman Chalisa', 'hanuman.jpg', '2018-01-22 11:39:27');

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE IF NOT EXISTS `gender` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `gender_name` varchar(255) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`id`, `gender_name`, `created_date_time`) VALUES
(1, 'Male', '2017-12-19 10:43:35'),
(2, 'Female', '2017-12-19 10:43:50'),
(3, 'Others', '2017-12-19 10:44:08');

-- --------------------------------------------------------

--
-- Table structure for table `home_remedies`
--

CREATE TABLE IF NOT EXISTS `home_remedies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `remedies_list_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `audio` varchar(255) DEFAULT NULL,
  `video` varchar(255) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `home_remedies`
--

INSERT INTO `home_remedies` (`id`, `remedies_list_id`, `title`, `description`, `image`, `audio`, `video`, `status`, `created_date_time`) VALUES
(4, 1, 'STEP 1 : Cleanse', '<p>To prep yourself for a facial, you need to start by washing your hands. Remember, you will be using your hands to massage and treat your face. So they must be clean. Wash them with warm water and a simple bar soap. Your hands carry germs that can lead to breakouts on your skin.&nbsp;</p>\r\n<p>If you want to use only natural ingredients while doing a facial at home, then you can use pure honey as a face cleanser. Yes! It is not a conventional face cleanser, but it has many skin benefits and can be used for all skin types.</p>', 'home remedies.jpg', '', '', 0, '2018-01-25 11:01:47'),
(5, 1, 'STEP 2 : Exfoliate', '<ul>\r\n<li>This is an important step. Exfoliation helps remove dead skin, reduces large pores and prevents fine lines and wrinkles.</li>\r\n</ul>', 'home remedies.jpg', '', '', 0, '2018-01-25 11:03:38'),
(6, 2, '1. Massage your scalp', '<p>This is the first step to start a hair treatment at home. Warm up some coconut oil or olive oil. Massage your head gently. This increases blood circulation and boosts hair growth.</p>', 'homeremedies.png', '', '', 0, '2018-01-25 11:05:04'),
(7, 2, '2. Steam the hair', '<p>Dip a towel in warm water and squeeze the excess water out. Wrap the towel around the hair. This allows the oil to penetrate deep into the scalp. Do this for about 5-6 minutes.</p>', 'homeremedies.png', '', '', 0, '2018-01-25 11:05:33');

-- --------------------------------------------------------

--
-- Table structure for table `home_remedies_list`
--

CREATE TABLE IF NOT EXISTS `home_remedies_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `remediesName` varchar(255) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `home_remedies_list`
--

INSERT INTO `home_remedies_list` (`id`, `remediesName`, `image`, `created_date_time`) VALUES
(1, 'Facial', 'home remedies.jpg', '2018-01-22 12:50:06'),
(2, 'Hair Treatment', 'homeremedies.png', '2018-01-22 12:50:06');

-- --------------------------------------------------------

--
-- Table structure for table `inner_sub_category`
--

CREATE TABLE IF NOT EXISTS `inner_sub_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `categoryId` int(11) NOT NULL,
  `subCategoryId` int(11) NOT NULL,
  `innerSubCategoryName` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `inner_sub_category`
--

INSERT INTO `inner_sub_category` (`id`, `categoryId`, `subCategoryId`, `innerSubCategoryName`, `image`, `created_date_time`) VALUES
(1, 17, 62, 'Jai Dev Jai Dev', '', '2018-01-20 07:21:16');

-- --------------------------------------------------------

--
-- Table structure for table `katha`
--

CREATE TABLE IF NOT EXISTS `katha` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `katha_list_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `audio` varchar(255) DEFAULT NULL,
  `video` varchar(255) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `katha`
--

INSERT INTO `katha` (`id`, `katha_list_id`, `title`, `description`, `image`, `audio`, `video`, `status`, `created_date_time`) VALUES
(6, 3, '', '<p>Lord Krishna was Lila-Purushottama, the divine sportive form of God. He was the Yogeshwar and at the same time, the embodiment of pure love. Each and every divine sport of His life is infused with sublimity and profound secrets. Their underlying sports were relevent not only in that era but hold paramount significance in today age.</p>', 'bhagvatkatha.jpg', '', '', 0, '2018-01-25 10:24:58'),
(7, 2, '', '<p>This story is connected with Monday fast. Monday fast is practiced to propitiate Lord Shiva and Parvati. The panchakshara Mantra "Om Namah Shivaya" should be repeated on this day. The Monday fast is up to the third phase.</p>', 'shivkatha.jpg', '', '', 0, '2018-01-25 10:25:33');

-- --------------------------------------------------------

--
-- Table structure for table `katha_list`
--

CREATE TABLE IF NOT EXISTS `katha_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kathaName` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `katha_list`
--

INSERT INTO `katha_list` (`id`, `kathaName`, `image`, `created_date_time`) VALUES
(2, 'Somvar Vrat Katha', 'kathaa.jpg', '2018-01-22 11:07:41'),
(3, 'Shree Bhagvat Katha', 'katha.jpg', '2018-01-24 05:26:32');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE IF NOT EXISTS `login` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `user_id` int(255) NOT NULL,
  `session_id` varchar(255) NOT NULL,
  `device_token` varchar(255) NOT NULL,
  `device_type` varchar(255) NOT NULL,
  `user_login_type` int(11) DEFAULT '0',
  `login_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `mantra`
--

CREATE TABLE IF NOT EXISTS `mantra` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mantra_list_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `audio` varchar(255) DEFAULT NULL,
  `video` varchar(255) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `mantra`
--

INSERT INTO `mantra` (`id`, `mantra_list_id`, `title`, `description`, `image`, `audio`, `video`, `status`, `created_date_time`) VALUES
(1, 2, 'Om Namah Shivay', 'Om Namah Shivay', 'shivpuja.jpg', 'Om-Mantra-Preview.mp3', 'vid-20140210-wa0001.mp4', 0, '2018-01-22 10:36:22'),
(2, 2, 'Om Namah Shivaya', '<p>In simple terms, this mantra means &lsquo;I bow to Shiva&rsquo;. However, when we explore at a deeper level, the meaning of this mantra unravels in a thought-provoking manner.</p>', 'shivmntra.jpg', '', '', 0, '2018-01-25 09:19:54'),
(3, 2, 'Om Tryambakam Yajamahe Sugandhim Pushti-Vardhanam  Urvarukamiva Bandhanan Mrityormukshiya Mamritat', '<p>OM. We worship the Three-eyed Lord who is scented and who nourishes and nurtures all living beings. As is the grown cucumber is freed from its bondage (the creeper), may He set us free from death for the sake of immortality.</p>', 'shivmntra.jpg', '', '', 0, '2018-01-25 09:21:07'),
(4, 3, 'Manojavam Maruta Tulya Vegam  Jitendriyam Buddhi Madam Varishtam  Vatadmajam Vanara Yuta Mukhyam  Sri Rama Dhutam Sharanam Prapatye', '<p>One who is so fast like that of the mind and the god of air</p>\r\n<p>One who has conquered the senses and one who has a superior intellect</p>\r\n<p>One who is the son of Lord Vayu (god of air) and the one who is the most important head of the Vanaras (onkey clan)</p>', 'hanuman.jpg', '', '', 0, '2018-01-25 09:24:45'),
(5, 3, 'Aum Aeem  Bhreem Hanumate,  Shree Ram Dootaaya Namaha', '<p>We plead to the Lord Hanuman, who is the greatest server and messenger of the Lord Shree Rama.</p>', 'hanuman.jpg', '', '', 0, '2018-01-25 09:27:26'),
(6, 5, 'Om Bhur Bhuvaá¸¥ Swaá¸¥ Tat-savitur VareÃ±yaá¹ƒ Bhargo Devasya DhÄ«mahi Dhiyo Yonaá¸¥ PrachodayÄt', '<p>We meditate on that most adored Supreme Lord, the creator, whose effulgence (divine light) illumines all realms (physical, mental and spiritual). May this divine light illumine our intellect.</p>', 'gaytrimntra.jpg', '', '', 0, '2018-01-25 09:29:02');

-- --------------------------------------------------------

--
-- Table structure for table `mantra_list`
--

CREATE TABLE IF NOT EXISTS `mantra_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mantraName` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `mantra_list`
--

INSERT INTO `mantra_list` (`id`, `mantraName`, `image`, `created_date_time`) VALUES
(2, 'Lord Shiva', 'shivpuja.jpg', '2018-01-22 10:34:22'),
(3, 'Lord Hanuman', 'care.png', '2018-01-22 10:34:46'),
(4, 'Shri Lakshmi', 'lakshmipuja.jpg', '2018-01-22 10:34:46'),
(5, 'Gayatri Mantra', 'mantra.png', '2018-01-23 14:46:30');

-- --------------------------------------------------------

--
-- Table structure for table `meditation`
--

CREATE TABLE IF NOT EXISTS `meditation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `meditation_list_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` longtext,
  `image` varchar(255) DEFAULT NULL,
  `audio` varchar(255) DEFAULT NULL,
  `video` varchar(255) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `meditation`
--

INSERT INTO `meditation` (`id`, `meditation_list_id`, `title`, `description`, `image`, `audio`, `video`, `status`, `created_date_time`) VALUES
(4, 2, 'Figure out why you want to use mantra meditation.', '<p>Every person has a different reason for meditating from health benefits to attaining a spiritual connection. Figuring out why you want to use mantra meditating will help you identify the best mantras to chant and time to dedicate to your meditation practice.</p>', 'mantra.png', '', '', 0, '2018-01-25 10:49:41'),
(5, 2, 'Find an appropriate mantra or mantras for your intention. ', '<p>One of the goals of chanting mantras is to feel their subtle vibrations. This sensation can help you effect positive changes and enter a deep state of meditation. Each mantra has different vibrations and you want to find one that corresponds to your intention.</p>', 'meditation.png', '', '', 0, '2018-01-25 10:50:27'),
(6, 2, 'Set an intention.', '<p>No mantra meditation practice is complete without first setting an intention. By taking a few seconds to dedicate your practice to something, you may be able to focus more intently and achieve a deeper state of meditation.</p>', 'meditation.png', '', '', 0, '2018-01-25 10:51:22'),
(7, 3, '1.Choose a target for your focus.', '<p>The sound of a metronome, the smell of incense, or a pleasing picture are all popular choices.</p>', 'meditation.png', '', '', 0, '2018-01-25 10:53:09'),
(8, 3, '2.Get into a comfortable position', '<p>Relax your body. Loosen your shoulders and breathe from your belly. You can cross your legs but you do not have to if you are more comfortable in another position, just as long as you can fully relax without falling asleep.</p>', '', '', 'meditation.png', 0, '2018-01-25 10:54:49');

-- --------------------------------------------------------

--
-- Table structure for table `meditation_list`
--

CREATE TABLE IF NOT EXISTS `meditation_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `meditationName` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `meditation_list`
--

INSERT INTO `meditation_list` (`id`, `meditationName`, `image`, `created_date_time`) VALUES
(2, 'Mantra Meditation', 'mantra.png', '2018-01-22 11:59:20'),
(3, 'Focused Meditation', 'meditation.png', '2018-01-24 06:00:21');

-- --------------------------------------------------------

--
-- Table structure for table `pooja`
--

CREATE TABLE IF NOT EXISTS `pooja` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `poojaId` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `audio` varchar(255) DEFAULT NULL,
  `video` varchar(255) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `pooja`
--

INSERT INTO `pooja` (`id`, `poojaId`, `title`, `description`, `image`, `audio`, `video`, `status`, `created_date_time`) VALUES
(6, 1, 'Step 2', '<p>Place chawki on your place of puja. Spread red cloth on it.</p>', 'pujavidhi.png', '', '', 0, '2018-01-25 08:26:33'),
(7, 1, 'Step 3', '<p>Place photo/idol of Goddess Lakshmi, Saraswati and Lord Ganesha.</p>', 'pujavidhi.png', '', '', 0, '2018-01-25 08:27:03'),
(5, 1, 'Step 1', '<p>Keep all these things together at one place.</p>', 'pujavidhi.png', '', '', 0, '2018-01-25 08:26:05'),
(8, 1, 'Step 4', '<p>&nbsp;Light a lamp to start puja. This lamp should be kept lit overnight. Also light dhoop stick.</p>', 'pujavidhi.png', '', '', 0, '2018-01-25 08:27:36'),
(11, 2, '1.Dhyana and Avahana', '<p>Pu', 'durga.png', '', '', 0, '2018-01-25 08:30:47'),
(12, 2, '2.Asana', '<p>After Goddess Durga has been invoked, five flowers for Anjali (by joining palm of both hands) are taken and left in front of the Murti to offer seat to Goddess Durga while chanting the following Mantra.</p>\r\n<p', 'durga.png', '', '', 0, '2018-01-25 08:31:33'),
(13, 2, '3.Padya Prakshalana ', '<p>The given Puja Vidhi includes all sixteen steps which are part of Shodashopachara Durga Puja Vidhi.</p>\r\n<ol>\r\n<li><strong>Dhyana and Avahana</strong></li>\r\n</ol>\r\n<p>Puja b', 'durga.png', '', '', 0, '2018-01-25 08:32:08');

-- --------------------------------------------------------

--
-- Table structure for table `pooja_list`
--

CREATE TABLE IF NOT EXISTS `pooja_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `poojaName` varchar(255) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `pooja_list`
--

INSERT INTO `pooja_list` (`id`, `poojaName`, `image`, `created_date_time`) VALUES
(1, 'Diwali Pooja', 'pujavidhi.png', '2018-01-22 10:10:35'),
(2, 'Durga Puja', 'durga.png', '2018-01-22 10:10:35');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE IF NOT EXISTS `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `categoryId` int(11) NOT NULL,
  `subCategoryId` int(11) NOT NULL,
  `price` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `video` varchar(255) NOT NULL,
  `audio` varchar(255) NOT NULL,
  `status` int(4) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=45 ;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `userId`, `title`, `categoryId`, `subCategoryId`, `price`, `description`, `image`, `video`, `audio`, `status`, `created_date_time`) VALUES
(44, 0, 'Puja Vidhi', 18, 65, '1233312', 'Puja vidhi', '', '', '', 1, '2018-01-08 12:49:39');

-- --------------------------------------------------------

--
-- Table structure for table `product_audio_gallery`
--

CREATE TABLE IF NOT EXISTS `product_audio_gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `productId` int(11) NOT NULL,
  `product_audio` varchar(255) NOT NULL,
  `created_date_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `product_audio_gallery`
--

INSERT INTO `product_audio_gallery` (`id`, `productId`, `product_audio`, `created_date_time`) VALUES
(1, 44, 'Om-Mantra-Preview.mp3', '2018-01-09 09:27:40');

-- --------------------------------------------------------

--
-- Table structure for table `product_image_gallery`
--

CREATE TABLE IF NOT EXISTS `product_image_gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `post_image` varchar(255) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=63 ;

--
-- Dumping data for table `product_image_gallery`
--

INSERT INTO `product_image_gallery` (`id`, `post_id`, `post_image`, `created_date_time`) VALUES
(62, 44, 'vishnuarti.jpg', '2018-01-08 12:49:39');

-- --------------------------------------------------------

--
-- Table structure for table `product_video_gallery`
--

CREATE TABLE IF NOT EXISTS `product_video_gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `productId` int(11) NOT NULL,
  `product_video` varchar(255) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `product_video_gallery`
--

INSERT INTO `product_video_gallery` (`id`, `productId`, `product_video`, `created_date_time`) VALUES
(1, 44, 'vid-20140210-wa0001.mp4', '2018-01-09 09:27:40');

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE IF NOT EXISTS `role` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(255) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `role_name`, `created_date_time`) VALUES
(1, 'Admin', '2017-12-19 10:44:36'),
(2, 'Pandit', '2017-12-19 10:45:30'),
(3, 'Yajman', '2017-12-19 10:46:05');

-- --------------------------------------------------------

--
-- Table structure for table `sub_category`
--

CREATE TABLE IF NOT EXISTS `sub_category` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `categoryId` int(11) NOT NULL,
  `subCategoryName` varchar(255) NOT NULL,
  `sub_category_status` tinyint(4) NOT NULL,
  `image` varchar(255) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=80 ;

--
-- Dumping data for table `sub_category`
--

INSERT INTO `sub_category` (`id`, `categoryId`, `subCategoryName`, `sub_category_status`, `image`, `created_date_time`) VALUES
(57, 27, 'Durga Chalisa', 1, 'durga.png', '2018-01-08 08:11:32'),
(58, 27, 'Hanuman Chalisa', 1, 'hanuman.jpg', '2018-01-08 09:12:03'),
(59, 16, 'Kundalini Yoga', 1, 'kundaliniyoga.jpg', '2018-01-08 09:19:03'),
(60, 16, 'Hatha Yoga', 1, 'hathayoga.jpg', '2018-01-08 09:19:53'),
(61, 16, 'Ashtanga Yoga', 1, 'astangayoga.png', '2018-01-08 09:20:31'),
(62, 17, 'Durga Arti', 1, 'durgaarti.jpg', '2018-01-08 09:21:05'),
(63, 17, 'Shiv Arti', 1, 'shivarti.jpg', '2018-01-08 09:24:06'),
(64, 17, 'Vshnu Arti', 1, 'vishnuarti.jpg', '2018-01-08 09:24:28'),
(65, 18, 'Lakshmi Puja Vidhi', 1, 'lakshmipuja.jpg', '2018-01-08 09:26:34'),
(66, 18, 'Vishnu  Puja Vidhi', 0, 'vishnupuja.jpg', '2018-01-08 09:27:23'),
(67, 18, 'Shiv Puja Vidhi', 1, 'shivpuja.jpg', '2018-01-08 09:27:52'),
(68, 19, 'Gaytri Mantra', 1, 'gaytrimntra.jpg', '2018-01-08 09:30:29'),
(69, 19, 'Maha Mirtunjay Mantra', 1, 'shivmntra.jpg', '2018-01-08 09:32:00'),
(70, 20, 'Bhagvat Katha', 1, 'bhagvatkatha.jpg', '2018-01-08 09:34:16'),
(71, 20, 'shiva Katha', 1, 'shivkatha.jpg', '2018-01-08 09:34:40'),
(72, 21, 'Transcendental Meditation', 1, 'transcendentalmeditation.jpg', '2018-01-08 14:53:32'),
(73, 21, 'Guided Visualization', 1, 'guidedvisualization.jpg', '2018-01-08 14:55:28'),
(74, 21, 'Movement Meditation', 1, 'movement.jpg', '2018-01-08 14:56:59'),
(75, 22, 'Cucumber', 1, 'cucumber.jpg', '2018-01-08 14:59:05'),
(76, 22, 'Ginger', 1, 'ginger.jpg', '2018-01-08 14:59:24'),
(77, 22, 'Calcium Rich Food', 0, 'calciumrichfood.jpg', '2018-01-08 15:00:14'),
(78, 23, 'Ayurveda Herbs', 0, 'ayurvedaherbs.jpg', '2018-01-08 15:01:25'),
(79, 23, 'Ayurvedic Medicines', 0, 'ayurvedicmedicines.jpg', '2018-01-08 15:02:16');

-- --------------------------------------------------------

--
-- Table structure for table `temporary_user`
--

CREATE TABLE IF NOT EXISTS `temporary_user` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `type` tinyint(4) NOT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `date_of_birth` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `social_id` varchar(255) DEFAULT NULL,
  `social_picture` varchar(255) DEFAULT NULL,
  `user_status` tinyint(4) NOT NULL,
  `is_driving_license` tinyint(1) NOT NULL DEFAULT '0',
  `is_credit_card_approved` tinyint(1) NOT NULL DEFAULT '0',
  `device_type` varchar(255) DEFAULT NULL,
  `device_token` varchar(255) DEFAULT NULL,
  `user_register_type` int(11) NOT NULL DEFAULT '3',
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `rating` varchar(100) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `temporary_user`
--

INSERT INTO `temporary_user` (`id`, `name`, `email`, `password`, `image`, `type`, `gender`, `date_of_birth`, `phone`, `social_id`, `social_picture`, `user_status`, `is_driving_license`, `is_credit_card_approved`, `device_type`, `device_token`, `user_register_type`, `created_date_time`, `rating`) VALUES
(12, 'Admin', 'ravindra.aln@gmail.com', 'f7c3bc1d808e04732adf679965ccc34ca7ae3441', '5a4e0e0364b3b.jpeg', 3, NULL, NULL, '9898989898', NULL, NULL, 0, 0, 0, NULL, NULL, 3, '2018-01-04 11:20:35', '0'),
(13, 'Saumya', 'saumya@gmail.com', '7c222fb2927d828af22f592134e8932480637c0d', '5a4e0ffd4c648.jpeg', 3, NULL, NULL, '8077024283', NULL, NULL, 0, 0, 0, NULL, NULL, 3, '2018-01-04 11:29:01', '0'),
(14, 'Admin', 'ravindra.aln@gmail.com', 'f7c3bc1d808e04732adf679965ccc34ca7ae3441', '5a4e10e0aa9e1.jpeg', 3, NULL, NULL, '9898989898', NULL, NULL, 0, 0, 0, NULL, NULL, 3, '2018-01-04 11:32:48', '0'),
(16, 'Admin', 'ravindra.aln@gmail.com', 'f7c3bc1d808e04732adf679965ccc34ca7ae3441', '5a4e157ada1b1.jpeg', 3, NULL, NULL, '9898989898', NULL, NULL, 0, 0, 0, NULL, NULL, 3, '2018-01-04 11:52:27', '0');

-- --------------------------------------------------------

--
-- Table structure for table `temporary_user_document`
--

CREATE TABLE IF NOT EXISTS `temporary_user_document` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `creditcard` varchar(255) NOT NULL,
  `driving_licence` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Dumping data for table `temporary_user_document`
--

INSERT INTO `temporary_user_document` (`id`, `user_id`, `creditcard`, `driving_licence`, `address`, `description`, `created_date_time`) VALUES
(26, 14, '1234568763678', 'DL45HG241352', 'c-75,sector 10,Noida', 'Users Document Description', '2018-01-04 11:32:48'),
(25, 13, '123456789789465132', '123456789798465132', 'Noida', 'Hello', '2018-01-04 11:29:01'),
(24, 12, '1234568763678', 'DL45HG241352', 'c-75,sector 10,Noida', 'Users Document Description', '2018-01-04 11:20:35'),
(28, 16, '1234568763678', 'DL45HG241352', 'c-75,sector 10,Noida', 'Users Document Description', '2018-01-04 11:52:27');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `type` tinyint(4) NOT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `user_status` tinyint(4) NOT NULL,
  `device_type` varchar(255) DEFAULT NULL,
  `device_token` varchar(255) DEFAULT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=117 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `image`, `type`, `phone`, `user_status`, `device_type`, `device_token`, `created_date_time`) VALUES
(2, 'PanditJi', 'panditji@gmail.com', 'f7c3bc1d808e04732adf679965ccc34ca7ae3441', 'panditji1.jpg', 1, '1234567899', 1, NULL, NULL, '2017-12-19 11:59:16'),
(105, 'Saumya', 'ravindra4736@gmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', NULL, 2, '9867876876', 1, NULL, NULL, '2018-01-23 06:23:12'),
(106, 'saumya', 'saumyajesus.gangwar@gmail.com', 'f7c3bc1d808e04732adf679965ccc34ca7ae3441', NULL, 2, '8439666778', 1, NULL, NULL, '2018-01-23 11:34:12');

-- --------------------------------------------------------

--
-- Table structure for table `user_register_type`
--

CREATE TABLE IF NOT EXISTS `user_register_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `register_type_name` varchar(255) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `user_register_type`
--

INSERT INTO `user_register_type` (`id`, `register_type_name`, `created_date_time`) VALUES
(1, 'Facebook', '2017-12-20 10:55:25'),
(2, 'Gmail', '2017-12-20 10:55:25'),
(3, 'App User', '2017-12-20 10:56:09');

-- --------------------------------------------------------

--
-- Table structure for table `user_verify`
--

CREATE TABLE IF NOT EXISTS `user_verify` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `otp` int(11) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=31 ;

--
-- Dumping data for table `user_verify`
--

INSERT INTO `user_verify` (`id`, `email`, `otp`, `created_date_time`) VALUES
(26, 'saumya@gmail.com', 9635, '2018-01-04 11:29:01');

-- --------------------------------------------------------

--
-- Table structure for table `yoga`
--

CREATE TABLE IF NOT EXISTS `yoga` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `yoga_list_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `audio` varchar(255) DEFAULT NULL,
  `video` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `yoga`
--

INSERT INTO `yoga` (`id`, `yoga_list_id`, `title`, `description`, `image`, `audio`, `video`, `status`, `created_date_time`) VALUES
(7, 1, 'Step 3', '<ol><li>As you exhale, slowly lower your buttocks towards your heels, feeling the tailbone lengthen away from the back of your pelvis</li></ol>', 'balasana2.jpg', '', '', 0, '2018-01-25 07:44:25'),
(8, 1, 'Step 4', '<ol>\r\n<li>As your torso folds over your thighs, lengthen the back of your neck before your forehead rests on the floor</li>\r\n</ol>', 'balasana2.jpg', '', '', 0, '2018-01-25 07:45:30'),
(5, 2, 'Step 1', '<p>Fold a thick blanket or two into a firm support about six inches high. Sit close to one edge of this support and stretch your legs out in front of your torso on the floor in&nbsp;<a href="https://www.yogajournal.com/poses/2480">Dandasana (Staff Pose)</a>.</p>', 'balasana2.jpg', '', '', 0, '2018-01-25 07:43:26'),
(14, 3, 'Step 1', '<ul>\r\n<li>First get into the standing position keep your body straight. By your legs keep a distance of 3-4 feet between each other.</li>\r\n</ul>', 'virbhadrasana.jpg', '', '', 0, '2018-01-25 12:10:20'),
(6, 1, 'Step 2', '<ol>\r\n<li>Release your toes on the floor and separate your knees about hip width apart</li>\r\n</ol>', 'balasana2.jpg', '', '', 0, '2018-01-25 07:44:08'),
(9, 1, 'Step 5', '<ol>\r\n<li>Lay your arms by the thighs with palms facing up and feel how the weight of your shoulders lightly spreads the shoulder blades</li>\r\n</ol>', 'balasana2.jpg', '', '', 0, '2018-01-25 07:46:00'),
(10, 2, 'Step 1', '<p>Fold a thick blanket or two into a firm support about six inches high. Sit close to one edge of this support and stretch your legs out in front of your torso on the floor in&nbsp;<a href="https://www.yogajournal.com/poses/2480">Dandasana (Staff Pose)</a>.</p>', 'sukhasana.png', '', '', 0, '2018-01-25 07:47:50'),
(11, 2, 'Step 2', '<p>Cross your shins, widen your knees, and slip each foot beneath the opposite knee as you bend your knees and fold the legs in toward your torso.</p>', 'sukhasana.png', '', '', 0, '2018-01-25 07:48:28'),
(12, 2, 'Step 3', '<p>Relax the feet so their outer edges rest comfortably on the floor and the inner arches settle just below the opposite shin. You will know you have the basic leg fold of Sukhasana when you look down and see a triangle, its three sides formed by the two thighs and the crossed shins. Don not confuse this position with that of other classic seated postures in which the ankles are tucked in close to the sitting bones. In Sukhasana, there should be a comfortable gap between the feet and the pelvis.</p>', 'sukhasana.png', '', '', 0, '2018-01-25 08:08:36');

-- --------------------------------------------------------

--
-- Table structure for table `yoga_audio_gallery`
--

CREATE TABLE IF NOT EXISTS `yoga_audio_gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `yogaId` int(11) NOT NULL,
  `yoga_audio` varchar(255) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `yoga_image_gallery`
--

CREATE TABLE IF NOT EXISTS `yoga_image_gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `yogaId` int(11) NOT NULL,
  `yoga_image` varchar(255) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Table structure for table `yoga_list`
--

CREATE TABLE IF NOT EXISTS `yoga_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `yogaName` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `yoga_list`
--

INSERT INTO `yoga_list` (`id`, `yogaName`, `image`, `created_date_time`) VALUES
(1, 'Balasana', 'balasana2.jpg', '2018-01-22 05:54:25'),
(2, 'Sukhaasana', 'sukhasana.png', '2018-01-22 05:54:25'),
(3, 'Virbhdrasana', 'virbhadrasana1.jpg', '2018-01-22 05:55:00'),
(4, 'Trikonasana', 'trikonasana.jpg', '2018-01-22 05:55:00'),
(5, 'Chaturanga', 'chaturangasana.jpg', '2018-01-22 05:55:48'),
(7, 'Utkatasana', 'utkatasana.jpg', '2018-01-24 06:45:38');

-- --------------------------------------------------------

--
-- Table structure for table `yoga_video_gallery`
--

CREATE TABLE IF NOT EXISTS `yoga_video_gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `yogaId` int(11) NOT NULL,
  `yoga_video` varchar(255) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
