-- phpMyAdmin SQL Dump
-- version 4.0.10.18
-- https://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Dec 08, 2017 at 07:22 AM
-- Server version: 5.6.36-cll-lve
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `towfeer`
--

-- --------------------------------------------------------

--
-- Table structure for table `avilable_product_branch`
--

CREATE TABLE IF NOT EXISTS `avilable_product_branch` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `product_id` int(200) NOT NULL,
  `branch_id` int(200) NOT NULL,
  `product_count` int(100) DEFAULT '10',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `size_id` int(11) NOT NULL DEFAULT '0',
  `bidding_start_time` timestamp NULL DEFAULT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=293 ;

-- --------------------------------------------------------

--
-- Table structure for table `branch`
--

CREATE TABLE IF NOT EXISTS `branch` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `address` varchar(200) NOT NULL,
  `lat` float(15,11) NOT NULL,
  `lng` float(15,11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `phone` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=49 ;

-- --------------------------------------------------------

--
-- Table structure for table `brand`
--

CREATE TABLE IF NOT EXISTS `brand` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(500) NOT NULL,
  `image` varchar(300) NOT NULL,
  `establish` varchar(300) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=74 ;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `cat_name` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Table structure for table `discount`
--

CREATE TABLE IF NOT EXISTS `discount` (
  `discount_id` int(11) NOT NULL AUTO_INCREMENT,
  `discount_name` varchar(255) NOT NULL,
  `discount_percent` varchar(50) NOT NULL DEFAULT '0',
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`discount_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `discount`
--

INSERT INTO `discount` (`discount_id`, `discount_name`, `discount_percent`, `created_date_time`) VALUES
(1, 'Get 5% discount', '5', '2017-11-28 12:06:52'),
(2, 'Get 10% discount', '10', '2017-11-28 12:07:09'),
(3, 'Get 15% discount', '15', '2017-11-28 12:07:18'),
(4, 'Get 20% discount', '20', '2017-11-28 12:07:31'),
(5, 'Get 25% discount', '25', '2017-11-28 12:07:40'),
(6, 'Get 30% discount', '30', '2017-11-28 12:07:50'),
(7, 'Get 35% discount', '35', '2017-11-28 12:08:02'),
(8, 'Get 40% discount', '40', '2017-11-28 12:08:17'),
(9, 'Get 45% discount', '45', '2017-11-28 12:08:29'),
(10, 'Get 50% discount', '50', '2017-11-28 12:39:13'),
(12, ' Get 70% discount ', '70', '2017-11-28 12:42:06'),
(13, 'Get 75% discount', '75', '2017-11-28 12:42:18'),
(14, 'Buy 1 Get 1 Free', '0', '2017-12-02 12:27:46'),
(15, 'Buy 2 Get 1 FreeG', '0', '2017-12-02 12:30:46'),
(16, 'Get 60% discount', '60', '2017-12-04 10:52:36'),
(17, 'Get 65% discount', '65', '2017-12-04 10:52:36');

-- --------------------------------------------------------

--
-- Table structure for table `extended_user_role`
--

CREATE TABLE IF NOT EXISTS `extended_user_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_role_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `extended_user_role`
--

INSERT INTO `extended_user_role` (`id`, `user_role_id`, `name`, `created_date_time`) VALUES
(1, 2, 'Individual', '2017-11-27 10:42:40'),
(2, 2, 'Company', '2017-11-24 06:53:52'),
(3, 1, 'default', '2017-11-24 10:48:06');

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE IF NOT EXISTS `gender` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gender_name` varchar(50) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`id`, `gender_name`, `created_date_time`) VALUES
(1, 'Male', '2017-11-24 06:02:55'),
(2, 'Female', '2017-11-24 06:02:55');

-- --------------------------------------------------------

--
-- Table structure for table `offers`
--

CREATE TABLE IF NOT EXISTS `offers` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(500) NOT NULL,
  `branch` varchar(500) NOT NULL,
  `details` varchar(500) NOT NULL,
  `brand_name` varchar(500) NOT NULL,
  `user_id` int(11) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `discount_cat_id` int(11) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `image` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=127 ;

-- --------------------------------------------------------

--
-- Table structure for table `offer_product`
--

CREATE TABLE IF NOT EXISTS `offer_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `offer_id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL DEFAULT '0',
  `brand_id` int(11) NOT NULL DEFAULT '0',
  `discount_cat_id` int(11) NOT NULL DEFAULT '0',
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=275 ;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(500) NOT NULL,
  `description` varchar(500) NOT NULL,
  `price` varchar(500) NOT NULL,
  `image` varchar(255) NOT NULL,
  `sub_categories` varchar(500) NOT NULL,
  `categories` varchar(255) NOT NULL,
  `bidding` int(11) NOT NULL DEFAULT '0',
  `availibility` varchar(500) NOT NULL,
  `user_id` int(11) NOT NULL,
  `size` varchar(50) DEFAULT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=118 ;

-- --------------------------------------------------------

--
-- Table structure for table `product_size`
--

CREATE TABLE IF NOT EXISTS `product_size` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `product_size` varchar(100) NOT NULL DEFAULT '0',
  `product_id` int(200) NOT NULL,
  `branch_id` int(200) NOT NULL,
  `brand_id` int(200) NOT NULL,
  `total_pices` varchar(100) NOT NULL DEFAULT '0',
  `size_id` int(11) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=279 ;

-- --------------------------------------------------------

--
-- Table structure for table `product_transactions`
--

CREATE TABLE IF NOT EXISTS `product_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `amount` varchar(50) NOT NULL,
  `user_id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `size_id` int(11) NOT NULL,
  `transaction_id` varchar(255) NOT NULL,
  `seller_id` int(11) NOT NULL,
  `created_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=170 ;

-- --------------------------------------------------------

--
-- Table structure for table `rating`
--

CREATE TABLE IF NOT EXISTS `rating` (
  `rating_id` int(30) NOT NULL AUTO_INCREMENT,
  `user_id` int(30) NOT NULL,
  `branch_id` int(30) NOT NULL,
  `rating_1` int(30) NOT NULL,
  `rating_2` int(30) NOT NULL,
  `rating_3` int(30) NOT NULL,
  `rating_4` int(30) NOT NULL,
  `rating_5` int(30) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`rating_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=44 ;

-- --------------------------------------------------------

--
-- Table structure for table `reservation`
--

CREATE TABLE IF NOT EXISTS `reservation` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `product_id` varchar(100) NOT NULL,
  `name` varchar(500) NOT NULL,
  `date` date NOT NULL,
  `transaction` varchar(500) NOT NULL,
  `amount` int(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `size_name`
--

CREATE TABLE IF NOT EXISTS `size_name` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `size_name` varchar(50) NOT NULL,
  `create_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `size_name`
--

INSERT INTO `size_name` (`id`, `size_name`, `create_date_time`) VALUES
(1, 'M', '2017-11-27 09:46:34'),
(2, 'L', '2017-11-27 09:46:34'),
(3, 'XL', '2017-11-27 09:46:46'),
(4, 'S', '2017-11-27 09:46:46');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `email` varchar(500) NOT NULL,
  `password` varchar(255) NOT NULL,
  `image` varchar(100) NOT NULL,
  `type` tinyint(1) NOT NULL,
  `name` varchar(500) NOT NULL,
  `gender` tinyint(1) NOT NULL,
  `DOB` date NOT NULL,
  `extende_role_id` int(11) NOT NULL DEFAULT '0',
  `created_date_type` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `phone` varchar(255) DEFAULT NULL,
  `user_status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=190 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `image`, `type`, `name`, `gender`, `DOB`, `extende_role_id`, `created_date_type`, `phone`, `user_status`) VALUES
(2, 'tawfer@gmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'RR.jpg', 1, 'Raghad Butaiban', 2, '2013-08-12', 3, '2017-11-24 06:06:48', '9876867967', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_login`
--

CREATE TABLE IF NOT EXISTS `user_login` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `session_id` varchar(255) NOT NULL,
  `login_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=449 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE IF NOT EXISTS `user_role` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(500) NOT NULL,
  `created_date_type` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `user_role`
--

INSERT INTO `user_role` (`id`, `role_name`, `created_date_type`) VALUES
(1, 'Admin', '2017-11-24 05:57:44'),
(2, 'Seller', '2017-11-24 05:57:44'),
(3, 'Customer', '2017-11-24 05:58:03');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
